package comms;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.LinkedBlockingQueue;

import comms.ServerEventListener.CONNECTION_STATE;

import gui.LogMessager;
import data.ACDataSource;
import data.Constants;
import data.DataSource;
import data.DirtDataSource;
import data.ETS2DataSource;
import data.InputDeviceManager.EventFromInputDeviceListener;
import data.Grid2DataSource;
import data.Gtr2DataSource;
import data.LFSData;
import data.PCarsDataSource;
import data.R3EDataSource;
import data.Race07DataSource;
import data.Settings;
import data.VDashData;
import data.DataSource.NullSimulationFileException;
import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;

public abstract class NetworkingBase implements EventFromInputDeviceListener{
	
	protected static DataSource dataSource;
	protected LogMessager messager;
	private ServerEventListener eventListener;
	
	protected VDashData vdashData;
	
	protected Thread listenThread;
	
	protected Thread sendingThread;
	protected Runnable sendingRunnable;
	
	private Thread dataSendingThread;
	private Runnable dataSendRunnable;
	private LinkedBlockingQueue<ByteBuffer> sendingQueue = new LinkedBlockingQueue<>();
	
	/**Send simulated data flag*/
	protected boolean simulateData = false;
	/**Send player names flag*/
	protected boolean sendParticipantNames = true;
	
	protected boolean sendBroken = false;
	protected boolean sending = false;
	
	private CONNECTION_STATE connectionState;

	public NetworkingBase(LogMessager messager, ServerEventListener eventListener){
		this.messager = messager;
		this.eventListener = eventListener;
		this.connectionState = CONNECTION_STATE.DISCONNECTED;
		
		init();
	}
	
	protected void init(){
		sending = true;
		
		sendingQueue.clear();
		
		dataSendRunnable = new Runnable() {
			@Override
			public void run() {
				try{
					while(true){
						sendByteBuffer(sendingQueue.take());
					}
				}catch (InterruptedException e){
					//Stopped while waiting for input
				}
			}
		};
		
		dataSendingThread = new Thread(dataSendRunnable);
		dataSendingThread.setName("Data sending queue thread");
		dataSendingThread.start();
		
		sendingRunnable = new Runnable() {
			@Override
			public void run() {
				//Pause the sending thread to let the android server finish sending monitor states/format data
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				log("Starting sending thread", LOG_LVL.L0);
				connectionStateChange("Fully operational", CONNECTION_STATE.CONNECTED);				
				byte[] sendingData = new byte[Constants.Comms.MAX_PACKET_SIZE];
				ByteBuffer buffer = ByteBuffer.wrap(sendingData);

				while(sending){
					try {
						if(dataSource == null){
							initDataSource();
						}

						dataSource.simulate(Settings.getInstance().shouldSimulateData());
						if(!dataSource.ready()){
							try{
								dataSource.open();
							}catch(IOException e){
								log("Cannot open data source,  closing server", LOG_LVL.L0);
								log(e.getMessage(), LOG_LVL.L0);
								throw e;
							}
						}else{
							vdashData = dataSource.readData();
							if(sendParticipantNames){
								vdashData.setSendParticipantNames();
								sendParticipantNames = false;
							}

							buffer.clear();
							buffer.put((byte) Constants.Comms.PCARS_DATA);
							buffer.putInt(1);
							vdashData.writeDataToStream(buffer);

							if(sendBroken){
								byte[] arr = buffer.array();
								Arrays.fill(arr, 5, arr.length, (byte)-1);
								sendBroken = false;
							}

							sendByteBuffer(buffer);
						}
					} catch (IOException e) {
						//failure to open the data source
						//failure to fill buffer
					} catch (NullSimulationFileException e) {
						shutdown();
						connectionStateChange("No simulation data. Stopped.", CONNECTION_STATE.DISCONNECTED);
					} 

					try {
						Thread.yield();
						Thread.sleep(Settings.getInstance().getDataPeriod());
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
	}
	
	protected void sendData(ByteBuffer b){
		try {
			sendingQueue.put(b);
		} catch (InterruptedException e) {
			//sendingQueue was full, and we were interrupted while waiting to insert it
			//sendingQueue is unbounded.
			e.printStackTrace();
		}
	}
	
	protected void initDataSource(){
		//Initialise the data source to the correct source specified in the Settings
		if(GAME_TYPE.DiRT2.equals(Settings.getInstance().getGameType())){
			dataSource = new DirtDataSource(messager);
		}else if(GAME_TYPE.DiRT3.equals(Settings.getInstance().getGameType())){
			dataSource = new DirtDataSource(messager);
		}else if(GAME_TYPE.ProjectCARS.equals(Settings.getInstance().getGameType())){
			dataSource = new PCarsDataSource(messager);
		}else if(GAME_TYPE.AC.equals(Settings.getInstance().getGameType())){
			dataSource = new ACDataSource(messager);
		}else if(GAME_TYPE.RACE07.equals(Settings.getInstance().getGameType())){
			dataSource = new Race07DataSource(messager);
		} else if(GAME_TYPE.R3E.equals(Settings.getInstance().getGameType())){
			dataSource = new R3EDataSource(messager);
		} else if(GAME_TYPE.GTR2.equals(Settings.getInstance().getGameType())){
			dataSource = new Gtr2DataSource(messager);
		} else if(GAME_TYPE.LFS.equals(Settings.getInstance().getGameType())){
			dataSource = new LFSData(messager);
		} else if(GAME_TYPE.GRID2.equals(Settings.getInstance().getGameType())){
			dataSource = new Grid2DataSource(messager);
		} else if(GAME_TYPE.ETS2.equals(Settings.getInstance().getGameType())){
			dataSource = new ETS2DataSource(messager);
		}
		//else if(GAME_TYPE.IR.equals(Settings.getInstance().getGameType())){
		//	dataSource = new IRacingDataSource(messager);
		//}
	}
	
	public void resetDataSource(){
		log("Resetting data source for "+Settings.getInstance().getGameType(), LOG_LVL.L0);
		dataSource.close();
		initDataSource();
	}
	
	public static DataSource getDatasource(){
		return dataSource;
	}
	
	@Override
	public void receiveInputEvent(int event) {
		byte[] sendingData = new byte[Constants.Comms.MAX_PACKET_SIZE];
		ByteBuffer buffer = ByteBuffer.wrap(sendingData);
		buffer.put((byte)Constants.Comms.INPUT_EVENT);
		buffer.putInt(0);
		buffer.put((byte)event);
		
		sendData(buffer);
	}
	
	protected void connectionStateChange(String msg, CONNECTION_STATE c){
		this.connectionState = c;
		eventListener.onConnectionStateChange(msg, c);
	}
	
	public CONNECTION_STATE getConnectionState(){
		return this.connectionState;
	}
	
	protected void log(String msg, LOG_LVL lvl){
		messager.logMessage(msg, lvl);
	}
	
	public abstract void sendByteBuffer(ByteBuffer b);
	public abstract void shutdown();
	
	protected void doShutdown(){
		dataSendingThread.interrupt();
	}
	
	public void sendBrokenPacket(){sendBroken = true;}
}
