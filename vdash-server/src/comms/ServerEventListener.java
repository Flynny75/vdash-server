package comms;

public interface ServerEventListener{
	enum CONNECTION_STATE{CONNECTED, CONNECTING, DISCONNECTED};
	public void onConnectionStateChange(String state, CONNECTION_STATE conn_state);
}