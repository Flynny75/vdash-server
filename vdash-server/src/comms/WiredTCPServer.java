package comms;

import gui.Gui;
import gui.LogMessager;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import comms.ServerEventListener.CONNECTION_STATE;

import data.InputDeviceManager.EventFromInputDeviceListener;
import data.Constants;
import data.Settings.LOG_LVL;


/**
 * Android acts as server (ADB forward doesnt work otherwise), so PC acts as client
 * @author Alex
 *
 */
public class WiredTCPServer extends NetworkingBase implements EventFromInputDeviceListener{
	private boolean listening;

	private InputStream inputStream;
	private OutputStream outputStream;

	private Runnable listenRunnable;

	private boolean connected = false;
	private boolean shutdownReq = false;
	private boolean shutdown = false;
	private boolean[] states = new boolean[Constants.MonitorStates.TOTAL_STATES];

	byte[] KEY = new byte[]{'V','D','A','S','H','_','&','0','&','_','S','T','A','R','T'};

	private Socket socket;

	public WiredTCPServer(ServerEventListener eventListener,LogMessager messeger){
		super(messeger,eventListener);
		connect();
	}

	/**
	 * Perform the actual connection, retrying if appropriate
	 */
	private void connect(){
		init();

		try {
			//Open a socket on localhost with the default port
			//ADB forward has already forwarded this port to the device

			socket  = new Socket("localhost", Constants.Comms.SERVER_LISTEN_PORT);
			inputStream = socket.getInputStream();
			outputStream = socket.getOutputStream();

			listenThread = new Thread(listenRunnable);
			listenThread.start();
		} catch (IOException e) {
			log("Could not open socket - I/O Exception - "+e.getMessage(),LOG_LVL.L0);

			if(listening && !shutdownReq){
				//Wait and restart
				try{
					connectionStateChange("Reconnecting...", CONNECTION_STATE.CONNECTING);
					log("Attempting to reconnect...", LOG_LVL.L0);
					Gui.killAdb(false, messager);
					Gui.restoreAdb(getClass().getClassLoader(), messager);
					connect();
				}catch(IOException e2){
					log("Could not auto-reconnect! - "+e2.getMessage(), LOG_LVL.L0);
					doShutdown();
				}
			}else{
				connectionStateChange("Connection error - "+e.getMessage(), CONNECTION_STATE.DISCONNECTED);
				doShutdown();
			}
		}
	}

	/**
	 * Create the listening runnable and reset state
	 */
	@Override
	protected void init() {
		super.init();

		listening = true;
		shutdown = false;

		listenRunnable = new Runnable() {
			public void run() {
				try {

					//APP STILL SENDS 5K PACKETS
					//TODO re-wite app to send well-formed packets (like server)
					int totalReadCount = 0;
					int lastReadCount = 0;
					int remainder = 0;

					byte[] networkData = new byte[Constants.Comms.MAX_PACKET_SIZE];
					byte[] bufferedData = new byte[Constants.Comms.MAX_PACKET_SIZE];
					ByteBuffer buffer = ByteBuffer.wrap(bufferedData);

					while((lastReadCount = inputStream.read(networkData, 0, networkData.length)) != -1 && listening){
						totalReadCount += lastReadCount;
						if(totalReadCount >= Constants.Comms.MAX_PACKET_SIZE){
							//Got the full packet, but might roll over
							remainder = totalReadCount - Constants.Comms.MAX_PACKET_SIZE;
							buffer.put(networkData,0,lastReadCount - remainder);
							processIncommmingPacket(buffer.array());
							buffer.clear();
							if(remainder != 0){
								buffer.put(networkData, (lastReadCount-remainder),remainder);
							}
							totalReadCount = remainder;

							if(!connected && !shutdown){
								messager.logMessage("Connected");
								connected = true;
								sendingThread = new Thread(sendingRunnable);
								sendingThread.start();
							}
						}else{
							buffer.put(networkData, 0, lastReadCount);
						}
					}

					if(listening && !shutdownReq){
						log("Unexpected EOS... Shutting down...", LOG_LVL.L0);
						doShutdown();
						Gui.killAdb(true, messager);

						connectionStateChange("Reconnecting...", CONNECTION_STATE.CONNECTING);
						log("Restarting...", LOG_LVL.L0);
						Gui.restoreAdb(getClass().getClassLoader(), messager);
						connect();
						return;
					}
				} catch (IOException e) {
					if(!shutdown){
						shutdown();
						if(!shutdownReq){
							log("Could not write to socket. Terminating server. Is the device still connected?",LOG_LVL.L0);
							connectionStateChange("Connection error - "+e.getMessage(), CONNECTION_STATE.DISCONNECTED);
							e.printStackTrace();
						}else{
							log("Device closed connection", LOG_LVL.L0);
							connectionStateChange("Device closed connection", CONNECTION_STATE.DISCONNECTED);
						}
					}
				}
			}
		};	
	}

	@Override
	public void sendByteBuffer(ByteBuffer b) {
		try {
			send(b);
		} catch (IOException e) {
			if(!shutdown){
				shutdown();
				if(!shutdownReq){
					log("Could not write to socket. Terminating server. Is the device still connected?", LOG_LVL.L0);
					connectionStateChange("Connection error - "+e.getMessage(), CONNECTION_STATE.DISCONNECTED);
					e.printStackTrace();
					log("ATTEMPT RE-CONNECT", LOG_LVL.L0);
				}else{
					log("Device closed connection", LOG_LVL.L0);
					connectionStateChange("Device closed connection", CONNECTION_STATE.DISCONNECTED);
				}
			}else{
				System.out.println("Duplicate shutdown ignored for WiredTCPServer");
			}
		}
	}

	/**
	 * Process the incoming packet from the client.
	 * Has different implementation than the WirelessUDP server
	 * @param networkData - The byte[] of raw data from the client
	 */
	private void processIncommmingPacket(byte[] networkData){
		//Open data streams to read the packet data
		shutdownReq = false;
		ByteArrayInputStream bais = new ByteArrayInputStream(networkData);
		DataInputStream dis = new DataInputStream(bais);
		int type;
		try {
			//Get the type
			type = dis.readInt();
			//Process according to protocol
			switch(type){
			case Constants.Comms.NEW_CONNECTION:
				log("Got code: NEW_CONNECTION", LOG_LVL.L1);
				//New connection also includes the monitor states
				setWaiting();
			case Constants.Comms.CHANGE_MONITOR_STATE:
				log("Got code: CHANGE_MONITOR_STATE", LOG_LVL.L1);

				states = new boolean[Constants.MonitorStates.TOTAL_STATES];
				for(int i = 0; i < Constants.MonitorStates.TOTAL_STATES; i++){
					states[i] = dis.readBoolean();
				}
				String stateString = new String();
				for(boolean b : states){
					stateString += b+", ";
				}
				log("Change sates - "+stateString, LOG_LVL.L1);
				break;
			case Constants.Comms.REQUEST_NAMES:
				log("Got code: REQUEST_NAMES", LOG_LVL.L1);
				sendParticipantNames = true;
				break;
			case Constants.Comms.REMOVE_CONNECTION:
				shutdown();
				break;
			}	

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**tell the app that its currently waiting for data...*/
	private void setWaiting(){
		byte[] sendingData = new byte[Constants.Comms.MAX_PACKET_SIZE];
		ByteBuffer buffer = ByteBuffer.wrap(sendingData);
		buffer.clear();
		buffer.put((byte) Constants.Comms.WAITING);
		buffer.putInt(1);

		sendData(buffer);
	}

	/**
	 * Send the actual buffer in a format appropriate for USB
	 * Key, size, packet
	 * @param buffer - buffer containing data to send
	 * @throws IOException
	 */
	private void send(ByteBuffer buffer) throws IOException{
		synchronized (outputStream) {
			outputStream.write(KEY);
			outputStream.write(ByteBuffer.allocate(4).putInt(buffer.position()).array());
			outputStream.write(buffer.array(), 0, buffer.position());
			outputStream.flush();
		}
	}

	@Override
	public void shutdown(){
		shutdownReq = true;
		log("Request shutdown", LOG_LVL.L1);
		doShutdown();
	}

	@Override
	protected void doShutdown(){
		if(!shutdown){			
			super.doShutdown();
			byte[] data = new byte[Constants.Comms.MAX_PACKET_SIZE];
			ByteBuffer buff = ByteBuffer.wrap(data);
			buff.put((byte)Constants.Comms.REMOVE_CONNECTION);
			buff.putInt(1);
			
			/*Send this directly as we are closing the socket straight after, and super has already 
			interrupted the sending thread
			*/
			try{
				send(buff);
			}catch(IOException e){
				log("Could not send close packet! - "+e.getMessage(), LOG_LVL.L1);
			}

			System.out.println("WiredTCPServer shutdown");
			shutdown = true;
			connected = false;
			sending = false;
			listening = false;
			try{
				socket.close();
			}catch(IOException e1){
				e1.printStackTrace();
			}

			if(dataSource != null){
				dataSource.close();
			}
		}
	}
}

