package comms;

import gui.LogMessager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.LinkedList;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceInfo;

import comms.ServerEventListener.CONNECTION_STATE;

import data.Constants;
import data.Settings;
import data.Settings.LOG_LVL;

/**
 * Class implementing a Wirelss UDB server. Responsible for registering the JmDNS server for auto discovery, managing connections, and sending data
 * @author Alex
 */
public class WirelessUDPServer extends NetworkingBase{
	/**The socket to listen any peers on*/
	private DatagramSocket listenSocket;
	/**The socket to talk to any peers on*/
	private DatagramSocket sendingSocket;
	/**A list of connected peers*/
	private volatile LinkedList<Peer> peers = new LinkedList<Peer>();
	/**Running flag*/
	private boolean running = true;
	private boolean listening = true;

	/**Increment every time a peer connects*/
	private int peerCounter = 0;
	/**Sleep interval for the sending thread*/
	private int lastPeerId = 0;

	/**The port the server is listening on*/
	private int listenPort;
	
	private Thread pingThread;

	/**
	 * Create a new WirelessUDPServer with the specified listeners
	 * @param eventListener - Event listener to receive server events
	 * @param messeger - Log listener to receive log events
	 * @throws UnknownHostException
	 * @throws SocketException
	 */
	public WirelessUDPServer(ServerEventListener eventListener,LogMessager messeger) throws UnknownHostException, SocketException{
		this(eventListener, messeger,Constants.Comms.SERVER_LISTEN_PORT);
	}

	/**
	 * Create a new WirelessUDPServer with the specified listeners and port
	 * @param eventer - Event listeners for server events
	 * @param messeger - Log listener for log events
	 * @param port - Custom port to connect to
	 * @throws UnknownHostException
	 * @throws SocketException
	 */
	public WirelessUDPServer(ServerEventListener eventer, LogMessager messeger, int port) throws UnknownHostException, SocketException{
		super(messeger, eventer);

		listenPort = port;

		initDataSource();

		final InetAddress localHost;

		try{
			String address = Settings.getInstance().getWirelessInterfaceAddress();
			if(address == null){
				localHost = InetAddress.getLocalHost();	
			}else{
				localHost = InetAddress.getByName(address);
			}

		}catch(UnknownHostException e){
			messager.logMessage(e.getMessage());
			connectionStateChange("Could not start. Unknown host exception", CONNECTION_STATE.DISCONNECTED);
			throw e;
		}

		log("Server IP: "+localHost+" Port: "+listenPort, LOG_LVL.L0);

		try{
			sendingSocket = new DatagramSocket();
			listenSocket = new DatagramSocket(listenPort, localHost);
		}catch(SocketException e){
			messager.logMessage(e.getMessage());
			connectionStateChange("Could not start. Socket exception", CONNECTION_STATE.DISCONNECTED);
			throw e;
		}

		sendingThread = new Thread(sendingRunnable);
		sendingThread.start();

		listenThread = new Thread(receivePacketRunnable);
		listenThread.start();
		connectionStateChange("Manual IP:PORT entry (registering auto service...)", CONNECTION_STATE.CONNECTED);

		// Register the service (blocking)
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					//start a JmDNS service on the same IP and PORT as the listening socket
					log("Starting network service...", LOG_LVL.L0);
					JmDNS mdnsServer = JmDNS.create(localHost);
					ServiceInfo pCarsService = ServiceInfo.create(Constants.Comms.SERVICE_NAME, "PCars Java Server", listenPort, "PCars Java Server");
					mdnsServer.registerService(pCarsService);
					log("Service registered", LOG_LVL.L0);
					connectionStateChange("Fully operational (auto enabled)", CONNECTION_STATE.CONNECTED);
				}catch(IOException e){
					log("Cannot start service. Auto discovery unavailable.", LOG_LVL.L0);
					e.printStackTrace();
				}
			}
		});
		t.start();
		
		Runnable pingRunnable = new Runnable() {
			@Override
			public void run() {
				while(running){
					try{
						Thread.sleep(1000);
					}catch(InterruptedException e){}
					
					for(Peer p : peers){
						pingPeer(p);
					}
				}
			}
		};
		
		pingThread = new Thread(pingRunnable);
		pingThread.setName("Wireless Ping Thread");
		pingThread.start();
	}

	/**Runnable to wait on the socket while receiving and processing incoming packets. */
	private Runnable receivePacketRunnable = new Runnable() {
		@Override
		public void run() {
			DatagramPacket incommingPacket = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
			while(listening){
				try {
					log("Waiting for new packet - "+peers.size()+" peers\n", LOG_LVL.L1);
					listenSocket.receive(incommingPacket);
					//Got a new packet!
					processIncommmingPacket(incommingPacket);
				} catch (IOException e) {
					shutdown();
				}
			}
		}
	};

	@Override
	public void sendByteBuffer(ByteBuffer b) {
		DatagramPacket outboundPacket = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
		outboundPacket.setData(b.array());
		for(Peer p : peers){
			outboundPacket.setPort(p.port);
			outboundPacket.setAddress(p.address);
			
			try {
				sendingSocket.send(outboundPacket);
			} catch (IOException e) {
				connectionStateChange("Server error", CONNECTION_STATE.DISCONNECTED);
				e.printStackTrace();
			}
		}
	}

	/**Process the received packet from a peer*/
	private void processIncommmingPacket(DatagramPacket packet){
		//Open data streams to read the packet data
		ByteArrayInputStream bais = new ByteArrayInputStream(packet.getData());
		DataInputStream dis = new DataInputStream(bais);
		int type,peerId;
		try {
			//Get the type
			type = dis.readInt();
			log("Got code: "+type, LOG_LVL.L1);
			synchronized (peers) {
				//Process according to protocol
				switch(type){
				case Constants.Comms.REMOVE_CONNECTION:
				case Constants.Comms.NEW_CONNECTION:
					if(type == Constants.Comms.NEW_CONNECTION){
						lastPeerId = peerCounter;
						log("New connection! id:"+peerCounter, LOG_LVL.L0);
						Peer p = new Peer(packet.getPort(), packet.getAddress(),peerCounter);
						peers.add(p);

						if(!dataSource.ready()){
							dataSource.open();
						}
						pingPeer(p);
						setWaiting(p);
						peerCounter++;
					}else{
						int toRemove = dis.readInt();
						log("Remove connection - id:"+toRemove, LOG_LVL.L0);
						for(Peer p:peers){
							if(p.id == toRemove){
								peers.remove(p);
								break;
							}
						}
						break;
					}
				case Constants.Comms.CHANGE_MONITOR_STATE:
					peerId = dis.readInt();
					if(peerId == Constants.Comms.NO_PEER_ID){
						peerId = lastPeerId;
					}
					boolean[] states = new boolean[Constants.MonitorStates.TOTAL_STATES];
					for(int i = 0; i < Constants.MonitorStates.TOTAL_STATES; i++){
						states[i] = dis.readBoolean();
					}
					log("changing states for id:"+peerId+" to "+buildStringDescriptionOfStates(states), LOG_LVL.L1);
					for(Peer p:peers){
						if(p.id == peerId){
							p.setMonitorStates(states);
							break;
						}
					}
					break;

				case Constants.Comms.REQUEST_NAMES:
					sendParticipantNames = true;
					break;
				}	
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void pingPeer(Peer p){
		try{
			DatagramPacket pingPacket = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
			ByteArrayOutputStream baos = new ByteArrayOutputStream(Constants.Comms.MAX_PACKET_SIZE);
			DataOutputStream dos = new DataOutputStream(baos);
			dos.write(Constants.Comms.PING);
			dos.writeInt(p.id);
			pingPacket.setData(baos.toByteArray());
			dos.close();
			pingPacket.setPort(p.port);
			pingPacket.setAddress(p.address);
			sendingSocket.send(pingPacket);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private void setWaiting(Peer p){
		try{
			DatagramPacket pingPacket = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
			ByteArrayOutputStream baos = new ByteArrayOutputStream(Constants.Comms.MAX_PACKET_SIZE);
			DataOutputStream dos = new DataOutputStream(baos);
			dos.write(Constants.Comms.WAITING);
			dos.writeInt(p.id);
			pingPacket.setData(baos.toByteArray());
			dos.close();
			pingPacket.setPort(p.port);
			pingPacket.setAddress(p.address);
			sendingSocket.send(pingPacket);
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	/**Class which holds details about a connected peer*/
	class Peer{
		/**The ID of this peer*/
		int id;
		/**The port the peer is receiving on*/
		int port;
		/**The InetAddress the peer is receiving on*/
		InetAddress address;
		/**The list of states the peer cares about */
		boolean[] monitorStates;
		public Peer(int port, InetAddress address,int id){this.port = port;this.address = address;this.id = id; monitorStates = new boolean[Constants.MonitorStates.TOTAL_STATES];}
		/**Map of format strings*/
		public HashMap<Integer,String> dataFormats = new HashMap<Integer,String>();
		/**Change this peers monitor states*/
		public void setMonitorStates(boolean[] states){this.monitorStates = states;}
	}

	/**Build a readable string from the monitor states*/
	private String buildStringDescriptionOfStates(boolean[] states){
		StringBuilder b = new StringBuilder();

		b.append("[GameState=").append(states[Constants.MonitorStates.GAME_STATE]).append(", ")
		.append("Input=").append(states[Constants.MonitorStates.UNFILTERED_INPUT]).append(", ")
		.append("Names=").append(states[Constants.MonitorStates.NAMES]).append(", ")
		.append("Times=").append(states[Constants.MonitorStates.TIMES]).append(", ")
		.append("RaceInfo=").append(states[Constants.MonitorStates.RACE_INFO]).append(", ")
		.append("CarInfo=").append(states[Constants.MonitorStates.CAR_INFO]).append("]");

		return b.toString();
	}

	@Override
	public void shutdown(){
		if(running){
			for(Peer p : peers){
				try{
					DatagramPacket pingPacket = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
					ByteArrayOutputStream baos = new ByteArrayOutputStream(Constants.Comms.MAX_PACKET_SIZE);
					DataOutputStream dos = new DataOutputStream(baos);
					dos.write(Constants.Comms.REMOVE_CONNECTION);
					dos.writeInt(p.id);;
					pingPacket.setData(baos.toByteArray());
					dos.close();
					pingPacket.setPort(p.port);
					pingPacket.setAddress(p.address);
					sendingSocket.send(pingPacket);
				}catch(IOException e){
					log("Could not send close packet!", LOG_LVL.L1);
				}
			}

			running = false;
			listening = false;
			listenSocket.close();
			sendingSocket.close();
			
			super.doShutdown();

			connectionStateChange("Not running", CONNECTION_STATE.DISCONNECTED);
			messager.logMessage("Stopping wifi server");
		}
	}
}