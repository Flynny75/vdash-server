package data;

import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;
import gui.LogMessager;

public class IRacingDataSource extends DataSource{
	
	private final String fileMappingObjName = "Local\\IRSDKMemMapFileName";
	private long filePtr;
	
	static {
		System.loadLibrary("data_IRacingDataSource"+System.getProperty("sun.arch.data.model"));
		findFieldIDs();
	}

	public IRacingDataSource(LogMessager m) {
		super(m);
		//type = GAME_TYPE.IR;
	}
	
	@Override
	public void open() {
		if(simulate)return;

		//filePtr = openFileMapping(fileMappingObjName);
		boolean r = irsdkStartup();
		if(r) {
//			int viewPtr = mapViewOfFile(filePtr);
//			if(viewPtr != 0) {
				ready = true;
				logMessage("Opened iRacing memory file successfully", LOG_LVL.L0);
//			}else{
//				//Could find file, but could not map/read
//				ready = false;
//			}
		}else{
			ready = false;
			if(!loggedOpenFailure){
				String s1 = "\n-- WARNING --";
				String s2 = "Could not open iRacing memory file. Is it running? Is shared memory enabled?";
				String s3 = "Will retry on new client connect.";
				String s4 = "-------------\n";

				logMessage(s1, LOG_LVL.L0);
				logMessage(s2, LOG_LVL.L0);
				logMessage(s3, LOG_LVL.L0);
				logMessage(s4, LOG_LVL.L0);
				loggedOpenFailure = true;
			}
		}
	}

	/**Close the handle to the MMF*/
	@Override
	public void doClose(){
		closeHandle(filePtr);
	}

	private static native boolean irsdkStartup();
	private static native int mapViewOfFile(long hFileMappingObj);
	private static native boolean closeHandle(long hObject);
	private static native void findFieldIDs();
	private static native VDashData getData();
	/**
	 * Get a new PCars data class.
	 * This instance is created and populated by the JNI code
	 * @throws NullSimulationFileException 
	 */
	@Override
	public VDashData readData() throws NullSimulationFileException{
		if(ready){
			data =  getData();
			data.setViewedParticipantIndex(0);
			data.setGameType(type);

			//Set lastSectorTime (from AC)
			if(data.getCurrentSector() == 0){
				data.setLastSectorTime(0);
			}else if(data.getCurrentSector() == 1){
				data.setLastSectorTime(data.getCurrentSector1Time());
			}else if(data.getCurrentSector() == 2){
				data.setLastSectorTime(data.getCurrentSector2Time());
			}

			return super.readData();
		}else{
			return super.readData();
		}
	}

}
