package data;

import gui.SettingsDialog;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;

import javax.swing.JOptionPane;

import net.java.games.input.Component;
import net.java.games.input.Controller;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.Transient;
import org.simpleframework.xml.core.Persister;

import data.VDashData.GAME_TYPE;

public class Settings {
	@Transient
	private String SETTINGS_FILENAME = "settings.txt";
	@Transient
	private String SIM_DATA_FILENAME = "sim_data.pcars"; 

	private static Settings instance;
	
	public static enum LOG_LVL{
		L0, //ALWAYS DISPLAY 
		L1, //PROCESS INCOMMING COMMANDS
		L2, //DISPLAY DATA AS MESSAGES
		L3;
	public boolean shouldLog(LOG_LVL lvl){
		int indexOfSelf = 0;
		for(int i = 0; i < LOG_LVL.values().length; i++){
			if(LOG_LVL.values()[i] == this){
				indexOfSelf = i;
				break;
			}
		}
		
		int indexOfComparator = 0;
		for(int i = 0; i < LOG_LVL.values().length; i++){
			if(LOG_LVL.values()[i] == lvl){
				indexOfComparator = i;
				break;
			}
		}
		
		if(indexOfSelf >= indexOfComparator)return true;
		
		return false;
	}
	};

	public static enum CONNECTION_TYPE {
		USB, WiFi
	};

	@Element(required=false)
	private boolean autoConnect = false;
	@Element(required=false)
	private CONNECTION_TYPE autoConnectType = null;
	@Element(required=false)
	private Integer autoConnectPort = null;
	@Element(required=false)
	private GAME_TYPE gameType = GAME_TYPE.ProjectCARS; 
	@Element(required=false)
	private boolean killAdbOnExit = true;
	@Element(required=false)
	private boolean simulateData = false;
	@ElementList(required=false)
	private LinkedList<ControllerConfig> controllerConfigs = new LinkedList<ControllerConfig>();
	@Element(required=false)
	private LOG_LVL logLvl = LOG_LVL.L0;

	@Element(required=false)
	public int dataPeriod = 12;
	
	@ElementMap(required=false, key="key")
	private HashMap<GAME_TYPE, GameSettings> gameSettings = new HashMap<GAME_TYPE, GameSettings>();
	
	@Element(required=false)
	private String wirelessInterfaceAddress = null;
	
	public static Settings getInstance(){
		if(instance == null){
			instance = new Settings();
		}
		return instance;
	}

	public void setReplaceControllerConfig(Controller c, Component comp, int event){
		for(ControllerConfig conf:controllerConfigs){
			if(conf.event == event){
				controllerConfigs.remove(conf);
				InputDeviceManager.getInstance().removeWatchersForEvent(event);
				break;
			}
		}
		ControllerConfig config = new ControllerConfig();
		config.event = event;
		config.controllerName = c.getName();
		config.componentName = comp.getName();
		config.value = comp.getPollData();
		controllerConfigs.add(config);
	}

	public ControllerConfig getControllerConfigForEvent(int event){
		for(ControllerConfig config : controllerConfigs){
			if(config.event == event){
				return config;
			}
		}
		return null;
	}

	public void saveSimData(SettingsDialog dialog, VDashData sim){		
		String filename = (String)JOptionPane.showInputDialog(
                dialog,
                "Enter a filename",
                "Save data",
                JOptionPane.PLAIN_MESSAGE,
                null,
                null,
                sim.getGameType()+"_capture");
		
		File f = new File(new File(getFilePath()),filename+".xml");
		Serializer s = new Persister();

		try {
			s.write(sim, f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public VDashData getSimData(String filename){
		VDashData returnData = null;
		
		File simFile = new File(new File(getFilePath()),filename);
		if(simFile.exists()){
			Serializer s = new Persister();
			try {
				returnData = s.read(VDashData.class, simFile, false);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Could not load data file");
			}
		}else{
			System.out.println("Could not find file specified. ("+filename+")");
		}
		
		return returnData;
	}

	public void saveSettings(){
		String path = getFilePath();

		System.out.println("SAVE SETTINGS TO: "+path);
		File f = new File(new File(path),SETTINGS_FILENAME);
		Serializer s = new Persister();

		try {
			s.write(this, f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void reloadFromFile(){
		String path = getFilePath();
		System.out.println("LOAD SETTINGS FROM: "+path);

		File f = new File(new File(path),SETTINGS_FILENAME);

		if(f.exists()){
			Serializer s = new Persister();
			try {
				Settings.instance = s.read(Settings.class, f, false);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Could not load settings!");
			}
		}else{
			System.out.println("No settings to load! Using default values");
		}
	}

	private String getFilePath(){
		URL location = Settings.class.getProtectionDomain().getCodeSource().getLocation();
		String path = location.getPath();
		int lastSep = path.lastIndexOf("/");
		return path.substring(0, lastSep);
	}
	
	public boolean getAutoConnect(){return autoConnect;}
	public void setAutoConnect(boolean autoConnect){this.autoConnect = autoConnect;}
	
	public CONNECTION_TYPE getAutoConnectType(){return autoConnectType;}
	public void setAutoConnectType(CONNECTION_TYPE type){this.autoConnectType = type;}
	
	public Integer getAutoConnectPort(){return autoConnectPort;}
	public void setAutoConnectPort(Integer port){this.autoConnectPort = port;}
	
	public GAME_TYPE getGameType(){return gameType;}
	public void setGameType(GAME_TYPE type){this.gameType = type;}
	
	public boolean getKillCleanAdbOnExit(){return killAdbOnExit;}
	public void setKillCleanAdbOnExit(boolean killClean){this.killAdbOnExit = killClean;}

	public void setLogLevel(LOG_LVL level){this.logLvl = level;}
	public LOG_LVL getLogLevel(){return this.logLvl;}
	
	public void setDataPeriod(int p){this.dataPeriod = p;}
	public long getDataPeriod() {return dataPeriod;}
	
	public void setGameSettingsIp(String ip){
		GameSettings gs = gameSettings.get(getGameType());
		if(gs == null){
			gs = new GameSettings();
		}
		gs.gameSettingsIp = ip;
		gameSettings.put(getGameType(), gs);
	}
	public String getGameSettingsIp(){
		GameSettings gs = gameSettings.get(getGameType());
		if(gs == null){
			gs = new GameSettings();
			gameSettings.put(getGameType(), gs);
		}
		return gs.gameSettingsIp;
	}
	
	public void setGameSettingsPort(String port){
		GameSettings gs = gameSettings.get(getGameType());
		if(gs == null){
			gs = new GameSettings();
		}
		gs.gameSettingsPort = port;
		gameSettings.put(getGameType(), gs);
	}
	public String getGameSettingsPort(){
		GameSettings gs = gameSettings.get(getGameType());
		if(gs == null){
			gs = new GameSettings();
			gameSettings.put(getGameType(), gs);
		}
		return gs.gameSettingsPort;
	}
	
	public void setSimulateData(boolean sim){
		this.simulateData = sim;
	}
	public boolean shouldSimulateData(){
		return this.simulateData;
	}
	
	public String getWirelessInterfaceAddress(){return this.wirelessInterfaceAddress;}
	public void setWirelssInterfaceAddress(String s){this.wirelessInterfaceAddress = s;}
	
	public static class ControllerConfig{
		public String controllerName,componentName;
		float value;
		int event;
	}
	
	public static class GameSettings{
		public String gameSettingsIp = "127.0.0.1";
		public String gameSettingsPort = "20777";
	}
}
