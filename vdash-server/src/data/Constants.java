package data;

public class Constants {

	/**All of these types REQUIRE that you supply MONITOR STATES*/
	public static class Comms{
		/**The name of the service to register and look for*/
		public static final String SERVICE_NAME = "_pcars_javaserver._tcp.local.";
		public static final int SERVER_LISTEN_PORT = 1234;
		public static final int MAX_PACKET_SIZE = 5000;
		/**followed by the ID of the peer*/
		public static final int PING = -1;
		/**followed by a NO_PEER_ID (new peer has no ID assigned) and the monitoring states*/
		public static final int NEW_CONNECTION = 1;
		/**followed by peerID*/
		public static final int REMOVE_CONNECTION = 2;
		/**Supply the PeerID first, then the list of states*/
		public static final int CHANGE_MONITOR_STATE = 3;
		/**This packet is the ID, and the PCarsData data*/
		public static final int PCARS_DATA = 4;
		/**Followed by an InputEvent type (below)*/
		public static final int INPUT_EVENT = 6;
		/**Not followed by anything, use by client to tell server to send the names again*/
		public static final int REQUEST_NAMES = 7;
		/**Sent immediatly after a connect, not folled by anything*/
		public static final int WAITING = 8;
		/**Use this when the server has not issued a peerID yet*/
		public static final int NO_PEER_ID = -1;
	}
	
	/**This type REQUIRES the peerID then a boolean[TOTAL_STATES] declaring what the peer wants to monitor*/
	public static class MonitorStates{
		public static final int TOTAL_STATES = 9;
		public static final int GAME_STATE = 0;
		public static final int UNFILTERED_INPUT = 1;
		public static final int NAMES = 2;
		public static final int TIMES = 3;
		public static final int RACE_INFO = 4;
		public static final int CAR_INFO = 5;
		public static final int MOTION_AND_DEVICE = 6;
		public static final int WHEELS_TYRES = 7;
		public static final int WEATHER = 8;
	}
	
	public static class InputEvent{
		/**Move to the dash on the left*/
		public static final int EVENT_DASH_LEFT = 1;
		/**Move to the dash on the right*/
		public static final int EVENT_DASH_RIGHT = 2;
		/**'Touch' the view on the dash that has been assigned*/
		public static final int EVENT_DASH_TOUCH = 3;
		public static final int EVENT_DASH_DRAWGROUP_NEXT = 4;
		public static final int EVENT_DASH_DRAWGROUP_PREVIOUS = 5;
	}

}
