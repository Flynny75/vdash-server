package data;

import java.util.LinkedList;

import data.Settings.ControllerConfig;

import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;

public class InputDeviceManager {
	
	/**Receive events from USB game controllers*/
	public interface EventFromInputDeviceListener{
		/**Receive an event from a game controller
		 * @param event - Type of event
		 */
		public void receiveInputEvent(int event);
	}
	
	private static InputDeviceManager instance;
	
	public static InputDeviceManager getInstance(){
		if(instance == null){
			instance = new InputDeviceManager();
		}
		return instance;
	}

	public static final int EVENT_DASH_LEFT = 1;
	public static final int EVENT_DASH_RIGHT = 2;
	public static final int EVENT_DASH_TOUCH = 3;
	public static final int EVENT_DASH_DRAWGROUP_PREV = 4;
	public static final int EVENT_DASH_DRAWGROUP_NEXT = 5;
	
	private int[] events = new int[]{EVENT_DASH_LEFT,EVENT_DASH_RIGHT,EVENT_DASH_TOUCH,EVENT_DASH_DRAWGROUP_PREV, EVENT_DASH_DRAWGROUP_NEXT};

	private LinkedList<Controller> controllers = new LinkedList<Controller>();
	private LinkedList<InputWatcherThread> watchers = new LinkedList<InputWatcherThread>();

	private EventFromInputDeviceListener eventListener;
	
	public void setEventListener(EventFromInputDeviceListener listener){
		eventListener = listener;
	}
	
	public void attempRestoreFromSettings(){
		Controller[] ca = ControllerEnvironment.getDefaultEnvironment().getControllers();
		Component[] components;
		
		Settings s = Settings.getInstance();
		
		for(int e:events){
			ControllerConfig config = s.getControllerConfigForEvent(e);
			if(config != null){
				for(Controller c:ca){
					if(c.getName().equals(config.controllerName)){
						components = c.getComponents();
						for(Component comp:components){
							if(comp.getName().equals(config.componentName)){
								//Guess we have found it? HEX UIDs would be nicer...
								ComponentEvent ce = new ComponentEvent(comp, e);
								ce.setValue(config.value);
								addControllerConfigForEvent(e, c, ce);
								break;
							}
						}
					}
				}
			}
		}
	}
	
	public boolean checkControllerForEvent(int event){
		for(InputWatcherThread watcher:watchers){
			LinkedList<ComponentEvent>ces = watcher.getCompnentEvents();
			for(ComponentEvent ce:ces){
				if(ce.event == event){
					return true;
				}
			}
		}
		return false;
	}
	
	public void dropAllControllers(){
		for(InputWatcherThread watcher:watchers){
			watcher.interrupt();
		}
	}

	public void addControllerComponentForEvent(Controller cont, Component comp, int event){
		if(event != EVENT_DASH_LEFT &&
				event != EVENT_DASH_RIGHT &&
				event != EVENT_DASH_TOUCH &&
				event != EVENT_DASH_DRAWGROUP_PREV &&
				event != EVENT_DASH_DRAWGROUP_NEXT){
			throw new IllegalArgumentException(""+event+" is not a valid event!");
		}

		ComponentEvent ce = new ComponentEvent(comp, event);
		addControllerConfigForEvent(event, cont, ce);
	}
	
	private void addControllerConfigForEvent(int event, Controller cont, ComponentEvent ce){
		if(controllers.contains(cont)){
			//Add this component to the thread for this controller, for this event
			//need to find the right watcher thread.
			for(InputWatcherThread watcher:watchers){
				if(watcher.getController() == cont){
					watcher.addComponentEvent(ce);
					break;
				}
			}
		}else{
			controllers.add(cont);
			//Start new thread for that controller, with component, for event
			InputWatcherThread t = new InputWatcherThread(cont, ce);
			watchers.add(t);
			t.start();
		}
	}

	public void removeWatchersForEvent(int event){
		for(InputWatcherThread watcher:watchers){
			watcher.removeComponentEvent(event);
			//for(ComponentEvent ce:watcher.getCompnentEvents()){
			//	if(ce.event == event){
			//		watcher.removeComponentEvent(ce.event);
					//watcher.interrupt();
					//watchers.remove(watcher);
					//controllers.remove(watcher.getController());
			//		break;
			//	}
			//}
		}
	}

	private class InputWatcherThread extends Thread{
		private Controller cont;
		private LinkedList<ComponentEvent> componentEvents = new LinkedList<ComponentEvent>();
		
		public InputWatcherThread(Controller cont, ComponentEvent ce){
			this.cont = cont;
			addComponentEvent(ce);
		}

		public void addComponentEvent(ComponentEvent event){
			componentEvents.add(event);
		}
		
		public LinkedList<ComponentEvent> getCompnentEvents(){
			return componentEvents;
		}
		
		public void removeComponentEvent(int event){
			for(ComponentEvent ce : componentEvents){
				if(ce.event == event){
					componentEvents.remove(ce);
					return;
				}
			}
		}

		public Controller getController(){
			return cont;
		}

		@Override
		public void run() {
			EventQueue q = cont.getEventQueue();
			Event e = new Event();

			while(!Thread.interrupted()){
				cont.poll();
				while(q.getNextEvent(e)){
					Component comp = e.getComponent();
					for(ComponentEvent ce:componentEvents){
						if(comp.getIdentifier() == ce.component.getIdentifier()){
							if(comp.getPollData() == ce.pollData){
								if(System.currentTimeMillis() > (ce.lastTime + ComponentEvent.THRESH_TIME)){
									ce.lastTime = System.currentTimeMillis();
									eventListener.receiveInputEvent(ce.event);
								}
								break;
							}
						}
					}
				}
				try {
					Thread.yield();
					Thread.sleep(100);
				} catch (InterruptedException e1) {
					break;
				}
			}
		}
	}

	private class ComponentEvent{
		public long lastTime;
		//TODO expose
		public static final long THRESH_TIME = 200; //ms
		public Component component;
		public float pollData;
		public int event;

		public ComponentEvent(Component comp, int event){
			this.component = comp;
			this.event = event;
			pollData = comp.getPollData();
			System.out.println("NEW COMP EVENT: "+pollData);
		}
		
		public void setValue(float value){
			System.out.println("Modified to: "+value);
			this.pollData = value;
		}
	}
}
