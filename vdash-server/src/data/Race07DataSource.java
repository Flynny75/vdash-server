package data;

import data.VDashData.GAME_TYPE;
import gui.LogMessager;

public class Race07DataSource extends SimBinDataSource{

	private static final String fileMappingObjName = "$Race$";
	
	public Race07DataSource(LogMessager m){
		super(m, GAME_TYPE.RACE07, fileMappingObjName);
	}
}
