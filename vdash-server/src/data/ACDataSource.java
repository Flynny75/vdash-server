package data;

import gui.LogMessager;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;

import comms.LittleEndianInputStream;
import comms.LittleEndianOutputStream;
import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;

public class ACDataSource extends DataSource{

	private static final String AC_PHYSICS_FILENAME = "Local\\acpmf_physics";
	private static final String AC_GRAPHICS_FILENAME = "Local\\acpmf_graphics";
	private static final String AC_STATICS_FILENAME = "Local\\acpmf_static";

	private enum STATE{NONE, HS, HS_ACK, REG, LISTEN, CLOSE};

	private long acPhysicsFilePointer;
	private long acGraphicsFilePointer;
	private long acStaticsFilePointer;

	private float s1Time, s2Time;

	private char[] carName, driverName, trackName, trackConfig;
	private int identity, version;

	private byte[] handshakePacket, registerUpdatePacket, registerSpotPacket, disconnectPacket;
	private byte[] receiveData = new byte[512];
	private DatagramPacket receivePacket = new DatagramPacket(receiveData, 512);
	private LinkedBlockingQueue<DatagramPacket> sendingQueue = new LinkedBlockingQueue<DatagramPacket>();

	private Timer handshakeTimer;

	private DatagramSocket socket;
	private Thread listenThread;
	private Thread sendThread;
	
	private ACUdpData udpData;

	private STATE state = STATE.NONE;

	private Runnable sendRunnable = new Runnable(){
		@Override
		public void run(){
			while(state != STATE.CLOSE){
				try{
					DatagramPacket p = sendingQueue.take();

					socket.send(p);
				}catch(InterruptedException e){
					e.printStackTrace();
				}catch (IOException e){
					e.printStackTrace();
				}
			}
			System.out.println("AC UDP Sending thread finished");
		}
	};

	private Runnable listenRunnable = new Runnable(){
		@Override
		public void run(){
			while(state != STATE.CLOSE){
				try{
					socket.receive(receivePacket);

					switch(state){
					case HS:{
						//This should be a HS_ACK
						readHandshakeAck();

						state = STATE.HS_ACK;
						state = STATE.REG;
						//send register packets
						System.out.println("SEND CONFIRM");
						sendData(registerUpdatePacket);
						//sendData(registerSpotPacket);
						break;}
					case REG:{
						System.out.println("CONNECTED");
						state = STATE.LISTEN;}
					//And fall through to listen
					case LISTEN:{
						parseACData();
						break;}

					}
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			System.out.println("AC UDP Listen thread finished");
		}
	};

	static {
		System.loadLibrary("data_ACDataSource"+System.getProperty("sun.arch.data.model"));
		findFieldIDs();
	}

	public ACDataSource(LogMessager m) {
		super(m);
		type = GAME_TYPE.AC;
	}

	@Override
	public void open(){
		if(simulate)return;

		if(state == STATE.NONE){
			//No state, start sending handshake reqs
			state = STATE.HS;

			makePackets();

			try{
				socket = new DatagramSocket();
			}catch(SocketException e){
				e.printStackTrace();
			}

			sendThread = new Thread(sendRunnable);
			listenThread = new Thread(listenRunnable);

			sendThread.start();
			listenThread.start();

			handshakeTimer = new Timer();
			handshakeTimer.schedule(makeHandshakeSendTask(), 1000);
		}

		//make handshake packet
		//mak register packet
		//start listener thread
		//start handshake send thread
		//every X ms
		//	if no handshake ack, send another
		//ELSE
		//	send register packet
		//
		//listen

		acGraphicsFilePointer = ACDataSource.openFileMapping(AC_GRAPHICS_FILENAME);
		acPhysicsFilePointer = ACDataSource.openFileMapping(AC_PHYSICS_FILENAME);
		acStaticsFilePointer = ACDataSource.openFileMapping(AC_STATICS_FILENAME);

		if(acGraphicsFilePointer != 0 && acPhysicsFilePointer != 0 && acStaticsFilePointer != 0){
			int openResult = ACDataSource.mapViewOfFile(acPhysicsFilePointer, acGraphicsFilePointer, acStaticsFilePointer);
			if(openResult != 0){
				ready = true;
				logMessage("Opened AC memory file successfully", LOG_LVL.L0);
			}else{
				ready = false;
			}
		}else{
			ready = false;
			if(!loggedOpenFailure){
				String s1 = "\n-- WARNING --";
				String s2 = "Could not open Assetto Corsa memory files. Is it running? Is API enabled?";
				String s3 = "Will retry on new client connect.";
				String s4 = "-------------\n";

				logMessage(s1, LOG_LVL.L0);
				logMessage(s2, LOG_LVL.L0);
				logMessage(s3, LOG_LVL.L0);
				logMessage(s4, LOG_LVL.L0);
				loggedOpenFailure = true;
			}
		}
	}

	@Override
	public void doClose() {
		ACDataSource.closeHandle(acGraphicsFilePointer);
		ACDataSource.closeHandle(acPhysicsFilePointer);
		ACDataSource.closeHandle(acStaticsFilePointer);

		state = STATE.CLOSE;
		sendData(disconnectPacket);
		socket.close();
	}

	@Override
	public VDashData readData() throws NullSimulationFileException{
		if(simulate){
			if(simulatedData == null){
				simulatedData = Settings.getInstance().getSimData(simulateFilename);
			}
			if(simulatedData == null){
				logMessage("No simulation file for AC", LOG_LVL.L0);
				throw new NullSimulationFileException(type);
			}
			return simulatedData;
		}
		if(ready){
			data =  getData();
			data.setGameType(type);
			
			addUdpData();

			//Have to manage separate sector times here
			if(data.getCurrentSector() == 0){
				data.setLastSectorTime(0);
			}else if(data.getCurrentSector() == 1){
				s1Time = data.getLastSectorTime();
			}else if(data.getCurrentSector() == 2){
				s2Time = data.getLastSectorTime();
			}

			data.setCurrentSector1Time(s1Time);
			data.setCurrentSector2Time(s2Time);

			//NORMALISE G FORCES
			float[] lacc = data.getLocalAcceleration();
			for(int i = 0; i < 3; i++){
				lacc[i] = lacc[i] * 9.80665f;
			}
			lacc[VDashData.VECTOR.VEC_Z] = -lacc[VDashData.VECTOR.VEC_Z];
			data.setLocalAcceleration(lacc);

			//APPEND UDP DATA HERE
			data.setCarName(carName);
			data.setTrackLocation(trackName);
			data.setTrackVariation(trackConfig);

			data.getParticipants()[0].setName(driverName);

			return data;
		}else{
			return null;
		}
	}
	
	private void addUdpData(){
		if(udpData == null){
			//logMessage("No UDP data to append", LOG_LVL.L3);
			return;
		}
		
		data.setClutch(udpData.clutch);
		data.setAbsEnabled(udpData.isAbsEnabled);
		data.setAbsActive(udpData.isAbsActive);
		
		data.setTcEnabled(udpData.isTcEnabled);
		data.setTcActive(udpData.isTcnActive);
		
		data.setSpeedLimited(udpData.isEngineLimited);
		data.getParticipants()[0].setWorldPosition(udpData.carCoordinates);
	}

	private void parseACData(){
		LittleEndianInputStream dis = new LittleEndianInputStream(new ByteArrayInputStream(receivePacket.getData()));
		int receivedBytes = receivePacket.getLength();

		try{
			ACUdpData udpData = new ACUdpData();
			udpData.ident = dis.readChar();
			udpData.size = dis.readInt();

			udpData.speedKmh = dis.readFloat();
			udpData.speedMph = dis.readFloat();
			udpData.speedMs = dis.readFloat();

			udpData.isAbsEnabled = dis.readBoolean();
			udpData.isAbsActive = dis.readBoolean();
			udpData.isTcnActive = dis.readBoolean();
			udpData.isTcEnabled = dis.readBoolean();
			udpData.isInPit = dis.readBoolean();
			udpData.isEngineLimited = dis.readBoolean();

			udpData.accX = dis.readFloat();
			udpData.accZ = dis.readFloat();
			udpData.accY = dis.readFloat();

			udpData.lapTime = dis.readInt();
			udpData.lastLapTime = dis.readInt();
			udpData.bestLapTime = dis.readInt();
			udpData.lapCount = dis.readInt();

			udpData.throttle = dis.readFloat();
			udpData.brake = dis.readFloat();
			udpData.clutch = dis.readFloat();
			udpData.rpm = dis.readFloat();
			udpData.steer = dis.readFloat();
			udpData.gear = dis.readInt();
			udpData.cgHeight = dis.readFloat();

			udpData.wheelAngularSpeed = readXFloats(dis, 4);
			udpData.slipAngle = readXFloats(dis, 4);
			udpData.slipAngle_ContactPatch = readXFloats(dis, 4);
			udpData.slipRatio = readXFloats(dis, 4);
			udpData.tyreSlip = readXFloats(dis, 4);
			udpData.ndSlip = readXFloats(dis, 4);
			udpData.load = readXFloats(dis, 4);
			udpData.Dy = readXFloats(dis, 4);
			udpData.Mz = readXFloats(dis, 4);
			udpData.tyreDirtyLevel = readXFloats(dis, 4);

			udpData.camberRAD = readXFloats(dis, 4);
			udpData.tyreRadius = readXFloats(dis, 4);
			udpData.tyreLoadedRadius = readXFloats(dis, 4);
			udpData.suspensionHeight = readXFloats(dis, 4);
			udpData.carPositionNormalized = dis.readFloat();
			udpData.carSlope = dis.readFloat();
			
			readXFloats(dis, 1);
			
			udpData.carCoordinates = readXFloats(dis, 3);
			
			//Swap the position arrays
			float temp = udpData.carCoordinates[0];
			udpData.carCoordinates[0] = udpData.carCoordinates[2];
			udpData.carCoordinates[2] = temp;
			
			this.udpData = udpData;
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private float[] readXFloats(LittleEndianInputStream dis, int X) throws IOException{
		float[] floats = new float[X];
		
		for(int i = 0; i < X; i++){
			floats[i] = dis.readFloat();
		}
		
		return floats;
	}

	private TimerTask makeHandshakeSendTask(){
		return new TimerTask() {
			@Override
			public void run() {
				if(state == STATE.HS){
					System.out.println("SEND HANDSHAKE");
					sendData(handshakePacket);
					handshakeTimer.schedule(makeHandshakeSendTask(), 1000);
				}else{
					System.out.println("HANDSHAKE TASK DONE");
				}

			}
		};
	}

	private void sendData(byte[] data){
		try{
			DatagramPacket p = new DatagramPacket(data, 0, data.length, InetAddress.getLocalHost(), 9996);
			sendingQueue.add(p);
		}catch(UnknownHostException e){
			e.printStackTrace();
		}
	}

	private void makePackets(){
		try{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			LittleEndianOutputStream dos = new LittleEndianOutputStream(baos);
			dos.writeInt(3);
			dos.writeInt(1);
			dos.writeInt(0);
			handshakePacket = baos.toByteArray();
			
			baos.reset();
			dos.writeInt(3);
			dos.writeInt(1);
			dos.writeInt(1);
			registerUpdatePacket = baos.toByteArray();
			
			baos.reset();
			dos.writeInt(3);
			dos.writeInt(1);
			dos.writeInt(2);
			registerSpotPacket = baos.toByteArray();
			
			baos.reset();
			dos.writeInt(3);
			dos.writeInt(1);
			dos.writeInt(3);
			disconnectPacket = baos.toByteArray();

			dos.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private void readHandshakeAck(){
		System.out.println("PARSE HANDSHAKE ACK");
		
		byte[] data = receivePacket.getData();
		carName = new char[50];
		driverName = new char[50];
		trackName = new char[50];
		trackConfig = new char[50];

		LittleEndianInputStream dis = new LittleEndianInputStream(new ByteArrayInputStream(data));
		try{
			for(int i = 0; i < 50; i++){
				carName[i] = dis.readChar();
			}

			for(int i = 0; i < 50; i++){
				driverName[i] = dis.readChar();
			}

			identity = dis.readInt();
			version = dis.readInt();

			for(int i = 0; i < 50; i++){
				trackName[i] = dis.readChar();
			}

			for(int i = 0; i < 50; i++){
				trackConfig[i] = dis.readChar();
			}

			carName = new String(carName).split("%")[0].replace('_', ' ').toCharArray();
			driverName = new String(driverName).split("%")[0].replace('_', ' ').toCharArray();
			trackName = new String(trackName).split("%")[0].replace('_', ' ').toCharArray();
			trackConfig = new String(trackConfig).split("%")[0].replace('_', ' ').toCharArray();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private class ACUdpData{
		char ident;
		int size;

		float speedKmh;
		float speedMph;
		float speedMs;

		boolean isAbsEnabled;
		boolean isAbsActive;
		boolean isTcnActive;
		boolean isTcEnabled;
		boolean isInPit;
		boolean isEngineLimited;

		float accX;
		float accZ;
		float accY;

		int lapTime;
		int lastLapTime;
		int bestLapTime;
		int lapCount;

		float throttle;
		float brake;
		float clutch;
		float rpm;
		float steer;
		int gear;
		float cgHeight;

		float[] wheelAngularSpeed;
		float[] slipAngle;
		float[] slipAngle_ContactPatch;
		float[] slipRatio;
		float[] tyreSlip;
		float[] ndSlip;
		float[] load;
		float[] Dy;
		float[] Mz;
		float[] tyreDirtyLevel;

		float[] camberRAD;
		float[] tyreRadius;
		float[] tyreLoadedRadius;
		float[] suspensionHeight;
		float carPositionNormalized;
		float carSlope;
		float[] carCoordinates;
	}

	private static native long openFileMapping(String name);
	private static native int mapViewOfFile(long acPhysicsFile, long acGraphicsFile, long acStaticsFile);
	private static native boolean closeHandle(long hObject);
	private static native void findFieldIDs();
	private static native VDashData getData();
}
