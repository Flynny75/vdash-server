package data;

import gui.LogMessager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;

/**
 * TODO https://www.lfsforum.net/showthread.php?p=1586792#post1586792
 * @author Alex
 * F1 2013 & GRID2 use extradata 3
 * F1 2012 - http://www.robertgray.net.au/posts/codemasters-f1-data-feed-updated#.U7RJhvldWSp (extradata 2)
 * F1 2010/2011 use extradata 1
 * RBR?
 *
 */
public class F12010DataSource extends DataSource {

	private static final int DIRT_3_PORT = 20777;
	private DatagramSocket socket;
	
	private boolean running = false;
	
	private Thread listenThread;
	private byte[] processingData = new byte[Constants.Comms.MAX_PACKET_SIZE];
	private ByteBuffer buffer;
	
	private Runnable listenRunnable = new Runnable() {
		@Override
		public void run() {
			logMessage("Starting F12010 listen thread", LOG_LVL.L0);
			while(running && !Thread.interrupted()){
				ready = true;
				DatagramPacket packet = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
				try {
					socket.receive(packet);
					readDirtData(packet.getData());
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	};
	
	public F12010DataSource(LogMessager m) throws IOException {
		super(m);
		//TODO
		//type = GAME_TYPE.
	}

	@Override
	public void open() throws IOException{
		try {
			socket = new DatagramSocket(DIRT_3_PORT);
			running = true;
			buffer  = ByteBuffer.wrap(processingData);
			buffer.order(ByteOrder.nativeOrder());
			data = new VDashData();
			listenThread = new Thread(listenRunnable);
			listenThread.start();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doClose(){
		running = false;
		listenThread.interrupt();
		socket.close();
	}

	private void readDirtData(byte[] dirtdata){
		buffer.clear();
		buffer.put(dirtdata);
		buffer.flip();
		
		data.setGameType(GAME_TYPE.DiRT3);
		data.setGameState(VDashData.GAME_STATES.GAME_INGAME_PLAYING);
		data.setCurrentLapTime(buffer.getFloat(1*4));
		data.setMps(buffer.getFloat(7*4));
		data.setThrottle(buffer.getFloat(29*4));
		data.setBrake(buffer.getFloat(31*4));
		data.setClutch(buffer.getFloat(32*4));
		float gear = buffer.getFloat(33*4);
		if(gear == 10)gear = -1;
		data.setGear((int)gear);
		float[] latAcc = new float[VDashData.VECTOR.VEC_MAX];
		latAcc[VDashData.VECTOR.VEC_X] = buffer.getFloat(34*4);
		latAcc[VDashData.VECTOR.VEC_Y] = buffer.getFloat(34*5);
		data.setLocalAcceleration(latAcc);
		//TODO use a participant
		//data.setCurrentLap((int)(buffer.getFloat(36*4)));
		data.setRpm(buffer.getFloat(37*4)*10);
		data.setMaxRpm(7000);
		
		logMessage("NEW DATA : Time: "+data.getCurrentLapTime()+" T: "+data.getThrottle()+" B: "+data.getBrake()+" RPM: "+data.getRpm()+" Gear: "+data.getGear()+" MPH: "+data.getMph(), LOG_LVL.L2);
	}
}
