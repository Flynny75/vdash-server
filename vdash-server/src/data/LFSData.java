package data;

import gui.LogMessager;

import java.io.IOException;

import data.Settings.LOG_LVL;
import net.sf.jinsim.SimpleClient;
import net.sf.jinsim.UDPChannel;
import net.sf.jinsim.request.InitRequest;
import net.sf.jinsim.response.InSimListener;
import net.sf.jinsim.response.InSimResponse;
import net.sf.jinsim.response.OutGaugeResponse;

public class LFSData extends DataSource implements InSimListener {

	private SimpleClient lfsClient;
	private boolean running = false;

	public LFSData(LogMessager m) {
		super(m);
		type = VDashData.GAME_TYPE.LFS;
	}

	@Override
	public void open() throws IOException {
		if(!running){
			logMessage("Starting LFS listen thread ("+Settings.getInstance().getGameSettingsIp()+":"+Settings.getInstance().getGameSettingsPort()+")", LOG_LVL.L0);
			data = new VDashData();
			lfsClient = new SimpleClient();

			// register for Errors and OutGaugeResponses
			lfsClient.addListener(this);

			int port;
			try{
				port = Integer.parseInt(Settings.getInstance().getGameSettingsPort());
			}catch(NumberFormatException e){
				throw new IOException("Cannot connect to LFS with port of: "+Settings.getInstance().getGameSettingsPort());
			}

			lfsClient.connect(new UDPChannel(Settings.getInstance().getGameSettingsIp(), port), null, "VDash", InitRequest.RECEIVE_NODE_LAP, 5, port+1);
			lfsClient.enableOutGauge(1);

			running = true;
			ready = true;
		}
	}

	@Override
	protected void doClose() {
		try {
			lfsClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void packetReceived(InSimResponse response) {
		if(response instanceof OutGaugeResponse){
			OutGaugeResponse resp  = (OutGaugeResponse)response;
			
			logMessage("Resp: SPEED: "+resp.getSpeed()+", T:"+resp.getTime(), LOG_LVL.L0);

			//TODO add more?
			data.setThrottle(resp.getThrottle());
			data.setBrake(resp.getBrake());
			data.setRpm(resp.getRpm());
			data.setMps(resp.getSpeed());
		}
	}

}
