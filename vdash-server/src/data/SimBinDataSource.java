package data;

import gui.LogMessager;
import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;

public abstract class SimBinDataSource extends DataSource{
	private String fileMappingObjName;
	
	private long filePtr;
	
	static {
		System.loadLibrary("data_SimBinDataSource"+System.getProperty("sun.arch.data.model"));
		findFieldIDs();
	}
	
	public SimBinDataSource(LogMessager m, GAME_TYPE type, String name) {
		super(m);
		this.fileMappingObjName = name;
		this.type = type;
	}

	@Override
	public void open() {
		if(simulate)return;

		filePtr = SimBinDataSource.openFileMapping(fileMappingObjName);
		if(filePtr != 0) {
			int viewPtr = SimBinDataSource.mapViewOfFile(filePtr);
			if(viewPtr != 0) {
				ready = true;

				logMessage("Opened "+type+" memory file successfully", LOG_LVL.L0);
			}else{
				//Could find file, but could not map/read
				ready = false;
			}
		}else{
			ready = false;
			if(!loggedOpenFailure){
				String s1 = "\n-- WARNING --";
				String s2 = "Could not open "+type+" memory file. Is it running? Is shared memory enabled?";
				String s3 = "Will retry on new client connect.";
				String s4 = "-------------\n";

				logMessage(s1, LOG_LVL.L0);
				logMessage(s2, LOG_LVL.L0);
				logMessage(s3, LOG_LVL.L0);
				logMessage(s4, LOG_LVL.L0);
				loggedOpenFailure = true;
			}
		}
	}

	/**Close the handle to the MMF*/
	@Override
	public void doClose(){
		SimBinDataSource.closeHandle(filePtr);
	}
	
	@Override
	public VDashData readData() throws NullSimulationFileException{
		if(simulate){
			if(simulatedData == null){
				simulatedData = Settings.getInstance().getSimData(simulateFilename);
			}
			if(simulatedData == null){
				logMessage("No "+type+" data to simulate", LOG_LVL.L0);
				throw new NullSimulationFileException(type);
			}
			return simulatedData;
		}
		if(ready){
			data =  getData();
			data.setViewedParticipantIndex(0);
			data.setGameType(type);
			
			return data;
		}else{
			return null;
		}
	}
	
	private static native long openFileMapping(String name);
	private static native int mapViewOfFile(long hFileMappingObj);
	private static native boolean closeHandle(long hObject);
	private static native void findFieldIDs();
	private static native VDashData getData();
}
