package data;

import data.VDashData.GAME_TYPE;
import gui.LogMessager;

public class Gtr2DataSource extends SimBinDataSource{

	public Gtr2DataSource(LogMessager m){
		super(m, GAME_TYPE.GTR2, "$gtr2$");
	}
}
