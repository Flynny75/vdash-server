package data;

import java.io.IOException;

import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;
import gui.LogMessager;

public abstract class DataSource {

	protected boolean ready = false;

	protected boolean simulate = false;
	protected String simulateFilename = null;
	protected VDashData simulatedData;
	protected VDashData data;

	private LogMessager messager;

	protected GAME_TYPE type;
	
	protected boolean loggedOpenFailure = false;
	
	private OnDataReadyListener dataListener; 

	public DataSource(LogMessager m){
		this.messager = m;
	}

	public abstract void open() throws IOException;

	public boolean ready(){
		if(simulate)return true;

		return ready;
	}
	public void close(){
		logMessage("Closing "+type+" data source...", LOG_LVL.L0);
		loggedOpenFailure = false;
		ready = false;

		if(simulate)return;
		
		doClose();
	}
	protected abstract void doClose();
	
	public VDashData readData() throws NullSimulationFileException{
		if(simulate){
			if(simulatedData == null){
				simulatedData = Settings.getInstance().getSimData(simulateFilename);
			}
			if(simulatedData == null){
				logMessage("No simulation file for "+type, LOG_LVL.L0);
				throw new NullSimulationFileException(type);
			}
			
			if(dataListener != null){
				dataListener.dataReady(simulatedData);
			}
			
			return simulatedData;
		}
		
		if(dataListener != null){
			dataListener.dataReady(data);
		}
		
		return data;
	}

	public void simulate(boolean sim){
		simulate(sim, type.toString()+"_capture.xml");
	}
	public void simulate(boolean sim, String filename) {
		if(this.simulate != sim){
			this.simulate = sim;
			if(sim){
				this.simulateFilename = filename;
				if(simulateFilename == null && simulate){
					throw new IllegalArgumentException("Must supply simulation filename");
				}else{
					logMessage("Loading simulated data for "+type, LOG_LVL.L0);
				}
			}else{
				close();
			}
		}
	}

	protected void logMessage(String msg, LOG_LVL lvl){
		if(messager != null){
			messager.logMessage(msg);
		}
	}

	public GAME_TYPE getType(){
		return type;
	}
	
	public void setDataListener(OnDataReadyListener listener){
		dataListener = listener;
	}
	
	public class NullSimulationFileException extends Exception{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public NullSimulationFileException(VDashData.GAME_TYPE game){
			super("Could not load simulation file for "+game);
		}
	}
	
	public interface OnDataReadyListener{
		public void dataReady(VDashData data);
	}
}
