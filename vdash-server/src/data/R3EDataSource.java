package data;

import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;
import gui.LogMessager;

public class R3EDataSource extends DataSource{

	static {
		System.loadLibrary("data_R3EDataSource"+System.getProperty("sun.arch.data.model"));
		findFieldIDs();
	}
	
	private static final String fileMappingObjName = "$Race$";
	private long filePtr;
	
	public R3EDataSource(LogMessager m){
		super(m);
		type = GAME_TYPE.R3E;
	}

	@Override
	public void open() {
		if(simulate)return;

		filePtr = R3EDataSource.openFileMapping(fileMappingObjName);
		if(filePtr != 0) {
			int viewPtr = R3EDataSource.mapViewOfFile(filePtr);
			if(viewPtr != 0) {
				ready = true;

				logMessage("Opened Project Cars memory file successfully", LOG_LVL.L0);
			}else{
				//Could find file, but could not map/read
				ready = false;
			}
		}else{
			ready = false;
			if(!loggedOpenFailure){
				String s1 = "\n-- WARNING --";
				String s2 = "Could not open Project Cars memory file. Is it running? Is shared memory enabled?";
				String s3 = "-------------\n";

				logMessage(s1, LOG_LVL.L0);
				logMessage(s2, LOG_LVL.L0);
				logMessage(s3, LOG_LVL.L0);
				loggedOpenFailure = true;
			}
		}
	}

	@Override
	public VDashData readData() throws NullSimulationFileException{
		if(simulate){
			return super.readData();
		}
		if(ready){
			data =  R3EDataSource.getData();
			data.setGameType(type);

			return super.readData();
		}else{
			return super.readData();
		}
	}
	
	@Override
	public void doClose(){
		R3EDataSource.closeHandle(filePtr);
	}
	
	private static native long openFileMapping(String name);
	private static native int mapViewOfFile(long hFileMappingObj);
	private static native boolean closeHandle(long hObject);
	private static native void findFieldIDs();
	private static native VDashData getData();
}
