package data;

import gui.LogMessager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;
import data.VDashData.Participant;

/**
 * @author Alex
 *
 */
//https://www.lfsforum.net/showthread.php?p=1586584#post1586584
/*
 * Triple post 

	F1 2010's extra data is the same size as DiRT2's, the differences are minor:
	Code:
	#	offset	type	description
	-----------------------------------
	0	0	float	time elapsed in seconds (since sending these packets started)
	1	4	float 	laptime in seconds
	2	8	float	position on track in metres from the start/finish line
	3	12	float	race progress, this - laps = lap progress
	4	16	float	world-Y position in metres
	5	20	float	world-Z position in metres
	6	24	float	world-X position in metres
	7	28	float	actual velocity in m/s
	8	32	float	world-Y velocity in m/s
	9	36	float	world-Z velocity in m/s
	10	40	float	world-X velocity in m/s

	11	44	float	?
	12	48	float	? roll *
	13	52	float	?

	14	56	float	?
	15	60	float	? pitch *
	16	64	float	?

	17	68	float	?	suspension travel front left?? *
	18	72	float	?	suspension travel front right?? *
	19	76	float	?	suspension travel rear left?? *
	20	80	float	?	suspension travel rear right?? *

	21	84	float	?
	22	88	float	?
	23	92	float	?
	24	96	float	?

	25	100	float	velocity rear left wheel in m/s *
	26	104	float	velocity rear right wheel in m/s *
	27	108	float	velocity front left wheel in m/s *
	28	112	float	velocity front right wheel in m/s *
	29	116	float	throttle 0 to 1
	30	120	float	steering in quarter turns (90�), negative = left, positive = right
	31	124	float	brakes 0 to 1
	32	128	float	anti-stall / clutch
	33	132	float	gear 0 = Neutral, 10 = Reverse, forwards speeds counting from 1
	34	136	float	lateral acceleration in g
	35	140	float	longitudinal acceleration in g
	36	144	float	lap counting from 0 (first lap = 0) *
	37	148	float	Revolutions per 6 seconds, multiply by 10.0 to get RPM
	float 33 (#32) is the only content change, representing the anti-stall or clutch in F1 instead of the handbrake (GRID/DiRT2) for obvious reasons. Other than that there is only a minor change to #33, where reverse is now 10 instead of 9. Finally, GRID and DiRT2 reset the first float (time elapsed) on restarts, F1 does not.

	I haven't fully verified everything yet, but I'm reasonably sure that's all the differences.

	I've updated the other two definitions with a more accurate interpretation of the steering value.
 */
public class Grid2DataSource extends DataSource {

	private DatagramSocket socket;

	private boolean running = false;

	private Thread listenThread;
	private byte[] processingData = new byte[Constants.Comms.MAX_PACKET_SIZE];
	private ByteBuffer buffer;

	private boolean r = false;
	private boolean closeReq = false;
	
	float maxRpm;
	float bestLapTime = -1;

	private Runnable listenRunnable = new Runnable() {
		@Override
		public void run() {
			logMessage("Starting GRID2 listen thread", LOG_LVL.L0);
			while(running && !Thread.interrupted()){
				ready = true;
				DatagramPacket packet = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
				try {
					socket.receive(packet);
					readGRID2Data(packet.getData());
				} catch (IOException e) {
					if(!closeReq){
						logMessage("GRID2 data source connection error: "+e.getMessage(), LOG_LVL.L0);
						e.printStackTrace();
					}
					close();
				}

			}
		}
	};

	public Grid2DataSource(LogMessager m){
		super(m);
		type = GAME_TYPE.GRID2;
	}

	@Override
	public void open() throws IOException{
		closeReq=false;
		if(!r){
			running = true;
			socket = new DatagramSocket(null);
			int port;
			try{
				port = Integer.valueOf(Settings.getInstance().getGameSettingsPort());
				socket.bind(new InetSocketAddress(Settings.getInstance().getGameSettingsIp(), port));
				logMessage("GRID2 data source listening on: "+socket.getLocalAddress()+":"+socket.getLocalPort(), LOG_LVL.L1);
				buffer  = ByteBuffer.wrap(processingData);
				buffer.order(ByteOrder.nativeOrder());
				data = new VDashData();
				listenThread = new Thread(listenRunnable);
				listenThread.start();
				r = true;
			}catch(NumberFormatException e){
				e.printStackTrace();
			}
		}
	}

	@Override
	public void doClose(){
		closeReq = true;
		running = false;
		listenThread.interrupt();
		socket.close();
	}

	private void readGRID2Data(byte[] dirtdata){
		buffer.clear();
		buffer.put(dirtdata);
		buffer.flip();
		
		float apiTime = buffer.getFloat(); 						//ok
		float lapTime = buffer.getFloat();						//ok
		float distanceThroughLap = buffer.getFloat();			//ok
		float completedLapsPercent = buffer.getFloat();			//ok
		float xPos = buffer.getFloat();
		float yPos = buffer.getFloat();
		float zPos = buffer.getFloat();
		float speed = buffer.getFloat();						//ok
		float xv = buffer.getFloat();
		float yv = buffer.getFloat();
		float zv = buffer.getFloat();
		float xr = buffer.getFloat();
		float yr = buffer.getFloat();
		float zr = buffer.getFloat();
		float xd = buffer.getFloat();
		float yd = buffer.getFloat();
		float zd = buffer.getFloat();
		float suspensionPosRearLeft = buffer.getFloat();
		float suspensionPosRearRight = buffer.getFloat();
		float suspensionPosFrontLeft = buffer.getFloat();
		float suspensionPosFrontRight = buffer.getFloat();
		float suspensionVelocityRearLeft = buffer.getFloat();
		float suspensionVelocityRearRight = buffer.getFloat();
		float suspensionVelocityFrontLeft = buffer.getFloat();
		float suspensionVelocityFrontRight = buffer.getFloat();
		float wheelSpeedRearLeft = buffer.getFloat();			//ok
		float wheelSpeedRearRight = buffer.getFloat();			//ok
		float wheelSpeedFrontLeft = buffer.getFloat();			//ok
		float wheelSpeedFrontRight = buffer.getFloat();			//ok
		float throttle = buffer.getFloat();						//ok
		float steer = buffer.getFloat();						//ok
		float brake = buffer.getFloat();						//ok
		float clutch = buffer.getFloat();						//ok
		float gear = buffer.getFloat();							//ok
		float gY = buffer.getFloat();							//ok
		float gX = buffer.getFloat();							//ok
		float currentLap = buffer.getFloat();
		float rpms = buffer.getFloat() * 10;					//ok
		float nf1 = buffer.getFloat();
		float racePosition = buffer.getFloat();					//ok
		float kersRem = buffer.getFloat();
		float kersRecharge = buffer.getFloat();
		float drsStatus = buffer.getFloat();
		float difficulty = buffer.getFloat();
		float assists = buffer.getFloat();
		float fuelRemaining = buffer.getFloat();				//0
		float sessionType = buffer.getFloat();
		float nf2 = buffer.getFloat();
		float sector = buffer.getFloat();						//ok
		float timeSector1 = buffer.getFloat();					//ok
		float timeSector2 = buffer.getFloat();					//ok
		float brakeTempRearLeft = buffer.getFloat();			//ok
		float brakeTempRearRight = buffer.getFloat();			//ok
		float brakeTempFrontLeft = buffer.getFloat();			//ok
		float brakeTempFrontRight = buffer.getFloat();			//ok
		float nf3 = buffer.getFloat();
		float nf4 = buffer.getFloat();
		float nf5 = buffer.getFloat();
		float nf6 = buffer.getFloat();
		float completedLaps = buffer.getFloat();				//ok
		float totalLaps = buffer.getFloat();					//ok
		float trackLength = buffer.getFloat();					//ok
		float previousLapTime = buffer.getFloat();				//ok
		

//		float apitime = buffer.getFloat(0);
//		float currentLapTime = buffer.getFloat(4);
//		float trackDistanceCovered = buffer.getFloat(8);
//		float raceProgress = buffer.getFloat(12);
//
		float[] worldPos = new float[VDashData.VECTOR.VEC_MAX];
		worldPos[VDashData.VECTOR.VEC_X] = xPos;
		worldPos[VDashData.VECTOR.VEC_Y] = yPos;
		worldPos[VDashData.VECTOR.VEC_Z] = zPos;

//		float handbrake = buffer.getFloat(128); 
//		int gear = (int)buffer.getFloat(132);
		
		if(gear == 10)gear = -1;

		float[] latAcc = new float[VDashData.VECTOR.VEC_MAX];
		latAcc[VDashData.VECTOR.VEC_X] = gX;
		latAcc[VDashData.VECTOR.VEC_Y] = gY;
		
		float[] brakeTemps = new float[VDashData.TYRES.TYRE_MAX];
		brakeTemps[VDashData.TYRES.TYRE_FRONT_LEFT] = brakeTempFrontLeft;
		brakeTemps[VDashData.TYRES.TYRE_FRONT_RIGHT] = brakeTempFrontRight;
		brakeTemps[VDashData.TYRES.TYRE_REAR_LEFT] = brakeTempRearLeft;
		brakeTemps[VDashData.TYRES.TYRE_REAR_RIGHT] = brakeTempRearRight;

		if(bestLapTime != -1){
			bestLapTime = Math.min(bestLapTime, previousLapTime);
		}else{
			if(previousLapTime > 0){
				bestLapTime = previousLapTime;
			}
		}
		
		if(rpms > maxRpm){
			maxRpm = rpms;
		}

		if(completedLapsPercent == 0){
			maxRpm = 0;
			bestLapTime = -1;
		}
		
		data.setGameType(type);
		data.setGameState(VDashData.GAME_STATES.GAME_INGAME_PLAYING);
		data.setCurrentLapTime(lapTime);
		data.setLapsInEvent((int) totalLaps);
		data.setMps(speed);
		data.setThrottle(throttle);
		data.setBrake(brake);
		data.setClutch(clutch);
		data.setGear((int) gear);
		data.setLocalAcceleration(latAcc);
		data.setRpm(rpms);
		data.setMaxRpm(maxRpm);
		data.setUnfilteredSteering(steer);
		data.setBrakeTempCelcius(brakeTemps);
		data.setCurrentSector1Time(timeSector1);
		data.setCurrentSector2Time(timeSector2);
		data.setSessionBestLapTime(bestLapTime);
		data.setLastLapTime(previousLapTime);

		Participant p = new Participant();
		p.setCurrentLap((int) currentLap);
		p.setCurrentSector((int) sector);
		p.setWorldPosition(worldPos);
		p.setIsActive(true);
		p.setRacePosition((int) racePosition);
//		p.setCurrentLapDistance(raceProgress - completedLapCount);
//
		Participant[] ps = new Participant[]{p};
		data.setParticipants(ps);
		data.setViewedParticipantIndex(0);
		data.setNumberOfParticipants(1);
	}
}
