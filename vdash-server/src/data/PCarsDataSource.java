package data;
import gui.LogMessager;
import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;

public class PCarsDataSource extends DataSource{

	private static final String fileMappingObjName = "$pcars$";
	private long filePtr;

	//private float xMin = Integer.MAX_VALUE, xMax = Integer.MIN_VALUE, yMin = Integer.MAX_VALUE, yMax = Integer.MIN_VALUE, zMin = Integer.MAX_VALUE, zMax = Integer.MIN_VALUE;

	static {
		System.loadLibrary("data_PCarsDataSource"+System.getProperty("sun.arch.data.model"));
		findFieldIDs();
	}

	public PCarsDataSource(LogMessager m) {
		super(m);
		type = GAME_TYPE.ProjectCARS;
	}

	@Override
	public void open() {
		if(simulate)return;

		filePtr = PCarsDataSource.openFileMapping(fileMappingObjName);
		if(filePtr != 0) {
			int viewPtr = PCarsDataSource.mapViewOfFile(filePtr);
			if(viewPtr != 0) {
				ready = true;

				logMessage("Opened Project Cars memory file successfully", LOG_LVL.L0);
			}else{
				//Could find file, but could not map/read
				ready = false;
			}
		}else{
			ready = false;
			if(!loggedOpenFailure){
				String s1 = "\n-- WARNING --";
				String s2 = "Could not open Project Cars memory file. Is it running? Is shared memory enabled?";
				String s3 = "-------------\n";

				logMessage(s1, LOG_LVL.L0);
				logMessage(s2, LOG_LVL.L0);
				logMessage(s3, LOG_LVL.L0);
				loggedOpenFailure = true;
			}
		}
	}

	/**Close the handle to the MMF*/
	@Override
	public void doClose(){
		PCarsDataSource.closeHandle(filePtr);
	}

	private static native long openFileMapping(String name);
	private static native int mapViewOfFile(long hFileMappingObj);
	private static native boolean closeHandle(long hObject);
	private static native void findFieldIDs();
	private static native VDashData getData();
	/**
	 * Get a new PCars data class.
	 * This instance is created and populated by the JNI code
	 * @throws NullSimulationFileException 
	 */
	@Override
	public VDashData readData() throws NullSimulationFileException{
		if(simulate){
			return super.readData();
		}
		if(ready){
			data =  PCarsDataSource.getData();
			data.setGameType(type);
			
			//Set the version strings based on the version numbers
			data.mVersionNumberString = String.valueOf(data.mVersionNumber).toCharArray();
			data.mBuildVersionNumberString = String.valueOf(data.mBuildVersionNumber).toCharArray();

			//Set lastSectorTime (from AC)
			if(data.getCurrentSector() == 0){
				data.setLastSectorTime(0);
			}else if(data.getCurrentSector() == 1){
				data.setLastSectorTime(data.getCurrentSector1Time());
			}else if(data.getCurrentSector() == 2){
				data.setLastSectorTime(data.getCurrentSector2Time());
			}

			return super.readData();
		}else{
			return super.readData();
		}
	}

	/*
	 * Shutdown hooks
	 */
	public static native void replaceConsoleHandler();
}