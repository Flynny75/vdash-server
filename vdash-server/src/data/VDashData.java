package data;

import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class VDashData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7151196802057232606L;

	public static final byte API_VERSION = 12;
	public static final byte MIN_SERVER_VERSION = 102;

	private static final float PRECISION = 0.00001f;
	private static int STRING_LENGTH_MAX = 64;
	public static int STORED_PARTICIPANTS_MAX = 64;

	private boolean sendParticipantNames = false;
	private char[][] participantNames;
	private int[] raceOrder;

	public static final float MPS_TO_MPH = 2.2369362920544f;
	public static final float KPA_TO_PSI = 0.145037738f;
	public static final float KPA_TO_BAR = 0.01f;

	public static enum GAME_TYPE{
		ProjectCARS((byte)1, "pcars_logo.png"),
		DiRT2((byte)2, "dirt2_logo.png"),
		DiRT3((byte)3, "dirt3_logo.png"),
		AC((byte)4, "ac_logo.png"),
		RACE07((byte)5,"race07_logo.png"),
		R3E((byte)6, "r3e_logo.png"),
		GTR2((byte)7, "gtr2_logo.png"),
		LFS((byte)8, ""),
		GRID2((byte)9, "grid2_logo.png"),
		ETS2((byte)10, "ets2_logo.png");

		private byte id;
		private String imageFilename;
		GAME_TYPE(byte id, String imageFilename){
			this.id = id;
			this.imageFilename = imageFilename;
		}
		public byte getId(){return id;}
		public String getImageFilename(){
			return imageFilename;
		}
	};

	public void setSendParticipantNames(){
		this.sendParticipantNames = true;
	}

	public static class GAME_STATES{
		public static final int GAME_EXITED = 0;
		public static final int GAME_FRONT_END = 1;
		public static final int GAME_INGAME_PLAYING = 2;
		public static final int GAME_INGAME_PAUSED = 3;
		public static final int GAME_RESTARTING = 6;
		public static final int GAME_REPLAY = 7;
		public static final int GAME_END_REPLAY = 8;
	}

	public static class TYRES {
		public static final int TYRE_FRONT_LEFT = 0;
		public static final int TYRE_FRONT_RIGHT = 1;
		public static final int TYRE_REAR_LEFT = 2;
		public static final int TYRE_REAR_RIGHT = 3;
		public static final int TYRE_MAX = 4;
	}

	public static class VECTOR {
		public static final int VEC_X = 0;
		public static final int VEC_Y = 1;
		public static final int VEC_Z = 2;
		public static final int VEC_MAX = 3;
	}

	public static class SESSION_STATES {
		public static final int SESSION_INVALID = 0;
		public static final int SESSION_PRACTICE = 1;
		public static final int SESSION_TEST = 2;
		public static final int SESSION_QUALIFY = 3;
		public static final int SESSION_FORMATION_LAP = 4;
		public static final int SESSION_RACE = 5;
		public static final int SESSION_TIME_ATTACK = 6;
		public static final int SESSION_HOTLAP = 7;
		public static final int SESSION_DRIFT = 8;
		public static final int SESSION_DRAG = 9;
		public static final int SESSION_MAX = 10;
	}

	public static class RACE_STATE{
		public static final int INVALID = 0;
		public static final int NOT_STARTED = 1;
		public static final int RACING = 2;
		public static final int FINISHED = 3;
		public static final int DISQUALIFIED = 4;
		public static final int RETIRED = 5;
		public static final int DNF = 6;
		public static final int MAX = 7;
	}

	public static class FLAGS{
		public static final int NONE = 0;
		public static final int GREEN = 1;
		public static final int BLUE = 2;
		public static final int WHITE = 3;
		public static final int YELLOW = 4;
		public static final int DOUBLE_YELLOW = 5;
		public static final int BLACK = 6;
		public static final int CHEQUERED = 7;
		public static final int MAX = 8;
	}

	public static class FLAG_REASON{
		public static final int NONE = 0;
		public static final int SOLO_CRASH = 1;
		public static final int VEHICLE_CRASH = 2;
		public static final int VEHICLE_OBSTRUCTION = 3;
		public static final int MAX = 4;
	}

	public static class PIT_MODE{
		public static final int NONE = 0;
		public static final int DRIVING_INTO_PITS = 1;
		public static final int IN_PIT = 2;
		public static final int DRIVING_OUT_OF = 3;
		public static final int IN_GARAGE = 4;
		public static final int MAX = 5;
	}

	public static class PIT_SCHEDULE{
		public static final int NONE = 0;
		public static final int STANDARD = 1;
		public static final int DRIVE_THROUGH = 2;
		public static final int STOP_GO = 3;
		public static final int MAX = 4;
	}

	/** Use with mTerrain */
	public static class TERRAIN {
		public static final int TERRAIN_ROAD = 0;
		public static final int TERRAIN_LOW_GRIP_ROAD = 1;
		public static final int TERRAIN_BUMPY_ROAD1 = 2;
		public static final int TERRAIN_BUMPY_ROAD2 = 3;
		public static final int TERRAIN_BUMPY_ROAD3 = 4;
		public static final int TERRAIN_MARBLES = 5;
		public static final int TERRAIN_GRASSY_BERMS = 6;
		public static final int TERRAIN_GRASS = 7;
		public static final int TERRAIN_GRAVEL = 8;
		public static final int TERRAIN_BUMPY_GRAVEL = 9;
		public static final int TERRAIN_RUMBLE_STRIPS = 10;
		public static final int TERRAIN_DRAINS = 11;
		public static final int TERRAIN_TYREWALLS = 12;
		public static final int TERRAIN_CEMENTWALLS = 13;
		public static final int TERRAIN_GUARDRAILS = 14;
		public static final int TERRAIN_SAND = 15;
		public static final int TERRAIN_BUMPY_SAND = 16;
		public static final int TERRAIN_DIRT = 17;
		public static final int TERRAIN_BUMPY_DIRT = 18;
		public static final int TERRAIN_DIRT_ROAD = 19;
		public static final int TERRAIN_BUMPY_DIRT_ROAD = 20;
		public static final int TERRAIN_PAVEMENT = 21;
		public static final int TERRAIN_DIRT_BANK = 22;
		public static final int TERRAIN_WOOD = 23;
		public static final int TERRAIN_DRY_VERGE = 24;
		public static final int TERRAIN_EXIT_RUMBLE_STRIPS = 25;
		public static final int TERRAIN_GRASSCRETE = 26;
		public static final int TERRAIN_LONG_GRASS = 27;
		public static final int TERRAIN_SLOPE_GRASS = 28;
		public static final int TERRAIN_COBBLES = 29;
		public static final int TERRAIN_SAND_ROAD = 30;
		public static final int TERRAIN_BAKED_CLAY = 31;
		public static final int TERRAIN_ASTROTURF = 32;
		public static final int TERRAIN_SNOWHALF = 33;
		public static final int TERRAIN_SNOWFULL = 34;
		// -------------
		public static final int TERRAIN_MAX = 35;
	}

	/** Use with mCrashState */
	public static class CRASH_DAMAGE {
		public static final int CRASH_DAMAGE_NONE = 0;
		public static final int CRASH_DAMAGE_OFFTRACK = 1;
		public static final int CRASH_DAMAGE_LARGE_PROP = 2;
		public static final int CRASH_DAMAGE_SPINNING = 3;
		public static final int CRASH_DAMAGE_ROLLING = 4;
		// -------------
		public static final int CRASH_MAX = 5;
	}

	/** Use with mCurrentSector */
	public static class SECTOR {
		public static final int SECTOR_INVALID = 0;
		public static final int SECTOR_START = 1;
		public static final int SECTOR_SECTOR1 = 2;
		public static final int SECTOR_SECTOR2 = 3;
		public static final int SECTOR_FINISH = 4;
		public static final int SECTOR_STOP = 5;
		// -------------
		public static final int SECTOR_MAX = 6;
	}

	/** Car Flags to be used with mCarFlags */
	public static class CAR_FLAGS {
		public static final int CAR_HEADLIGHT = (1 << 0);
		public static final int CAR_ENGINE_ACTIVE = (1 << 1);
		public static final int CAR_ENGINE_WARNING = (1 << 2);
		public static final int CAR_SPEED_LIMITER = (1 << 3);
		public static final int CAR_ABS = (1 << 4);
		public static final int CAR_HANDBRAKE = (1 << 5);
	}

	/** Use with mTyreFlags */
	public static class TYRE_FLAGS {
		public static final int TYRE_ATTACHED = (1 << 0);
		public static final int TYRE_INFLATED = (1 << 1);
		public static final int TYRE_IS_ON_GROUND = (1 << 2);
	}

	public static class Participant implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = -3700158881727601528L;
		private boolean mIsActive;
		private char[] mName = null;
		private float[] mWorldPosition;
		private float mCurrentLapDistance;
		private int mRacePosition;
		private int mCurrentLap;
		private int mLapsCompleted;
		private int mCurrentSector;
		//TODO AC can provide first and last name

		public boolean isActive(){
			return mIsActive;
		}
		public void setIsActive(boolean isActive){
			this.mIsActive = isActive;
		}

		public char[] getName(){
			return mName;
		}
		public void setName(char[] name){
			this.mName = name;
		}

		public float[] getWorldPosition(){
			return mWorldPosition;
		}
		public void setWorldPosition(float[] pos){
			this.mWorldPosition = pos;
		}

		/**PERCENT*/
		public float getCurrentLapDistance(){
			return mCurrentLapDistance;
		}
		/**PERCENT*/
		public void setCurrentLapDistance(float dist){
			this.mCurrentLapDistance = dist;
		}

		/**THIS IS NOT ZERO-BASED*/
		public int getRacePosition(){
			return mRacePosition;
		}
		public void setRacePosition(int pos){
			this.mRacePosition = pos;
		}

		public int getCurrentLap(){
			return mCurrentLap;
		}
		public void setCurrentLap(int lap){
			this.mCurrentLap = lap;
		}

		public int getLapsCompleted(){
			return mLapsCompleted;
		}
		public void setLapsCompleted(int completed){
			this.mLapsCompleted = completed;
		}

		public int getCurrentSector(){
			return mCurrentSector;
		}
		public void setCurrentSector(int s){
			this.mCurrentSector = s;
		}
		@Override
		public String toString() {
			return "Participant [mIsActive=" + mIsActive + ", mName=" + new String(mName)
					+ ", mWorldPosition=" + Arrays.toString(mWorldPosition)
					+ ", mCurrentLapDistance=" + mCurrentLapDistance
					+ ", mRacePosition=" + mRacePosition + ", mCurrentLap="
					+ mCurrentLap + ", mLapsCompleted=" + mLapsCompleted
					+ ", mCurrentSector=" + mCurrentSector + "]";
		}


	}

	private GAME_TYPE gameType;

	public int mVersionNumber; 							// [ RANGE = 0->... ]
	public int mBuildVersionNumber;
	
	//AC supplies versions as Strings
	public char[] mVersionNumberString;					// STRING
	public char[] mBuildVersionNumberString;			// STRING

	private int mGameState; 							// [ enum (Type#1) Game state ]
	private int mSessionState; 							// [ enum (Type#2) Session state ]
	private int mRaceState;								// [ enum (Type#3) Race state ]

	private int mViewedParticipantIndex;
	private int mNumParticipants;

	private int mHighestFlagColour;         			// [ enum (Type#5) Flag Colour ]
	private int mHighestFlagReason;         			// [ enum (Type#6) Flag Reason ]

	private int mPitMode;                           	// [ enum (Type#7) Pit Mode ]
	private int mPitSchedule;                       	// [ enum (Type#8) Pit Stop Schedule ]

	private float mUnfilteredThrottle; 					// [ RANGE = 0.0f->1.0f ]
	private float mUnfilteredBrake; 					// [ RANGE = 0.0f->1.0f ]
	private float mUnfilteredSteering; 					// [ RANGE = -1.0f->1.0f ]
	private float mUnfilteredClutch; 					// [ RANGE = 0.0f->1.0f ]

	private char[] mCarName; 							// [ string ]
	private char[] mCarClassName; 						// [ string ]
	private char[] mTrackLocation; 						// [ string ]
	private char[] mTrackVariation;
	private float mTrackLength;

	private boolean mLapInvalidated;					// [ UNITS = boolean ]   [ RANGE = false->true ]   [ UNSET = false ]
	private float mBestLapTime;                         // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mLastLapTime;                         // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = 0.0f ]
	private float mCurrentTime;                         // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = 0.0f ]
	private float mSplitTimeAhead;                      // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mSplitTimeBehind;                     // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mSplitTime;                           // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = 0.0f ]
	private float mEventTimeRemaining;                  // [ UNITS = milli-seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mPersonalFastestLapTime;              // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mWorldFastestLapTime;                 // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mCurrentSector1Time;                  // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mCurrentSector2Time;                  // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mCurrentSector3Time;                  // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mFastestSector1Time;                  // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mFastestSector2Time;                  // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mFastestSector3Time;                  // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mPersonalFastestSector1Time;          // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mPersonalFastestSector2Time;          // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mPersonalFastestSector3Time;          // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mWorldFastestSector1Time;             // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mWorldFastestSector2Time;             // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private float mWorldFastestSector3Time;             // [ UNITS = seconds ]   [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	//TODO remove
	private float mLastSectorTime;						// From AC. 

	private int mLapsInEvent; 							// [ RANGE = 0->... ] [ UNSET = 0 ]

	private int mCarFlags; 								// [ enum (Type#6) Car Flags ]
	private float mOilTempCelsius; 						// [ UNITS = Celsius ] [ UNSET = 0.0f ]
	private float mOilPressureKPa; 						// [ UNITS = Kilopascal ] [ RANGE = 0.0f->... ] [ UNSET = 0.0f ]
	private float mWaterTempCelsius; 					// [ UNITS = Celsius ] [ UNSET = 0.0f ]
	private float mWaterPressureKPa; 					// [ UNITS = Kilopascal ] [ RANGE = 0.0f->... ] [ UNSET = 0.0f ]
	private float mFuelPressureKPa; 					// [ UNITS = Kilopascal ] [ RANGE =0.0f->... ] [ UNSET = 0.0f ]
	private float mFuelLevel; 							// [ RANGE = 0.0f->1.0f ]
	private float mFuelCapacity; 						// [ UNITS = Liters ] [ RANGE = 0.0f->1.0f ] [ UNSET = 0.0f ]
	private float mSpeed; 								// [ UNITS = Metres per-second ] [ RANGE = 0.0f->... ]
	private float mRPM; 								// [ UNITS = Revolutions per minute ] [ RANGE = 0.0f->... ] [ UNSET = 0.0f ]
	private float mMaxRPM; 								// [ UNITS = Revolutions per minute ] [ RANGE = 0.0f->... ] [ UNSET = 0.0f ]
	private float mBrake; 								// [ RANGE = 0.0f->1.0f ]
	private float mThrottle; 							// [ RANGE = 0.0f->1.0f ]
	private float mClutch; 								// [ RANGE = 0.0f->1.0f ]
	private float mSteering; 							// [ RANGE = -1.0f->1.0f ]
	private int mGear; 									// [ RANGE = -1 (Reverse) 0 (Neutral) 1 (Gear 1) 2 (Gear 2) etc... ] [ UNSET = 0 (Neutral) ]
	private int mNumGears;                              // [ RANGE = 0->... ]   [ UNSET = -1 ]
	private float mOdometerKM;                          // [ RANGE = 0.0f->... ]   [ UNSET = -1.0f ]
	private int mLastOpponentCollisionIndex;            // [ RANGE = 0->STORED_PARTICIPANTS_MAX ]   [ UNSET = -1 ]
	private float mLastOpponentCollisionMagnitude;      // [ RANGE = 0.0f->... ]
	private boolean mBoostActive;                       // [ UNITS = boolean ]   [ RANGE = false->true ]   [ UNSET = false ]
	private float mBoostAmount;                         // [ RANGE = 0.0f->100.0f ]
	private boolean isHeadlightLit;						// BOOLEAN
	private boolean isEngineActive;						// BOOLEAN
	private boolean isEngineWarning;					// BOOLEAN
	private boolean isSpeedLimited;						// BOOLEAN
	private boolean isAbsEnabled;						// BOOLEAN
	private boolean isAbsActive;                		// [ UNITS = boolean ]   [ RANGE = false->true ]   [ UNSET = false ]
	private boolean isTcEnabled;						// BOOLEAN
	private boolean isTcActive;							// BOOLEAN
	private boolean isHandbrakeActive;					// BOOLEAN
	private float mMaxTorque;							// FROM AC
	private float mMaxPower;							// FROM AC

	// Motion & Device Related
	private float[] mOrientation; 						// [ UNITS = Euler Angles ]
	private float[] mLocalVelocity; 					// [ UNITS = Metres per-second ]
	private float[] mWorldVelocity; 					// [ UNITS = Metres per-second ]
	private float[] mAngularVelocity; 					// [ UNITS = Radians per-second ]
	private float[] mLocalAcceleration; 				// [ UNITS = Metres per-second ]
	private float[] mWorldAcceleration; 				// [ UNITS = Metres per-second ]
	private float[] mExtentsCentre; 					// [ UNITS = Local Space X Y Z ]
	private float[] mGs;

	private int[] mTyreFlags; 							// [ enum (Type#7) Tyre Flags ]
	//TODO
	private boolean[] mTyreAttached;					// BOOLEAN From flags
	private boolean[] mTyreInflated;					// BOOLEAN From flags
	private boolean[] mTyreOnGround;					// BOOLEAN From flags
	private int[] mTerrain; 							// [ enum (Type#3) Terrain Materials ]
	private float[] mTyreY; 							// [ UNITS = Local Space Y ]
	private float[] mTyreRPS; 							// [ UNITS = Revolutions per second ]
	private float[] mTyreSlipSpeed; 					// [ UNITS = Metres per-second ]
	private float[] mTyreTemp; 							// [ UNITS = Celsius ] [ UNSET = 0.0f ]
	private float[] mTyreGrip; 							// [ RANGE = 0.0f->1.0f ]
	private float[] mTyreHeightAboveGround; 			// [ UNITS = Local Space Y ]
	private float[] mTyreLateralStiffness; 				// [ UNITS = Lateral stiffness coefficient used in tyre deformation ]
	public float[] mTyreCondition; 						// [ RANGE = 100f->0.0f ]
	public float[] mBrakeCondition;						// [ RANGE = 100f->0.0f ]
	private float[] mBrakeTempCelcius; 					// [ UNITS = C ]
	public float[] mSuspensionCondition;				// [ RANGE = 100f->0.0f ]
	public float[] mTyrePressures;						// KPa
	private float[] mTyreTreadTemp;						// KELVIN
	private float[] mTyreLayerTemp;						// KELVIN
	private float[] mTyreCarcassTemp;					// KELVIN
	private float[] mTyreRimTemp;						// KELVIN
	private float[] mTyreInternalAirTemp;				// KELVIN
	public float[] mTyreInnerTemps;						// KELVIN
	public float[] mTyreMidTemps;						// KELVIN
	public float[] mTyreOuterTemps;						// KELVIN

	// Car Damage
	private int mCrashState;                        	// [ enum (Type#12) Crash Damage State ]
	public float mAeroCondition;						// [ RANGE = 100f->0.0f ]
	public float mEngineCondition;                      // [ RANGE = 100f->0.0f ]
	public float mTransmissionCondition;				// [ RANGE = 100f->0.0f ]

	// Weather
	private float mAmbientTemperature;                  // [ UNITS = Celsius ]   [ UNSET = 25.0f ]
	private float mTrackTemperature;                    // [ UNITS = Celsius ]   [ UNSET = 30.0f ]
	private float mRainDensity;                         // [ UNITS = How much rain will fall ]   [ RANGE = 0.0f->1.0f ]
	private float mWindSpeed;                           // [ RANGE = 0.0f->100.0f ]   [ UNSET = 2.0f ]
	private float mWindDirectionX;                      // [ UNITS = Normalised Vector X ]
	private float mWindDirectionY;                      // [ UNITS = Normalised Vector Y ]
	private float mCloudBrightness;                     // [ RANGE = 0.0f->... ]

	private Participant[] mParticipants;
	
	//ETS2
	private float navigationDistance;					// KM
	public char[] jobSourceCity;						// STRING
	public char[] jobDestinationCity;					// STRING

	private static VDashData data;

	public VDashData() {
		init();
	}

	public GAME_TYPE getGameType(){
		return this.gameType;
	}
	public void setGameType(GAME_TYPE type){
		this.gameType = type;
	}

	public int getVersionNumber() {
		return mVersionNumber;
	}

	public int getGameState() {
		return mGameState;
	}
	public void setGameState(int state){
		this.mGameState = state;
	}
	public int getRaceState(){
		return mRaceState;
	}
	public int getSessionState() {
		return mSessionState;
	}

	public float getUnfilteredThrottle() {
		return mUnfilteredThrottle;
	}
	public float getUnfilteredBrake() {
		return mUnfilteredBrake;
	}
	public void setUnfilteredSteering(float f){
		this.mUnfilteredSteering = f;
	}
	public float getUnfilteredSteering() {
		return mUnfilteredSteering;
	}
	public float getUnfilteredClutch() {
		return mUnfilteredClutch;
	}

	public char[] getPlayerName(int index) {
		try{
			char[] retVal = participantNames[index];
			if(retVal == null){
				retVal = new char[0];
			}
			return retVal;
		}catch(Exception e){
			return new char[0];
		}
	}

	/**
	 * Provide the viewing index to 
	 * @param index
	 * @return
	 */
	public char[]getPlayerNameInRaceOrder(int index){
		try{
			return participantNames[raceOrder[index]];
		}catch(Exception e){
			return new char[0];
		}
	}

	public void setCarName(char[] carName){
		this.mCarName = carName;
	}
	public char[] getCarName() {
		return mCarName;
	}

	public char[] getCarClassName() {
		return mCarClassName;
	}

	public void setTrackLocation(char[] trackLocation){
		this.mTrackLocation = trackLocation;
	}
	public char[] getTrackLocation() {
		return mTrackLocation;
	}

	public void setTrackVariation(char[] var){
		this.mTrackVariation = var;
	}
	public char[] getTrackVariation(){
		return mTrackVariation;
	}

	public float getTrackLength(){
		return mTrackLength;
	}	

	public boolean isCurrentLapInvalid(){
		return mLapInvalidated;
	}
	public void setSessionBestLapTime(float time){
		this.mBestLapTime = time;
	}
	public float getSessionBestLap(){
		return mBestLapTime;
	}
	public float getPersonalBestLapTime() {
		return mPersonalFastestLapTime;
	}
	public float getWorldBestLapTime(){
		return mWorldFastestLapTime;
	}
	public void setLastLapTime(float time){
		this.mLastLapTime = time;
	}
	public float getLastLapTime() {
		return mLastLapTime;
	}
	public float getCurrentLapTime() {
		return mCurrentTime;
	}
	public void setCurrentLapTime(float currentTime){
		this.mCurrentTime = currentTime;
	}

	public float getCurrentSector1Time() {
		return mCurrentSector1Time;
	}
	public void setCurrentSector1Time(float f){
		this.mCurrentSector1Time = f;
	}
	public float getSessionBestSector1Time(){
		return mFastestSector1Time;
	}
	public float getPersonalBestSector1Time() {
		return mPersonalFastestSector1Time;
	}
	public float getWorldBestSector1Time(){
		return mWorldFastestSector1Time;
	}

	public float getCurrentSector2Time() {
		return mCurrentSector2Time;
	}
	public void setCurrentSector2Time(float f){
		this.mCurrentSector2Time = f;
	}
	public float getSessionBestSector2Time(){
		return mFastestSector2Time;
	}
	public float getPersonalBestSector2Time() {
		return mPersonalFastestSector2Time;
	}
	public float getWorldBestSector2Time(){
		return mWorldFastestSector2Time;
	}

	public float getCurrentSector3Time(){
		return mCurrentSector3Time;
	}
	public float getSessionBestSector3Time(){
		return mFastestSector3Time;
	}
	public float getPersonalBestSector3Time(){
		return mPersonalFastestSector2Time;
	}
	public float getWorldBestSector3Time(){
		return mWorldFastestSector3Time;
	}

	public float getLastSectorTime(){
		return mLastSectorTime;
	}
	public void setLastSectorTime(float time){
		this.mLastSectorTime = time;
	}

	public float getmSplitTime() {
		return mSplitTime;
	}
	public float getmEventTimeRemaining() {
		return mEventTimeRemaining;
	}
	public float getSplitAheadTime(){
		return mSplitTimeAhead;
	}
	public float getSplitBehindTime(){
		return mSplitTimeBehind;
	}

	public int getLapsInEvent() {
		return mLapsInEvent;
	}
	public void setLapsInEvent(int laps){
		this.mLapsInEvent = laps;
	}

	public int getViewedParticipantIndex(){
		return mViewedParticipantIndex;
	}
	public void setViewedParticipantIndex(int index){
		this.mViewedParticipantIndex = index;
	}

	public int getCarFlags() {
		return mCarFlags;
	}

	public float getOilTempCelsius() {
		return mOilTempCelsius;
	}
	public void setOilTempCelsius(float temp){
		this.mOilTempCelsius = temp;
	}
	public float getOilTempFahrenheit(){
		return convertCtoF(getOilTempCelsius());
	}

	public float getOilPressureKPa() {
		return mOilPressureKPa;
	}
	public float getOilPressurePSI(){
		return convertKpaToPsi(getOilPressureKPa());
	}
	public float getOilPressureBAR(){
		return convertKpaToBar(getOilPressureKPa());
	}

	public float getWaterTempCelsius() {
		return mWaterTempCelsius;
	}
	public void setWaterTempCelsius(float temp){
		this.mWaterTempCelsius = temp;
	}
	public float getWaterTempFahrenheit(){
		return convertCtoF(getWaterTempCelsius());
	}

	public float getWaterPressureKPa() {
		return mWaterPressureKPa;
	}
	public float getWaterPressureBAR(){
		return convertKpaToBar(getWaterPressureKPa());
	}
	public float getWaterPressurePSI(){
		return convertKpaToPsi(getWaterPressureKPa());
	}

	public float getFuelPressureKPa() {
		return mFuelPressureKPa;
	}
	public void setFuelPressure(float pressure){
		this.mFuelPressureKPa = pressure;
	}
	public float getFuelPressureBAR(){
		return convertKpaToBar(getFuelPressureKPa());
	}
	public float getFuelPressurePSI(){
		return convertKpaToPsi(getFuelPressureKPa());
	}

	public float getFuelLevel() {
		return mFuelLevel;
	}
	public void setFuelLevel(float percentage){
		this.mFuelLevel = percentage;
	}
	public float getFuelCapacity() {
		return mFuelCapacity;
	}
	/**LITRES*/
	public void setFuelCapacity(float totalFuel){
		this.mFuelCapacity = totalFuel;
	}

	public float getRpm() {
		return mRPM;
	}
	public void setRpm(float rpm){
		this.mRPM = rpm;
	}

	public float getMaxRPM() {
		return mMaxRPM;
	}
	public void setMaxRpm(float maxRpm){
		this.mMaxRPM = maxRpm;
	}

	public float getBrake() {
		return mBrake;
	}
	public void setBrake(float brake){
		this.mBrake = brake;
	}

	public float getThrottle() {
		return mThrottle;
	}
	public void setThrottle(float throttle){
		this.mThrottle = throttle;
	}

	public float getClutch() {
		return mClutch;
	}
	public void setClutch(float clutch){
		this.mClutch = clutch;
	}

	public float getSteering() {
		return mSteering;
	}

	public int getGear() {
		return mGear;
	}
	public void setGear(int g){
		this.mGear = g;
	}
	public int getMaxGear(){
		return mNumGears;
	}

	public float getMaxTorque(){
		return mMaxTorque;
	}
	public float getMaxPower(){
		return mMaxPower;
	}

	public void setMps(float speed){
		this.mSpeed = speed;
	}

	public float getMps() {
		return mSpeed;
	}

	public float getMph(){
		return getMps() * 2.2369362920544f; 
	}

	public float getKph() {
		return getMps() * 3.6f;
	}

	public float getOdometerKM(){
		return mOdometerKM;
	}
	public float getOdometerMiles(){
		return convertKphToMph(getOdometerKM());
	}
	public float getOdometerMeters(){
		return getOdometerKM() * 1000f;
	}

	public boolean isHeadlightLit() {
		return isHeadlightLit;
	}
	public boolean isEngineActive() {
		return isEngineActive;
	}
	public boolean isEngineWarning() {
		return isEngineWarning;
	}
	public boolean isSpeedLimited() {
		return isSpeedLimited;
	}
	public void setSpeedLimited(boolean limited){
		isSpeedLimited = limited;
	}

	public boolean isAbsEnabled() {
		return isAbsEnabled;
	}
	public void setAbsEnabled(boolean enabled){
		isAbsEnabled = enabled;
	}
	public boolean isAbsActive(){
		return isAbsActive;
	}
	public void setAbsActive(boolean active){
		isAbsActive = active;
	}

	public boolean isTcEnabled(){
		return isTcEnabled;
	}
	public void setTcEnabled(boolean enabled){
		isTcEnabled = enabled;
	}
	public boolean isTcActive(){
		return isTcActive;
	}
	public void setTcActive(boolean active){
		isTcActive = active;
	}

	public boolean isHandbrakeActive() {
		return isHandbrakeActive;
	}
	public boolean isBoostActive(){
		return mBoostActive;
	}
	public float getBoostAmount(){
		return mBoostAmount;
	}

	public int getLastOpponentCollisionIndex(){
		return mLastOpponentCollisionIndex;
	}
	public float getLastOpponentCollisionMagnitude(){
		return mLastOpponentCollisionMagnitude;
	}

	public float[] getOrientation() {
		return mOrientation;
	}

	public float[] getLocalVelocity() {
		return mLocalVelocity;
	}

	public float[] getWorldVelocity() {
		return mWorldVelocity;
	}

	public float[] getAngularVelocity() {
		return mAngularVelocity;
	}

	public float[] getLocalAcceleration() {
		return mLocalAcceleration;
	}
	public void setLocalAcceleration(float[] acc){
		this.mLocalAcceleration = acc;
	}

	public float[] getWorldAcceleration() {
		return mWorldAcceleration;
	}

	public float[] getExtentsCentre() {
		return mExtentsCentre;
	}

	public float[] getGForces() {
		return mGs;
	}

	public int[] getTyreFlags() {
		return this.mTyreFlags;
	}

	public int[] getTerrain() {
		return this.mTerrain;
	}

	public float[] getTyreY() {
		return this.mTyreY;
	}

	public float[] getTyreRPS() {
		return this.mTyreRPS;
	}

	public float[] getTyreSlipSpeed() {
		return this.mTyreSlipSpeed;
	}

	public float[] getTyreTemp() {
		return this.mTyreTemp;
	}

	public float[] getTyreGrip() {
		return this.mTyreGrip;
	}

	public float[] getTyreHeightAboveGround() {
		return this.mTyreHeightAboveGround;
	}

	public float[] getTyreLateralStiffness() {
		return this.mTyreLateralStiffness;
	}

	public float[] getTyreWear() {
		return this.mTyreCondition;
	}

	public float[] getBrakeDamage() {
		return this.mBrakeCondition;
	}

	public void setBrakeTempCelcius(float[] temps){
		this.mBrakeTempCelcius = temps;
	}
	public float[] getBrakeTempCelcius() {
		return this.mBrakeTempCelcius;
	}

	/**KELVIN*/
	public float[] getTyreTreadTemp(){
		return this.mTyreTreadTemp;
	}

	/**KELVIN*/
	public float[] getTyreLayerTemp(){
		return this.mTyreLayerTemp;
	}

	/**KELVIN*/
	public float[] getTyreCarcassTemp(){
		return this.mTyreCarcassTemp;
	}

	/**KELVIN*/
	public float[] getTyreRimTemp(){
		return this.mTyreRimTemp;
	}

	/**KELVIN*/
	public float[] getTyreInternalAirTemp(){
		return this.mTyreInternalAirTemp;
	}

	public int getHighestFlagColour(){
		return mHighestFlagColour;
	}
	public int getHighestFlagReason(){
		return mHighestFlagReason;
	}

	public int getPitMode(){
		return mPitMode;
	}
	public int getPitSchedule(){
		return mPitSchedule;
	}

	public float getAmbientTempCelsius(){
		return mAmbientTemperature;
	}
	public float getAmbientTempFahrenheit(){
		return convertCtoF(getAmbientTempCelsius());
	}

	public float getTrackTempCelsius(){
		return mTrackTemperature;
	}
	public float getTrackTempFahrenheit(){
		return convertCtoF(getTrackTempCelsius());
	}

	public float getRainDensity(){
		return mRainDensity;
	}
	public float getWindSpeed(){
		return mWindSpeed;
	}
	public float getWindDirectionX(){
		return mWindDirectionX;
	}
	public float getWindDirectionY(){
		return mWindDirectionY;
	}
	public float getCloudBrightness(){
		return mCloudBrightness;
	}

	public int getCrashState(){
		return mCrashState;
	}

	/**Get percent condition of aerodynamics*/
	public float getAeroCondition(){
		return (1 - mAeroCondition) * 100;
	}

	public Participant[] getParticipants(){
		return this.mParticipants;
	}
	public void setParticipants(Participant[] p){
		this.mParticipants = p;
	}

	public void writeDataToStream(ByteBuffer buffer) throws IOException {
		sanatize();
		buffer.put(API_VERSION);

		//APP STILL EXPECTS MONITOR STATES, EVEN THOUGH THEYRE NOT USED
		boolean[] statesToMonitor = new boolean[Constants.MonitorStates.TOTAL_STATES];
		Arrays.fill(statesToMonitor, true);
		for(boolean b:statesToMonitor){
			buffer.put(b ? (byte)1 : (byte)0);
		}
		
		buffer.put(this.gameType.getId());

		writeGameStateToStream(buffer);

		writeInputToStream(buffer);

		writeNamesToStream(buffer);

		writeTimesToStream(buffer);

		writeRaceInfoToStream(buffer);

		writeCarInfoToStream(buffer);

		writeMotionDeviceInfoToStream(buffer);

		writeTyreWheelInfoToStream(buffer);

		writeWeatherInfoToStream(buffer);

		writeParticipantInfoToStream(buffer);
		
		writeETS2ToStream(buffer);
	}

	///////////////////////////////////////////////////
	// STATE
	//////////////////////////////////////////////////
	private void writeGameStateToStream(ByteBuffer dos) throws IOException {
		dos.put((byte) mVersionNumber);
		dos.put((byte) mGameState);
		dos.put((byte) mSessionState);
		dos.put((byte) mRaceState);
		dos.put((byte) mViewedParticipantIndex);
		dos.putInt(mBuildVersionNumber);
		
		if(mBuildVersionNumberString != null && mBuildVersionNumberString.length > 0){
			dos.putInt(mBuildVersionNumberString.length);
			for(char c : mBuildVersionNumberString){
				dos.putChar(c); 
			}
		}else{
			dos.putInt(-1);
		}
		
		if(mVersionNumberString != null && mVersionNumberString.length > 0){
			dos.putInt(mVersionNumberString.length);
			for(char c : mVersionNumberString){
				dos.putChar(c);
			}
		}else{
			dos.putInt(-1);
		}
	}

	// ////////////////////////////////////////////
	// INPUT
	// ////////////////////////////////////////////
	private void writeInputToStream(ByteBuffer dos) throws IOException {
		dos.putInt((int) (mUnfilteredThrottle / PRECISION));
		dos.putInt((int) (mUnfilteredBrake / PRECISION));
		dos.putInt((int) (mUnfilteredSteering / PRECISION));
		dos.putInt((int) (mUnfilteredClutch / PRECISION));
	}

	// //////////////////////////////////////////
	// NAMES
	// /////////////////////////////////////////
	private void writeNamesToStream(ByteBuffer dos) throws IOException {

		if (mCarName != null) {
			dos.putInt(mCarName.length);
			for(char c:mCarName){
				dos.putChar(c);
			}
		} else {
			dos.putInt(0);
		}

		if (mCarClassName != null) {
			dos.putInt(mCarClassName.length);
			for(char c:mCarClassName){
				dos.putChar(c);
			}
		} else {
			dos.putInt(0);
		}

		if (mTrackLocation != null) {
			dos.putInt(mTrackLocation.length);
			for(char c : mTrackLocation){
				dos.putChar(c);
			}
		} else {
			dos.putInt(0);
		}

		if (mTrackVariation != null) {
			dos.putInt(mTrackVariation.length);
			for(char c : mTrackVariation){
				dos.putChar(c);
			}
		} else {
			dos.putInt(0);
		}
	}

	// ///////////////////////////////////////
	// TIMES
	// ///////////////////////////////////////
	private void writeTimesToStream(ByteBuffer dos) throws IOException {
		if(mLapInvalidated){
			dos.put((byte) 1);
		}else{
			dos.put((byte) 0);
		}
		dos.putFloat(mBestLapTime);
		dos.putFloat(mLastLapTime);
		dos.putFloat(mCurrentTime);
		dos.putFloat(mSplitTimeAhead);
		dos.putFloat(mSplitTimeBehind);
		dos.putFloat(mSplitTime);
		dos.putFloat(mEventTimeRemaining);
		dos.putFloat(mPersonalFastestLapTime);
		dos.putFloat(mWorldFastestLapTime);
		dos.putFloat(mCurrentSector1Time);
		dos.putFloat(mCurrentSector2Time);
		dos.putFloat(mCurrentSector3Time);
		dos.putFloat(mFastestSector1Time);
		dos.putFloat(mFastestSector2Time);
		dos.putFloat(mFastestSector3Time);
		dos.putFloat(mPersonalFastestSector1Time);
		dos.putFloat(mPersonalFastestSector2Time);
		dos.putFloat(mPersonalFastestSector3Time);
		dos.putFloat(mWorldFastestSector1Time);
		dos.putFloat(mWorldFastestSector2Time);
		dos.putFloat(mWorldFastestSector3Time);
	}

	// ///////////////////////////////////////////////////
	// RACE INFO
	// ////////////////////////////////////////////////////
	private void writeRaceInfoToStream(ByteBuffer dos) throws IOException {
		dos.putInt(mLapsInEvent);
		dos.putInt(mNumParticipants);
	}

	// ///////////////////////////////////////////////////////
	// CAR INFO
	// //////////////////////////////////////////////////////
	private void writeCarInfoToStream(ByteBuffer buffer) throws IOException {
		buffer.putInt((int) (mOilTempCelsius / PRECISION));
		buffer.putInt((int) (mOilPressureKPa / PRECISION));
		buffer.putInt((int) (mWaterTempCelsius / PRECISION));
		buffer.putInt((int) (mWaterPressureKPa / PRECISION));
		buffer.putInt((int) (mFuelLevel / PRECISION));
		buffer.putInt((int) (mFuelPressureKPa / PRECISION));
		buffer.putInt((int) (mFuelCapacity / PRECISION));
		buffer.putInt((int) (mSpeed / PRECISION));
		buffer.putInt((int) (mRPM / PRECISION));
		buffer.putInt((int) (mMaxRPM / PRECISION));
		buffer.putInt((int) (mBrake / PRECISION));
		buffer.putInt((int) (mThrottle / PRECISION));
		buffer.putInt((int) (mClutch / PRECISION));
		buffer.putInt(mGear);
		buffer.putInt((int) (mSpeed / PRECISION));
		buffer.putInt((int) (mSteering / PRECISION));
		buffer.putInt((int) (mMaxTorque / PRECISION));
		buffer.putInt((int) (mMaxPower / PRECISION));
		buffer.putInt((int) (mOdometerKM / PRECISION));
		buffer.put(((mCarFlags & CAR_FLAGS.CAR_HEADLIGHT) != 0 || isHeadlightLit) ? (byte) 1 : (byte)0);
		buffer.put(((mCarFlags & CAR_FLAGS.CAR_ENGINE_ACTIVE) != 0 || isEngineActive) ? (byte) 1 : (byte) 0);
		buffer.put(((mCarFlags & CAR_FLAGS.CAR_ENGINE_WARNING) != 0 || isEngineWarning) ? (byte) 1 : (byte) 0);
		buffer.put(((mCarFlags & CAR_FLAGS.CAR_SPEED_LIMITER) != 0 || isSpeedLimited) ? (byte) 1 : (byte) 0);
		buffer.put(((mCarFlags & CAR_FLAGS.CAR_ABS) != 0 || isAbsEnabled) ? (byte) 1 : (byte) 0);
		buffer.put(((mCarFlags & CAR_FLAGS.CAR_HANDBRAKE) != 0 || isHandbrakeActive) ? (byte) 1 : (byte) 0);
		buffer.put(isAbsActive ? (byte)1 : (byte)0);
		buffer.put(isTcEnabled ? (byte)1 : (byte)0);
		buffer.put(isTcActive ? (byte)1 : (byte)0);
		buffer.putInt((int) (mAeroCondition / PRECISION));
		buffer.putInt((int) (mEngineCondition / PRECISION));
		buffer.putInt((int) (mTransmissionCondition / PRECISION));
	}

	// ////////////////////////////////////////////////////
	// MOTION
	// //////////////////////////////////////////////////////
	private void writeMotionDeviceInfoToStream(ByteBuffer buffer) throws IOException {
		for (float f : mOrientation) {
			buffer.putInt((int) (f / PRECISION));
		}
		for (float f : mLocalVelocity) {
			buffer.putInt((int) (f / PRECISION));
		}
		for (float f : mWorldVelocity) {
			buffer.putInt((int) (f / PRECISION));
		}
		for (float f : mAngularVelocity) {
			buffer.putInt((int) (f / PRECISION));
		}
		for (float f : mLocalAcceleration) {
			buffer.putInt((int) (f / PRECISION));
		}
		for (float f : mWorldAcceleration) {
			buffer.putInt((int) (f / PRECISION));
		}
		for (float f : mExtentsCentre) {
			buffer.putInt((int) (f / PRECISION));
		}
		mGs = new float[VECTOR.VEC_MAX];
		for(int i = 0; i < VECTOR.VEC_MAX; i++){
			mGs[i] = (float) (mLocalAcceleration[i] / (9.80665));
		}
		for(float f:mGs){
			buffer.putInt((int)(f / PRECISION));
		}
	}

	// //////////////////////////////////////////////////////
	// TYES AND WHEELS
	// /////////////////////////////////////////////////////
	private void writeTyreWheelInfoToStream(ByteBuffer dos) throws IOException {
		for (int i : this.mTyreFlags) {
			dos.putInt(i);
		}
		for (int i : this.mTerrain) {
			dos.putInt(i);
		}
		for (float f : this.mTyreY) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreRPS) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreSlipSpeed) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreTemp) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyrePressures){
			dos.putInt((int)(f / PRECISION));
		}



		for (float f : this.mTyreTreadTemp) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreLayerTemp) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreCarcassTemp) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreRimTemp) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreInternalAirTemp) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreInnerTemps){
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreMidTemps){
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreOuterTemps){
			dos.putInt((int) (f / PRECISION));
		}




		for (float f : this.mTyreGrip) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreHeightAboveGround) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreLateralStiffness) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mTyreCondition) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mBrakeCondition) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mSuspensionCondition) {
			dos.putInt((int) (f / PRECISION));
		}
		for (float f : this.mBrakeTempCelcius) {
			dos.putInt((int) (f / PRECISION));
		}
	}

	///////////////////////////////////////////////////////
	// WEATHER
	///////////////////////////////////////////////////////
	private void writeWeatherInfoToStream(ByteBuffer dos) throws IOException{
		dos.putInt((int) (mAmbientTemperature / PRECISION));
		dos.putInt((int) (mTrackTemperature / PRECISION));
		dos.putInt((int) (mRainDensity / PRECISION));
		dos.putInt((int) (mWindSpeed / PRECISION));
		dos.putInt((int) (mWindDirectionX / PRECISION));
		dos.putInt((int) (mWindDirectionY / PRECISION));
		dos.putInt((int) (mCloudBrightness / PRECISION));
	}

	//  ///////////////////////////////////////////////////
	// PARTICIPATS
	// ////////////////////////////////////////////////////
	private void writeParticipantInfoToStream(ByteBuffer bb) throws IOException{
		if(mParticipants != null){
			bb.put((byte)mParticipants.length);
			sendParticipantNames = true;
			bb.put(sendParticipantNames ? (byte)1 : (byte)0);

			for(Participant p:mParticipants){
				bb.put(p.isActive() ? (byte)1 : (byte)0);

				if(sendParticipantNames){
					if(p.getName() != null){
						bb.putInt(p.getName().length);
						for(char c : p.getName()){
							bb.putChar(c);
						}	
					}else{
						bb.putInt(-1);
					}
				}

				if(p.getWorldPosition() != null){
					bb.put((byte)1);
					for(int i = 0; i < VECTOR.VEC_MAX; i++){
						float f = p.getWorldPosition()[i];
						bb.putInt((int) (f / PRECISION));
					}
				}else{
					bb.put((byte)0);
				}

				int dist = (int) (p.getCurrentLapDistance() / PRECISION);
				bb.putInt(dist);
				bb.put((byte)p.getRacePosition());
				bb.put((byte)p.getLapsCompleted());
				bb.put((byte)p.getCurrentLap());
				bb.put((byte)p.getCurrentSector());
			}
		}else{
			bb.put((byte)-1);
		}
	}
	
	private void writeETS2ToStream(ByteBuffer buffer){
		buffer.putInt((int) (navigationDistance / PRECISION));
		
		if(jobSourceCity != null && jobSourceCity.length > 0){
			buffer.putInt(jobSourceCity.length);
			for(char c : jobSourceCity){
				buffer.putChar(c);
			}
		}else{
			buffer.putInt(-1);
		}
		
		if(jobDestinationCity != null && jobDestinationCity.length > 0){
			buffer.putInt(jobDestinationCity.length);
			for(char c: jobDestinationCity){
				buffer.putChar(c);
			}
		}else{
			buffer.putInt(-1);
		}
	}


	@Override
	public String toString() {
		return "VDashData [sendParticipantNames="
				+ sendParticipantNames + ",\nparticipantNames="
				+ Arrays.toString(participantNames) + ",\nraceOrder="
				+ Arrays.toString(raceOrder) + ",\ngameType=" + gameType
				+ ",\nmVersionNumber=" + mVersionNumber
				+ ",\nmBuildVersionNumber=" + mBuildVersionNumber
				+ ",\nmGameState=" + mGameState + ",\nmSessionState="
				+ mSessionState + ",\nmRaceState=" + mRaceState
				+ ",\nmViewedParticipantIndex=" + mViewedParticipantIndex
				+ ",\nmNumParticipants=" + mNumParticipants
				+ ",\nmHighestFlagColour=" + mHighestFlagColour
				+ ",\nmHighestFlagReason=" + mHighestFlagReason
				+ ",\nmPitMode=" + mPitMode + ",\nmPitSchedule="
				+ mPitSchedule + ",\nmUnfilteredThrottle="
				+ mUnfilteredThrottle + ",\nmUnfilteredBrake="
				+ mUnfilteredBrake + ",\nmUnfilteredSteering="
				+ mUnfilteredSteering + ",\nmUnfilteredClutch="
				+ mUnfilteredClutch + ",\nmCarName=" + new String(mCarName)
				+ ",\nmCarClassName=" + new String(mCarClassName) + ",\nmTrackLocation="
				+ new String(mTrackLocation) + ",\nmTrackVariation=" + new String(mTrackVariation)
				+ ",\nmTrackLength=" + mTrackLength + ",\nmLapInvalidated="
				+ mLapInvalidated + ",\nmBestLapTime=" + mBestLapTime
				+ ",\nmLastLapTime=" + mLastLapTime + ",\nmCurrentTime="
				+ mCurrentTime + ",\nmSplitTimeAhead=" + mSplitTimeAhead
				+ ",\nmSplitTimeBehind=" + mSplitTimeBehind
				+ ",\nmSplitTime=" + mSplitTime + ",\nmEventTimeRemaining="
				+ mEventTimeRemaining + ",\nmPersonalFastestLapTime="
				+ mPersonalFastestLapTime + ",\nmWorldFastestLapTime="
				+ mWorldFastestLapTime + ",\nmCurrentSector1Time="
				+ mCurrentSector1Time + ",\nmCurrentSector2Time="
				+ mCurrentSector2Time + ",\nmCurrentSector3Time="
				+ mCurrentSector3Time + ",\nmFastestSector1Time="
				+ mFastestSector1Time + ",\nmFastestSector2Time="
				+ mFastestSector2Time + ",\nmFastestSector3Time="
				+ mFastestSector3Time + ",\nmPersonalFastestSector1Time="
				+ mPersonalFastestSector1Time
				+ ",\nmPersonalFastestSector2Time="
				+ mPersonalFastestSector2Time
				+ ",\nmPersonalFastestSector3Time="
				+ mPersonalFastestSector3Time + ",\nmWorldFastestSector1Time="
				+ mWorldFastestSector1Time + ",\nmWorldFastestSector2Time="
				+ mWorldFastestSector2Time + ",\nmWorldFastestSector3Time="
				+ mWorldFastestSector3Time + ",\nmLapsInEvent=" + mLapsInEvent
				+ ",\nmCarFlags=" + mCarFlags + ",\nmOilTempCelsius="
				+ mOilTempCelsius + ",\nmOilPressureKPa=" + mOilPressureKPa
				+ ",\nmWaterTempCelsius=" + mWaterTempCelsius
				+ ",\nmWaterPressureKPa=" + mWaterPressureKPa
				+ ",\nmFuelPressureKPa=" + mFuelPressureKPa
				+ ",\nmFuelLevel=" + mFuelLevel + ",\nmFuelCapacity="
				+ mFuelCapacity + ",\nmSpeed=" + mSpeed + ",\nmRPM=" + mRPM
				+ ",\nmMaxRPM=" + mMaxRPM + ",\nmBrake=" + mBrake
				+ ",\nmThrottle=" + mThrottle + ",\nmClutch=" + mClutch
				+ ",\nmSteering=" + mSteering + ",\nmGear=" + mGear
				+ ",\nmNumGears=" + mNumGears + ",\nmOdometerKM="
				+ mOdometerKM + ",\nmLastOpponentCollisionIndex="
				+ mLastOpponentCollisionIndex
				+ ",\nmLastOpponentCollisionMagnitude="
				+ mLastOpponentCollisionMagnitude + ",\nmBoostActive="
				+ mBoostActive + ",\nmBoostAmount=" + mBoostAmount
				+ ",\nisHeadlightLit="
				+ isHeadlightLit + ",\nisEngineActive=" + isEngineActive
				+ ",\nisEngineWarning=" + isEngineWarning
				+ ",\nisSpeedLimited=" + isSpeedLimited + ",\nisAbsEnabled="
				+ isAbsEnabled + ",\nisAbsActive=" + isAbsActive
				+ ",\nisHandbrakeActive=" + isHandbrakeActive
				+ ",\nmMaxTorque=" + mMaxTorque + ",\nmMaxPower=" + mMaxPower
				+ ",\nmOrientation=" + Arrays.toString(mOrientation)
				+ ",\nmLocalVelocity=" + Arrays.toString(mLocalVelocity)
				+ ",\nmWorldVelocity=" + Arrays.toString(mWorldVelocity)
				+ ",\nmAngularVelocity=" + Arrays.toString(mAngularVelocity)
				+ ",\nmLocalAcceleration="
				+ Arrays.toString(mLocalAcceleration)
				+ ",\nmWorldAcceleration="
				+ Arrays.toString(mWorldAcceleration) + ",\nmExtentsCentre="
				+ Arrays.toString(mExtentsCentre) + ",\nmGs="
				+ Arrays.toString(mGs) + ",\nmTyreFlags="
				+ Arrays.toString(mTyreFlags) + ",\nmTerrain="
				+ Arrays.toString(mTerrain) + ",\nmTyreY="
				+ Arrays.toString(mTyreY) + ",\nmTyreRPS="
				+ Arrays.toString(mTyreRPS) + ",\nmTyreSlipSpeed="
				+ Arrays.toString(mTyreSlipSpeed) + ",\nmTyreTemp="
				+ Arrays.toString(mTyreTemp) + ",\nmTyreGrip="
				+ Arrays.toString(mTyreGrip) + ",\nmTyreHeightAboveGround="
				+ Arrays.toString(mTyreHeightAboveGround)
				+ ",\nmTyreLateralStiffness="
				+ Arrays.toString(mTyreLateralStiffness) + ",\nmTyreWear="
				+ Arrays.toString(mTyreCondition) + ",\nmBrakeDamage="
				+ Arrays.toString(mBrakeCondition) + ",\nmBrakeTempCelcius="
				+ Arrays.toString(mBrakeTempCelcius) + ",\nmSuspensionDamage="
				+ Arrays.toString(mSuspensionCondition) + ",\nmTyreTreadTemp="
				+ Arrays.toString(mTyreTreadTemp) + ",\nmTyreLayerTemp="
				+ Arrays.toString(mTyreLayerTemp) + ",\nmTyreCarcassTemp="
				+ Arrays.toString(mTyreCarcassTemp) + ",\nmTyreRimTemp="
				+ Arrays.toString(mTyreRimTemp) + ",\nmTyreInternalAirTemp="
				+ Arrays.toString(mTyreInternalAirTemp) + ",\nmCrashState="
				+ mCrashState + ",\nmAeroDamage=" + mAeroCondition
				+ ",\nmEngineDamage=" + mEngineCondition
				+ ",\nmAmbientTemperature=" + mAmbientTemperature
				+ ",\nmTrackTemperature=" + mTrackTemperature
				+ ",\nmRainDensity=" + mRainDensity + ",\nmWindSpeed="
				+ mWindSpeed + ",\nmWindDirectionX=" + mWindDirectionX
				+ ",\nmWindDirectionY=" + mWindDirectionY
				+ ",\nmCloudBrightness=" + mCloudBrightness
				+ ",\nmParticipants=" + Arrays.toString(mParticipants) + "]";
	}

	private void init() {
		mOrientation = new float[VECTOR.VEC_MAX];
		mLocalVelocity = new float[VECTOR.VEC_MAX];
		mWorldVelocity = new float[VECTOR.VEC_MAX];
		mAngularVelocity = new float[VECTOR.VEC_MAX];
		mLocalAcceleration = new float[VECTOR.VEC_MAX];
		mWorldAcceleration = new float[VECTOR.VEC_MAX];
		mExtentsCentre = new float[VECTOR.VEC_MAX];
		mGs = new float[VECTOR.VEC_MAX];

		mTyreFlags = new int[TYRES.TYRE_MAX];
		mTerrain = new int[TYRES.TYRE_MAX];
		mTyreY = new float[TYRES.TYRE_MAX];
		mTyreRPS = new float[TYRES.TYRE_MAX];
		mTyreSlipSpeed = new float[TYRES.TYRE_MAX];
		mBrakeTempCelcius = new float[TYRES.TYRE_MAX];
		mTyreTemp = new float[TYRES.TYRE_MAX];
		mTyrePressures = new float[TYRES.TYRE_MAX];
		mTyreTreadTemp = new float[TYRES.TYRE_MAX];
		mTyreLayerTemp = new float[TYRES.TYRE_MAX];
		mTyreCarcassTemp = new float[TYRES.TYRE_MAX];
		mTyreRimTemp = new float[TYRES.TYRE_MAX];
		mTyreInternalAirTemp = new float[TYRES.TYRE_MAX];
		mTyreGrip = new float[TYRES.TYRE_MAX];
		mTyreHeightAboveGround = new float[TYRES.TYRE_MAX];
		mTyreLateralStiffness = new float[TYRES.TYRE_MAX];
		mTyreCondition = new float[TYRES.TYRE_MAX];
		mBrakeCondition = new float[TYRES.TYRE_MAX];
		mSuspensionCondition = new float[TYRES.TYRE_MAX];
		mTyreInnerTemps = new float[TYRES.TYRE_MAX];
		mTyreMidTemps = new float[TYRES.TYRE_MAX];
		mTyreOuterTemps = new float[TYRES.TYRE_MAX];

		mCarName = null;
		mCarClassName = null;
		mTrackLocation = null;
		mTrackVariation = null;

		gameType = GAME_TYPE.ProjectCARS;
	}

	public static float convertCtoF(float c){
		return (c * 1.8f) + 32f;
	}
	public static float convertFtoC(float f){
		return (f - 32f) / 1.8f;
	}

	public static float convertKpaToPsi(float kpa){
		return kpa * KPA_TO_PSI;
	}
	public static float convertPsiToKpa(float psi){
		return psi / KPA_TO_PSI;
	}

	public static float convertKpaToBar(float kpa){
		return kpa * KPA_TO_BAR;
	}
	public static float convertBarToKpa(float bar){
		return bar / KPA_TO_BAR;
	}

	public static float convertMphToKph(float mph){
		return mph * 1.60934f;
	}
	public static float convertMphToMps(float mph){
		return convertKphToMps(convertMphToKph(mph));
	}
	public static float convertKphToMph(float kph){
		return kph / 1.60934f;
	}
	public static float convertKphToMps(float kph){
		return (kph * 1000) / 3600f;
	}
	public static float convertMpsToKph(float mps){
		return mps * 3.6f;
	}
	public static float convertMpsToMph(float mps){
		return convertKphToMph(convertMpsToKph(mps));
	}

	public static float convertKelvinToC(float k){
		return k-273.15f;
	}
	public static float convertKelvinToF(float k){
		return convertCtoF(convertKelvinToC(k));
	}

	public static class DataFormat {
		public int dataType;
		public String formatString;

		public DataFormat(int dataType, String formatString) {
			this.dataType = dataType;
			this.formatString = formatString;
		}
	}

	/**
	 * Get the current lap of the currently viewed participant, or -1 if no participants available
	 * NOT ZERO BASED
	 * @return
	 */
	public int getCurrentLap() {
		if(mParticipants != null && mViewedParticipantIndex >= 0 && mViewedParticipantIndex < mParticipants.length){
			return mParticipants[mViewedParticipantIndex].getCurrentLap();
		}else{
			return -1;
		}
	}
	/**
	 * Get the current sector of the currently viewed participant, or -1 if no participants available
	 * @return
	 */
	public int getCurrentSector() {
		if(mParticipants != null){
			return mParticipants[mViewedParticipantIndex].getCurrentSector();
		}else{
			return -1;
		}
	}
	/**
	 * Get the current position of the currently viewed participant, or -1 if no participants available
	 * @return
	 */
	public int getCurrentPosition() {
		if(mParticipants != null){
			return mParticipants[mViewedParticipantIndex].getRacePosition();
		}else{
			return -1;
		}
	}

	public void setNumberOfParticipants(int n){
		this.mNumParticipants = n;
	}
	public int getNumberOfParticipants() {
		return mNumParticipants;
	}

	public void sanatize(){
		mMaxRPM = Math.max(0, mMaxRPM);
		mOilTempCelsius = Math.max(0, mOilTempCelsius);
		mOilPressureKPa = Math.max(0, mOilPressureKPa);
		mWaterTempCelsius = Math.max(0, mWaterTempCelsius);
		mWaterPressureKPa = Math.max(0, mWaterPressureKPa);
	}

	public static class ApiVersionException extends IOException{
		/**
		 * 
		 */
		private static final long serialVersionUID = 4252399324491064278L;
		private byte expected, received;

		public ApiVersionException(byte expected, byte received){
			super("I/O API version error! Expected "+expected+", received "+received);
			this.expected = expected;
			this.received = received;
		}

		public byte expected(){return expected;}
		public byte received(){return received;}
	}
}
