package data;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;
import data.VDashData.Participant;
import gui.LogMessager;

/**
 * @author Alex
 *
 */
//https://www.lfsforum.net/showthread.php?p=1586584#post1586584
/*
 * Triple post 

	F1 2010's extra data is the same size as DiRT2's, the differences are minor:
	Code:
	#	offset	type	description
	-----------------------------------
	0	0	float	time elapsed in seconds (since sending these packets started)
	1	4	float 	laptime in seconds
	2	8	float	position on track in metres from the start/finish line
	3	12	float	race progress, this - laps = lap progress
	4	16	float	world-Y position in metres
	5	20	float	world-Z position in metres
	6	24	float	world-X position in metres
	7	28	float	actual velocity in m/s
	8	32	float	world-Y velocity in m/s
	9	36	float	world-Z velocity in m/s
	10	40	float	world-X velocity in m/s

	11	44	float	?
	12	48	float	? roll *
	13	52	float	?

	14	56	float	?
	15	60	float	? pitch *
	16	64	float	?

	17	68	float	?	suspension travel front left?? *
	18	72	float	?	suspension travel front right?? *
	19	76	float	?	suspension travel rear left?? *
	20	80	float	?	suspension travel rear right?? *

	21	84	float	?
	22	88	float	?
	23	92	float	?
	24	96	float	?

	25	100	float	velocity rear left wheel in m/s *
	26	104	float	velocity rear right wheel in m/s *
	27	108	float	velocity front left wheel in m/s *
	28	112	float	velocity front right wheel in m/s *
	29	116	float	throttle 0 to 1
	30	120	float	steering in quarter turns (90�), negative = left, positive = right
	31	124	float	brakes 0 to 1
	32	128	float	anti-stall / clutch
	33	132	float	gear 0 = Neutral, 10 = Reverse, forwards speeds counting from 1
	34	136	float	lateral acceleration in g
	35	140	float	longitudinal acceleration in g
	36	144	float	lap counting from 0 (first lap = 0) *
	37	148	float	Revolutions per 6 seconds, multiply by 10.0 to get RPM
	float 33 (#32) is the only content change, representing the anti-stall or clutch in F1 instead of the handbrake (GRID/DiRT2) for obvious reasons. Other than that there is only a minor change to #33, where reverse is now 10 instead of 9. Finally, GRID and DiRT2 reset the first float (time elapsed) on restarts, F1 does not.

	I haven't fully verified everything yet, but I'm reasonably sure that's all the differences.

	I've updated the other two definitions with a more accurate interpretation of the steering value.
 */
public class DirtDataSource extends DataSource {

	private static final int DIRT_2_PORT = 20777;
	private DatagramSocket socket;

	private boolean running = false;

	private Thread listenThread;
	private byte[] processingData = new byte[Constants.Comms.MAX_PACKET_SIZE];
	private ByteBuffer buffer;

	private boolean r = false;
	private boolean closeReq = false;
	
	float maxRpm;

	private Runnable listenRunnable = new Runnable() {
		@Override
		public void run() {
			logMessage("Starting DiRT2 listen thread", LOG_LVL.L0);
			while(running && !Thread.interrupted()){
				ready = true;
				DatagramPacket packet = new DatagramPacket(new byte[Constants.Comms.MAX_PACKET_SIZE], Constants.Comms.MAX_PACKET_SIZE);
				try {
					socket.receive(packet);
					readDirtData(packet.getData());
				} catch (IOException e) {
					if(!closeReq){
						logMessage("DiRT2 data source connection error: "+e.getMessage(), LOG_LVL.L0);
						e.printStackTrace();
					}
					close();
				}

			}
		}
	};

	public DirtDataSource(LogMessager m){
		super(m);
		type = GAME_TYPE.DiRT2;
	}

	@Override
	public void open() throws IOException{
		closeReq=false;
		if(!r){
			running = true;
			socket = new DatagramSocket(null);
			socket.bind(new InetSocketAddress("localhost", DIRT_2_PORT));
			logMessage("DiRT2 data source listening on: "+socket.getLocalAddress()+":"+socket.getLocalPort(), LOG_LVL.L1);
			buffer  = ByteBuffer.wrap(processingData);
			buffer.order(ByteOrder.nativeOrder());
			data = new VDashData();
			listenThread = new Thread(listenRunnable);
			listenThread.start();
			r = true;
		}
	}

	@Override
	public void doClose(){
		closeReq = true;
		running = false;
		listenThread.interrupt();
		socket.close();
	}

	private void readDirtData(byte[] dirtdata){
		buffer.clear();
		buffer.put(dirtdata);
		buffer.flip();
		
		float apitime = buffer.getFloat(0);
		float currentLapTime = buffer.getFloat(4);
		float trackDistanceCovered = buffer.getFloat(8);
		float raceProgress = buffer.getFloat(12);

		float[] worldPos = new float[VDashData.VECTOR.VEC_MAX];
		worldPos[VDashData.VECTOR.VEC_X] = buffer.getFloat(24);
		worldPos[VDashData.VECTOR.VEC_Y] = buffer.getFloat(16);
		worldPos[VDashData.VECTOR.VEC_Z] = buffer.getFloat(20);

		float mps = buffer.getFloat(28);
		float throttle = buffer.getFloat(116);
		float brake = buffer.getFloat(124);
		float handbrake = buffer.getFloat(128); 
		int gear = (int)buffer.getFloat(132);
		if(gear == 10)gear = -1;

		float[] latAcc = new float[VDashData.VECTOR.VEC_MAX];
		latAcc[VDashData.VECTOR.VEC_X] = buffer.getFloat(136);
		latAcc[VDashData.VECTOR.VEC_Y] = buffer.getFloat(140);

		int completedLapCount = (int)buffer.getFloat(144);
		float rpm = buffer.getFloat(148)*10f;
		
		if(currentLapTime == 0){
			maxRpm = 0;
		}
		if(rpm > maxRpm){
			maxRpm = rpm;
		}

		data.setGameType(type);
		data.setGameState(VDashData.GAME_STATES.GAME_INGAME_PLAYING);
		data.setCurrentLapTime(currentLapTime);
		data.setMps(mps);
		data.setThrottle(throttle);
		data.setBrake(brake);
		//TODO handbrake?
		data.setClutch(handbrake);
		data.setGear(gear);
		data.setLocalAcceleration(latAcc);
		data.setRpm(rpm);
		data.setMaxRpm(maxRpm);

		Participant p = new Participant();
		p.setCurrentLap(completedLapCount);
		p.setWorldPosition(worldPos);
		p.setIsActive(true);
		p.setCurrentLapDistance(raceProgress - completedLapCount);

		Participant[] ps = new Participant[]{p};
		data.setParticipants(ps);
		data.setViewedParticipantIndex(0);
	}
}
