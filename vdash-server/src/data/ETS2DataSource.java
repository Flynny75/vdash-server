package data;

import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;
import gui.LogMessager;

public class ETS2DataSource extends DataSource{
	
	private static final String fileMappingObjName = "$vdash_ets2_plugin$";
	private long filePtr;
	
	static {
		System.loadLibrary("data_ETS2DataSource"+System.getProperty("sun.arch.data.model"));
		ETS2DataSource.findFieldIDs();
	}

	public ETS2DataSource(LogMessager m) {
		super(m);
		type = GAME_TYPE.ETS2;
	}

	@Override
	public void open() {
		if(simulate)return;

		filePtr = ETS2DataSource.openFileMapping(fileMappingObjName);
		if(filePtr != 0) {
			int viewPtr = ETS2DataSource.mapViewOfFile(filePtr);
			if(viewPtr != 0) {
				ready = true;

				logMessage("Opened ETS2 memory file successfully", LOG_LVL.L0);
			}else{
				//Could find file, but could not map/read
				ready = false;
			}
		}else{
			ready = false;
			if(!loggedOpenFailure){
				String s1 = "\n-- WARNING --";
				String s2 = "Could not open ETS2 memory file. Is it running?";
				String s3 = "-------------\n";

				logMessage(s1, LOG_LVL.L0);
				logMessage(s2, LOG_LVL.L0);
				logMessage(s3, LOG_LVL.L0);
				loggedOpenFailure = true;
			}
		}
	}
	
	@Override
	public VDashData readData() throws NullSimulationFileException{
		if(simulate){
			return super.readData();
		}
		if(ready){
			data =  getData();
			data.setViewedParticipantIndex(0);
			data.setGameType(type);

			//Set lastSectorTime (from AC)
			if(data.getCurrentSector() == 0){
				data.setLastSectorTime(0);
			}else if(data.getCurrentSector() == 1){
				data.setLastSectorTime(data.getCurrentSector1Time());
			}else if(data.getCurrentSector() == 2){
				data.setLastSectorTime(data.getCurrentSector2Time());
			}

			return super.readData();
		}else{
			return super.readData();
		}
	}

	@Override
	protected void doClose() {
		ETS2DataSource.closeHandle(filePtr);
	}

	private static native long openFileMapping(String name);
	private static native int mapViewOfFile(long hFileMappingObj);
	private static native boolean closeHandle(long hObject);
	private static native void findFieldIDs();
	private static native VDashData getData();
}
