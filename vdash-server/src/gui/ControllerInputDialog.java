package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ControllerInputDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7793345321886679413L;
	private final JPanel contentPanel = new JPanel();
	private JLabel messageLabel;
	private Runnable okListener,cancelListener;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ControllerInputDialog dialog = new ControllerInputDialog("SOME TEXT HERE");
			dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ControllerInputDialog(String defaultMessage) {
		if(null == defaultMessage){
			defaultMessage = "DEFAULT";
		}
		setBounds(100, 100, 207, 129);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			messageLabel = new JLabel(defaultMessage);
			contentPanel.add(messageLabel);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				okButton.addMouseListener(new MouseAdapter() {

					@Override
					public void mouseClicked(MouseEvent e) {
						if(okListener != null){
							okListener.run();
						}
						setVisible(false);
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addMouseListener(new MouseAdapter(){
					@Override
					public void mouseClicked(MouseEvent e){
						if(cancelListener != null){
							cancelListener.run();
						}
						setVisible(false);
						dispose();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}

	public void setListeners(Runnable ok, Runnable cancel){
		this.okListener = ok;
		this.cancelListener = cancel;
	}
	
	public void changeMessage(String newMessage){
		messageLabel.setText(newMessage);
	}
}
