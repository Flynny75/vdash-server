package gui;

import data.Settings.LOG_LVL;

public interface LogMessager{
	public void logMessage(String message);
	public void logMessage(String message, LOG_LVL lvl);
}
