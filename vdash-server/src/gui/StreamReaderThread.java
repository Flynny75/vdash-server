package gui;

import java.io.BufferedReader;
import java.io.IOException;

public class StreamReaderThread{
	private boolean noDeviceFound = false;
	private Thread t;
	private BufferedReader mreader;
	private LogMessager m;

	public StreamReaderThread(LogMessager m){
		this.m = m;
	}
	
	public void start(final BufferedReader reader){
		mreader = reader;
		t = new Thread(new Runnable() {
			@Override
			public void run() {
				String s;
				try {
					while((s = mreader.readLine()) != null){
						if(s.equals("error: device not found") || s.equals("error: device offline")){
							noDeviceFound = true;
						}
						logMessage("ADB - "+s);
					}
				} catch (IOException e) {
					logMessage("reader stream closed STREAM Y U NO CLOSE");
					e.printStackTrace();
				}
			}
		});
		t.start();
	}
	public boolean noDeviceFound(){return noDeviceFound;}
	public Thread getThread(){return t;}
	
	private void logMessage(String s){
		m.logMessage(s);
	}
}
