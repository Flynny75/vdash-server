package gui;

import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.zip.ZipException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import net.miginfocom.swing.MigLayout;
import comms.NetworkingBase;
import comms.ServerEventListener;
import comms.WirelessUDPServer;
import comms.WiredTCPServer;
import comms.ServerEventListener.CONNECTION_STATE;
import data.Constants;
import data.InputDeviceManager;
import data.InputDeviceManager.EventFromInputDeviceListener;
import data.Settings;
import data.Settings.CONNECTION_TYPE;
import data.Settings.LOG_LVL;

import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class Gui implements LogMessager{

	static{
		if ("64".equals(System.getProperty("sun.arch.data.model"))) {
			System.loadLibrary("jinput-dx8_64");			
		}
		else {
			System.loadLibrary("jinput-dx8");
		}
	}

	private JFrame frmPcarsJavaMonitor;
	private JTextArea textArea;
	private JLabel runningStatusLabel;
	private JScrollPane scrollPane;
	private JLabel lblImage;
	private JButton wifiButton, usbButton;

	private int serverVersion = 104;

	private NetworkingBase serverListener;

	private static final String ADB_FILENAME = "adb.exe";
	private static final String ADB_DLL_FILENAME = "AdbWinApi.dll";
	private static final int ADB_TIMEOUT_MILLIS = 7000;
	private static String pathToAdb;
	private JButton btnDetails;
	private JLabel lblExtraInfo;
	
	private ServerEventListener serverEventListener = new ServerEventListener() {
		@Override
		public void onConnectionStateChange(String state, CONNECTION_STATE connState) {
			runningStatusLabel.setText(state);

			if(serverListener != null){
				if(connState != CONNECTION_STATE.DISCONNECTED){
					if(serverListener instanceof WirelessUDPServer){
						wifiButton.setText("Stop");
					}else{
						usbButton.setText("Stop");
					}
				}else{
					if(serverListener instanceof WirelessUDPServer){
						wifiButton.setText("WiFi");
					}else{
						usbButton.setText("USB");
					}
				}
			}
		}
	};

	/**
	 * Receive events from input devices like wheels or 360 controllers
	 */
	private EventFromInputDeviceListener inputEventListener = new EventFromInputDeviceListener() {
		@Override
		public void receiveInputEvent(int event) {
			if(serverListener != null){
				System.out.println("Got server input event : "+event);
				serverListener.receiveInputEvent(event);
			}
		}
	};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					final Gui window = new Gui();
					window.frmPcarsJavaMonitor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gui() {
		Settings.getInstance().reloadFromFile();

		initialize();

		frmPcarsJavaMonitor.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmPcarsJavaMonitor.addWindowListener(new WindowListenerStub() {
			@Override
			public void windowClosing(WindowEvent e) {
				InputDeviceManager.getInstance().dropAllControllers();
				if(pathToAdb != null){
					try {
						cleanupAdb(false);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
				frmPcarsJavaMonitor.setVisible(false);
				frmPcarsJavaMonitor.dispose();
				System.exit(0);
			}
		});

		InputDeviceManager.getInstance().setEventListener(inputEventListener);
		InputDeviceManager.getInstance().attempRestoreFromSettings();

		// Autostart server?
		Runnable r = new Runnable() {
			@Override
			public void run() {
				try {
					if (Settings.getInstance().getAutoConnect()) {
						if (CONNECTION_TYPE.USB.equals(Settings.getInstance().getAutoConnectType())) {
							logMessage("Automatically starting USB Server");
							startUSB();
						}
						else if (CONNECTION_TYPE.WiFi.equals(Settings.getInstance().getAutoConnectType())) {
							logMessage("Automatically starting WiFi Server");
							autoStartWifi();
						}
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}	
			}
		};
		Thread t = new Thread(r);
		t.start();


		//Check for updates
		Runnable updateChecker = new Runnable() {
			@Override
			public void run() {
				InputStream is = null;
				try {
					lblExtraInfo.setText("Checking with vdash.pro for updates...");
					is = new URL("http://vdash.pro/server/version.txt").openStream();
					BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

					StringBuilder sb = new StringBuilder();
					int cp;
					while ((cp = rd.read()) != -1) {
						sb.append((char) cp);
					}

					JSONObject json = (JSONObject) JSONValue.parse(sb.toString());
					long version = (Long) json.get("version");
					final String description = (String)json.get("desc");

					if(version > serverVersion){
						lblExtraInfo.setText("Server "+version+" available!");
						btnDetails.setEnabled(true);
						btnDetails.addMouseListener(new MouseAdapter(){
							@Override
							public void mouseClicked(MouseEvent e) {
								JLabel label = new JLabel();
								Font font = label.getFont();

								// create some css from the label's font
								StringBuffer style = new StringBuffer("font-family:" + font.getFamily() + ";");
								style.append("font-weight:" + (font.isBold() ? "bold" : "normal") + ";");
								style.append("font-size:" + font.getSize() + "pt;");

								// html content
								final JEditorPane ep = new JEditorPane("text/html", "<html><body style=\"" + style + "\">" //
										+ description
										+ "</body></html>");

								// handle link events
								ep.addHyperlinkListener(new HyperlinkListener(){
									@Override
									public void hyperlinkUpdate(HyperlinkEvent e){
										if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)){
											Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
											if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
												try {
													desktop.browse(e.getURL().toURI());
												} catch (Exception e2) {
													e2.printStackTrace();
												}
											}
											SwingUtilities.getWindowAncestor(ep).dispose();
										}
									}
								});
								ep.setEditable(false);
								ep.setBackground(label.getBackground());

								JOptionPane.showMessageDialog(frmPcarsJavaMonitor, ep);
							}
						});
					}else{
						lblExtraInfo.setText("You are using the latest version");
					}
				} catch (IOException e) {
					logMessage("Could not check for updates");
					e.printStackTrace();
				} finally {
					try {
						if(is != null){
							is.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		};

		Thread updateCheckerThread = new Thread(updateChecker);
		updateCheckerThread.start();
	}

	public NetworkingBase getNetworkingBase(){
		return serverListener;
	}

	public void setGameImage(){
		String path = "/images/"+Settings.getInstance().getGameType().getImageFilename();
		lblImage.setIcon(new ImageIcon(Gui.class.getResource(path)));
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPcarsJavaMonitor = new JFrame();
		frmPcarsJavaMonitor.setTitle("VDash Server S"+serverVersion);
		frmPcarsJavaMonitor.setBounds(100, 100, 659, 405);
		frmPcarsJavaMonitor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPcarsJavaMonitor.getContentPane().setLayout(new MigLayout("", "[][][][grow][10px]", "[][][][][][grow][]"));

		runningStatusLabel = new JLabel("Not running");
		frmPcarsJavaMonitor.getContentPane().add(runningStatusLabel, "cell 0 0 5 1,growx");

		JLabel lblNewLabel = new JLabel("Start Server");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 18));
		frmPcarsJavaMonitor.getContentPane().add(lblNewLabel, "cell 0 1 3 1");

		wifiButton = new JButton("Wifi ");
		wifiButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(serverListener == null || serverListener.getConnectionState() == CONNECTION_STATE.DISCONNECTED){
					try {
						wifiButtonClicked();
					} catch (UnknownHostException e) {
						e.printStackTrace();
					} catch (SocketException e) {
						e.printStackTrace();
					}
				}else{
					serverListener.shutdown();
					serverEventListener.onConnectionStateChange("Server stopped", CONNECTION_STATE.DISCONNECTED);
					serverListener = null;
				}
			}
		});

		lblImage = new JLabel();
		setGameImage();
		lblImage.setHorizontalAlignment(SwingConstants.RIGHT);
		frmPcarsJavaMonitor.getContentPane().add(lblImage, "cell 3 1 2 3,alignx right");
		frmPcarsJavaMonitor.getContentPane().add(wifiButton, "cell 0 2");

		usbButton = new JButton("USB");
		usbButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(serverListener == null || serverListener.getConnectionState() == CONNECTION_STATE.DISCONNECTED){
					try {
						usbButtonClicked();
					} catch (URISyntaxException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}else{
					serverListener.shutdown();
					serverEventListener.onConnectionStateChange("Server stopped", CONNECTION_STATE.DISCONNECTED);
					serverListener = null;
				}
			}
		});
		frmPcarsJavaMonitor.getContentPane().add(usbButton, "cell 2 2,alignx left");

		scrollPane = new JScrollPane();
		frmPcarsJavaMonitor.getContentPane().add(scrollPane, "cell 0 4 5 2,grow");

		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);

		JButton btnSettings = new JButton("Settings");
		btnSettings.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				final SettingsDialog dialog = new SettingsDialog(frmPcarsJavaMonitor.getBounds().x, frmPcarsJavaMonitor.getBounds().y, Gui.this);
				dialog.setVisible(true);
			}
		});

		JLabel lblNewLabel_1 = new JLabel("Log");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 18));
		frmPcarsJavaMonitor.getContentPane().add(lblNewLabel_1, "cell 0 3,aligny bottom");
		frmPcarsJavaMonitor.getContentPane().add(btnSettings, "cell 0 6 3 1,growx");

		lblExtraInfo = new JLabel("");
		frmPcarsJavaMonitor.getContentPane().add(lblExtraInfo, "cell 3 6,alignx right");

		btnDetails = new JButton("Details");
		btnDetails.setEnabled(false);
		frmPcarsJavaMonitor.getContentPane().add(btnDetails, "cell 4 6,alignx right");
	}

	/**
	 * Start the WiFi service. Optional dialog for custom port. Throws exceptions from starting server.
	 * @throws SocketException 
	 * @throws UnknownHostException 
	 */
	private void wifiButtonClicked() throws UnknownHostException, SocketException{
		textArea.setText("");
		serverEventListener.onConnectionStateChange("Setup", CONNECTION_STATE.CONNECTING);
		int result = JOptionPane.showOptionDialog(frmPcarsJavaMonitor, //parent
				"Pick port",								//message
				"WiFi Server",								//title
				JOptionPane.YES_NO_CANCEL_OPTION,			//option types
				JOptionPane.QUESTION_MESSAGE,				//message type
				null,										//icon
				new String[]{"Default (1234)","Custom"},	//options
				"Default (1234)");									//default

		if(result != JOptionPane.CLOSED_OPTION){ //Dialog was Xed
			if(result == 0){
				showAutoStartOptionDialog(CONNECTION_TYPE.WiFi);
				serverListener = new WirelessUDPServer(serverEventListener,this);
			}
			else{
				//Start dialog asking for custom port
				String customPort = JOptionPane.showInputDialog(frmPcarsJavaMonitor, "Enter port");
				int portVal;
				if(customPort != null){
					try{
						portVal = Integer.valueOf(customPort);
						showAutoStartOptionDialog(CONNECTION_TYPE.WiFi, portVal);
						serverListener = new WirelessUDPServer(serverEventListener, this, portVal);
					}
					catch(NumberFormatException e){
						JOptionPane.showMessageDialog(frmPcarsJavaMonitor,"Must be a number EG 1234","Error",JOptionPane.ERROR_MESSAGE);
						wifiButtonClicked();
					}
				}else{
					serverEventListener.onConnectionStateChange("Custom port cannot be null", CONNECTION_STATE.DISCONNECTED);
				}
			}
		}
	}

	private void showAutoStartOptionDialog(CONNECTION_TYPE type) {
		showAutoStartOptionDialog(type, null);
	}
	private void showAutoStartOptionDialog(CONNECTION_TYPE type, Integer port) {
		if (!Settings.getInstance().getAutoConnect()) {

			String message = "Should the server be started automatically on next launch?\n";
			message += "Connection Type: " + type;
			if (CONNECTION_TYPE.WiFi.equals(type)) {
				message += "\nPort: ";
				if (port == null) {
					message += "default (1234)";
				}
				else {
					message += port;
				}
			}

			int result = JOptionPane.showOptionDialog(frmPcarsJavaMonitor, //parent
					message,
					"Autostart option",								//title
					JOptionPane.YES_NO_CANCEL_OPTION,			//option types
					JOptionPane.QUESTION_MESSAGE,				//message type
					null,										//icon
					new String[]{"Yes","No"},	//options
					"Yes");

			if (result == 0) {
				Settings.getInstance().setAutoConnect(true);
				Settings.getInstance().setAutoConnectType(type);
				Settings.getInstance().setAutoConnectPort(port);
				Settings.getInstance().saveSettings();
			}
		}
	}

	private void autoStartWifi() throws UnknownHostException, SocketException {
		if (Settings.getInstance().getAutoConnectPort() == null) {
			serverListener = new WirelessUDPServer(serverEventListener, this);
		}
		else {
			serverListener = new WirelessUDPServer(serverEventListener, this, Settings.getInstance().getAutoConnectPort());
		}
	}

	public void usbButtonClicked() throws URISyntaxException, IOException {
		startUSB();
		showAutoStartOptionDialog(CONNECTION_TYPE.USB);
	}

	private void startUSB() throws URISyntaxException, IOException {
		//Clear text
		textArea.setText("");
		
		//Attempt to copy the ADB binaries to a temp folder
		try{
			pathToAdb = getFile(getClass().getClassLoader(), ADB_FILENAME);
			getFile(getClass().getClassLoader(), ADB_DLL_FILENAME);
		}catch(IOException e){
			serverEventListener.onConnectionStateChange("Could not copy binaries to temp folder. Can not proceed", CONNECTION_STATE.DISCONNECTED);
			throw e;
		}

		serverEventListener.onConnectionStateChange("Setup",CONNECTION_STATE.CONNECTING);
		String msg = "1. Make sure USB debugging is enabled on your Android device\n2. Connect your android device to your PC. Press OK to continue";
		logMessage("Starting USB server...");
		int result = JOptionPane.showConfirmDialog(frmPcarsJavaMonitor, msg, "Start USB server", JOptionPane.OK_CANCEL_OPTION);

		if(result != JOptionPane.YES_OPTION){
			serverEventListener.onConnectionStateChange("Not running", CONNECTION_STATE.DISCONNECTED);
			if(Settings.getInstance().getKillCleanAdbOnExit()){
				cleanupAdb(false);
			}
			return;
		}

		//Attempt to forward the port
		ProcessBuilder pBuild = new ProcessBuilder(pathToAdb,"forward","tcp:"+Constants.Comms.SERVER_LISTEN_PORT,"tcp:"+Constants.Comms.SERVER_LISTEN_PORT);
		pBuild.redirectErrorStream(true);
		Process proc = pBuild.start();
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		
		//Redirect process output to the StreamReaderThread
		StreamReaderThread readerThread = new StreamReaderThread(this);
		readerThread.start(stdInput);

		Timer t = new Timer();
		
		//Interrupt the threads after a timeout
		InterruptTimerTask task = new InterruptTimerTask(this);
		task.addThread(Thread.currentThread());
		task.addThread(readerThread.getThread());

		t.schedule(task, ADB_TIMEOUT_MILLIS);
		try {
			proc.waitFor();
			//Give the StreamReaderThread a chance to exhaust its stream
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			t.cancel();
			//Cant just destroy because of forward (cant close cause its synced with read.. which is blocking.. so closing also blocks (WTF SUN))
			//http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4859836
			//So instead just shutdown and kill ADB (finally closing the reader)
			//proc.getInputStream().close();
			Thread.interrupted();
		}

		if(readerThread.noDeviceFound()){
			msg = "Device not found. Could not start server.";
			logMessage(msg);
			serverEventListener.onConnectionStateChange("Not running", CONNECTION_STATE.DISCONNECTED);
			JOptionPane.showMessageDialog(frmPcarsJavaMonitor, msg, "Could not start", JOptionPane.WARNING_MESSAGE);
			cleanupAdb(false);
			return;
		}
		
		//Device was found, device needs to start the server socket
		serverEventListener.onConnectionStateChange("Waiting for app",CONNECTION_STATE.CONNECTING);
		logMessage("Port forwarding complete\nStart a new connection in USB mode, then click OK");

		result = JOptionPane.showConfirmDialog(frmPcarsJavaMonitor, "Initiate a new connection in USB mode in the app, then click OK", "Waiting for app", JOptionPane.OK_CANCEL_OPTION);

		if(result != JOptionPane.OK_OPTION){
			logMessage("Connection cancelled");
			serverEventListener.onConnectionStateChange("Not running", CONNECTION_STATE.DISCONNECTED);
			if(Settings.getInstance().getKillCleanAdbOnExit()){
				cleanupAdb(false);
			}
			return;
		}

		//Finally start
		serverListener = new WiredTCPServer(serverEventListener, this);
	}
	
	/**
	 * Copy the file from resources to a tmp directory
	 */
	private static String getFile(ClassLoader cl, final String fileName)throws ZipException,IOException{
		InputStream is = cl.getResourceAsStream(fileName);
		File tempFile = new File(System.getProperty("java.io.tmpdir") + File.separator + fileName);

		FileOutputStream fileStream = null;
		if(tempFile.exists()) return tempFile.getAbsolutePath();

		try{
			final byte[] buf = new byte[1024];
			int i;

			fileStream = new FileOutputStream(tempFile);
			
			while((i = is.read(buf)) != -1){
				fileStream.write(buf, 0, i);
			}
			return tempFile.getAbsolutePath();
		}finally{
			close(is);
			close(fileStream);
		}
	}

	private static void close(final Closeable stream){
		if(stream != null){
			try{
				stream.close();
			}catch(final IOException ex){
				ex.printStackTrace();
			}
		}
	}

	/** Just a simple TimerTask that interrupts the specified threads when run.*/
	private static class InterruptTimerTask extends TimerTask{
		private LinkedList<Thread> threads = new LinkedList<Thread>();
		private LogMessager m;
		
		public InterruptTimerTask(LogMessager m){
			this.m = m;
		}
		
		public void addThread(Thread t){
			threads.add(t);
		}

		@Override
		public void run(){
			m.logMessage("ADB timeout!");
			for(Thread t:threads){
				t.interrupt();
			}
		}
	}

	/**
	 * Perform the ADB-KillServer command, and if in settings (or forced) delete the ADB files
	 * @param forceCleanup
	 * @throws IOException
	 */
	private void cleanupAdb(boolean forceCleanup) throws IOException{
		if(serverListener != null){
			serverListener.shutdown();
		}
		killAdb(forceCleanup, this);
	}
	
	/**
	 * Kill the adb server and attempt to delete the binaries if the Settings say so, or if forced
	 */
	public static void killAdb(boolean forceCleanup, LogMessager logger) throws IOException{
		if(Settings.getInstance().getKillCleanAdbOnExit() || forceCleanup){
			ProcessBuilder pBuild = new ProcessBuilder(pathToAdb,"kill-server");
			try{
				logger.logMessage("Killing ADB server...");
				int result = pBuild.start().waitFor();
				
				if(result == 0){
					//Kill-server was a success
					File tempFile = new File(System.getProperty("java.io.tmpdir") + File.separator + ADB_FILENAME);
					if(!tempFile.delete()){
						System.out.println("Could not delete "+ADB_FILENAME);
					}else{
						System.out.println("Cleanup "+ADB_FILENAME);
					}
					tempFile = new File(System.getProperty("java.io.tmpdir") + File.separator + ADB_DLL_FILENAME);
					if(!tempFile.delete()){
						System.out.println("Could not delete "+ADB_DLL_FILENAME);
					}else{
						System.out.println("Cleanup "+ADB_DLL_FILENAME);
					}
				}else{
					//Was not
					logger.logMessage("Could not stop ADB");
				}
				
			} catch (InterruptedException e){
				//We were interrupted while waiting for kill-server to finish
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Recreate the adb connection without waiting for any user interaction
	 */
	public static boolean restoreAdb(ClassLoader cl, LogMessager m) throws IOException{
		try{
			pathToAdb = getFile(cl, ADB_FILENAME);
			getFile(cl, ADB_DLL_FILENAME);
		}catch(IOException e){
			m.logMessage("RestoreAdb - Could not copy binaries to temp folder. Can not proceed", LOG_LVL.L1);
			throw e;
		}

		ProcessBuilder pBuild = new ProcessBuilder(pathToAdb,"forward","tcp:"+Constants.Comms.SERVER_LISTEN_PORT,"tcp:"+Constants.Comms.SERVER_LISTEN_PORT);
		pBuild.redirectErrorStream(true);
		Process proc = pBuild.start();
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(proc.getInputStream()));
		StreamReaderThread readerThread = new StreamReaderThread(m);
		readerThread.start(stdInput);

		Timer t = new Timer();
		InterruptTimerTask task = new InterruptTimerTask(m);
		task.addThread(Thread.currentThread());
		task.addThread(readerThread.getThread());

		t.schedule(task, ADB_TIMEOUT_MILLIS);
		try {
			proc.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			t.cancel();
			//Cant just destroy because of forward (cant close cause its synced with read.. which is blocking.. so closing also blocks (WTF SUN))
			//http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4859836
			//So instead just shutdown and kill ADB (finally closing the reader)
			//proc.getInputStream().close();
			Thread.interrupted();
		}

		if(readerThread.noDeviceFound()){
			m.logMessage("RestoreAdb - Device not found. Could not start server.", LOG_LVL.L0);
			killAdb(false, m);
			return false;
		}
		return true;
	}

	@Override
	public void logMessage(String msg){
		logMessage(msg, LOG_LVL.L0);
	}
	
	@Override
	public void logMessage(String message, LOG_LVL lvl) {
		if(Settings.getInstance().getLogLevel().shouldLog(lvl)){
			textArea.append(message+"\n");
			textArea.setCaretPosition(textArea.getDocument().getLength());
		}
		System.out.println(message);
	}
}
