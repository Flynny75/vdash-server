package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JTextField;

import data.Settings;
import data.VDashData;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class GameSettingsDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JTextField ipAddressField;
	private JTextField portField;
	
	private final String IP_HELP_TEXT = "The IP Address must be formatted like '127.0.0.1'. Set this to the game's IP.";
	private final String PORT_HELP_TEXT = "The Port is a number. Set this to the game's port.";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			GameSettingsDialog dialog = new GameSettingsDialog(0, 0, null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public GameSettingsDialog(int x, int y, VDashData.GAME_TYPE game) {
		setBounds(100, 100, 254, 304);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[grow][]", "[][][][][]"));
		{
			JLabel lblGameSettings = new JLabel("GAME SETTINGS");
			lblGameSettings.setFont(new Font("Tahoma", Font.BOLD, 18));
			contentPanel.add(lblGameSettings, "cell 0 0");
		}
		{
			JLabel lblIpAddress = new JLabel("IP Address");
			contentPanel.add(lblIpAddress, "cell 0 1");
		}
		{
			ipAddressField = new JTextField();
			contentPanel.add(ipAddressField, "cell 0 2,growx");
			ipAddressField.setText(Settings.getInstance().getGameSettingsIp());
			ipAddressField.setColumns(10);
		}
		{
			JButton btnNewButton = new JButton();
			btnNewButton.setIcon(UIManager.getIcon("OptionPane.questionIcon"));
			btnNewButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent ev){
					JOptionPane.showMessageDialog(GameSettingsDialog.this, IP_HELP_TEXT);
				}
			});
			contentPanel.add(btnNewButton, "cell 1 2");
		}
		{
			JLabel lblPort = new JLabel("Port");
			contentPanel.add(lblPort, "cell 0 3");
		}
		{
			portField = new JTextField();
			contentPanel.add(portField, "cell 0 4,growx");
			portField.setText(Settings.getInstance().getGameSettingsPort());
			portField.setColumns(10);
		}
		{
			JButton btnNewButton_1 = new JButton();
			btnNewButton_1.setIcon(UIManager.getIcon("OptionPane.questionIcon"));
			btnNewButton_1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent ev){
					JOptionPane.showMessageDialog(GameSettingsDialog.this, PORT_HELP_TEXT);
				}
			});
			contentPanel.add(btnNewButton_1, "cell 1 4");
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Save");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent ev){
						Settings.getInstance().setGameSettingsIp(ipAddressField.getText().toString().trim());
						Settings.getInstance().setGameSettingsPort(portField.getText().toString().trim());
						Settings.getInstance().saveSettings();
						close();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent ev){
						close();
					}
				});
				buttonPane.add(cancelButton);
			}
		}
	}
	
	private void close(){
		setVisible(false);
		dispose();
	}

}
