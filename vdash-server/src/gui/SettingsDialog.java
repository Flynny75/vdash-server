package gui;

import gui.LogMessager;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner.DefaultEditor;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.java.games.input.Component;
import net.java.games.input.Controller;
import net.java.games.input.ControllerEnvironment;
import net.java.games.input.Event;
import net.java.games.input.EventQueue;
import net.miginfocom.swing.MigLayout;
import data.ACDataSource;
import data.DataSource;
import data.DirtDataSource;
import data.Grid2DataSource;
import data.Gtr2DataSource;
import data.InputDeviceManager;
import data.LFSData;
import data.PCarsDataSource;
import data.R3EDataSource;
import data.Race07DataSource;
import data.Settings;
import data.VDashData;
import data.Settings.LOG_LVL;
import data.VDashData.GAME_TYPE;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.LinkedList;

import javax.swing.JSpinner;
import javax.swing.SwingConstants;

public class SettingsDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8684320800008996046L;
	private final JPanel contentPanel = new JPanel();
	private JLabel lblDashLeft,lblDashRight,lblDashTouch;
	private JLabel lblDgPrevious, lblDgNext, lblInterface;
	private JButton gameSettingsButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SettingsDialog dialog = new SettingsDialog(0,0,null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SettingsDialog(int x, int y, final Gui gui) {
		setResizable(false);
		setBounds(x, y, 290, 536);


		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 6, 5, 6));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[][][][100px:n][]", "[][][30px:n][][][][][][][][][][][10px:n][][][][][][]"));
		{
			JLabel lblSettings = new JLabel("SETTINGS");
			lblSettings.setFont(new Font("Tahoma", Font.BOLD, 18));
			contentPanel.add(lblSettings, "cell 0 0 7 1");
		}
		{
			final JCheckBox chckbxAutoStart = new JCheckBox("Automatically start server on launch");
			chckbxAutoStart.setSelected(Settings.getInstance().getAutoConnect());
			chckbxAutoStart.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent arg0) {
					Settings.getInstance().setAutoConnect(chckbxAutoStart.isSelected());
				}
			});
			contentPanel.add(chckbxAutoStart, "cell 0 1 7 1");
		}
		{
			final JCheckBox chckbxKillClean = new JCheckBox("Kill / Clean ADB on exit");
			chckbxKillClean.setSelected(Settings.getInstance().getKillCleanAdbOnExit());
			chckbxKillClean.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent arg0) {
					Settings.getInstance().setKillCleanAdbOnExit(chckbxKillClean.isSelected());
				}
			});
			contentPanel.add(chckbxKillClean, "cell 0 2 7 1");
		}
		{
			JLabel lblWirelessNetwork = new JLabel("Wireless Network");
			lblWirelessNetwork.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblWirelessNetwork, "cell 0 3 2 1");
		}
		{
			JButton btnNetworkInterfaceChange = new JButton("Change");
			contentPanel.add(btnNetworkInterfaceChange, "cell 0 4");
			
			btnNetworkInterfaceChange.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					try {
						Settings.getInstance().setWirelssInterfaceAddress(pickInterface());
						lblInterface.setText("Interface: "+Settings.getInstance().getWirelessInterfaceAddress());
					} catch (SocketException e1) {
						System.out.println("Could not list network interfaces");
						e1.printStackTrace();
					}
				}
			});
		}
		{
			lblInterface = new JLabel("Interface: ");
			contentPanel.add(lblInterface, "cell 0 5 3 1");
			
			try {
				String address = Settings.getInstance().getWirelessInterfaceAddress();
				if(address == null){
					InetAddress localHost = InetAddress.getLocalHost();
					address = localHost.getCanonicalHostName();
				}
				lblInterface.setText("Interface: "+address);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			
		}
		{
			JLabel lblControllerMapping = new JLabel("Controller Mapping");
			lblControllerMapping.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblControllerMapping, "cell 0 6 2 1,aligny bottom");
		}
		{
			JLabel lblNewLabel = new JLabel("Dash");
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblNewLabel, "cell 0 7 2 1");
		}
		{
			JLabel lblNewLabel_1 = new JLabel("Touch");
			lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblNewLabel_1, "cell 2 7,alignx center");
		}
		{
			JButton btnNewButton = new JButton("Left");
			btnNewButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					final Controller c = pickController();
					if(c == null){
						lblDashLeft.setText("N/A");
						InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_LEFT);
						return;
					}

					pickButton(new PickedControllerButtonListener() {
						@Override
						public void picked(Component comp) {
							if(null == comp){
								lblDashLeft.setText("N/A");
								InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_LEFT);
								return;
							}
							lblDashLeft.setText("Set");
							InputDeviceManager.getInstance().addControllerComponentForEvent(c, comp, InputDeviceManager.EVENT_DASH_LEFT);
							Settings s = Settings.getInstance();
							s.setReplaceControllerConfig(c, comp, InputDeviceManager.EVENT_DASH_LEFT);
							s.saveSettings();
						}
					},c);
				}
			});
			contentPanel.add(btnNewButton, "cell 0 8");
		}
		{
			JButton btnNewButton_1 = new JButton("Right");
			btnNewButton_1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					final Controller c = pickController();
					if(c == null){
						lblDashRight.setText("N/A");
						InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_RIGHT);
						return;
					}

					pickButton(new PickedControllerButtonListener() {
						@Override
						public void picked(Component comp) {
							if(null == comp){
								lblDashRight.setText("N/A");
								InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_RIGHT);
								return;
							}
							lblDashRight.setText("Set");
							InputDeviceManager.getInstance().addControllerComponentForEvent(c, comp, InputDeviceManager.EVENT_DASH_RIGHT);
							Settings s = Settings.getInstance();
							s.setReplaceControllerConfig(c, comp, InputDeviceManager.EVENT_DASH_RIGHT);
							s.saveSettings();
						}
					},c);
				}
			});
			contentPanel.add(btnNewButton_1, "cell 1 8");
		}
		{
			JButton btnNewButton_2 = new JButton("touch");
			btnNewButton_2.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					final Controller c = pickController();
					if(c == null){
						lblDashTouch.setText("N/A");
						InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_TOUCH);
						return;
					}

					pickButton(new PickedControllerButtonListener() {
						@Override
						public void picked(Component comp) {
							if(null == comp){
								lblDashTouch.setText("N/A");
								InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_TOUCH);
								return;
							}
							lblDashTouch.setText("Set");
							InputDeviceManager.getInstance().addControllerComponentForEvent(c, comp, InputDeviceManager.EVENT_DASH_TOUCH);
							Settings s = Settings.getInstance();
							s.setReplaceControllerConfig(c, comp, InputDeviceManager.EVENT_DASH_TOUCH);
							s.saveSettings();
						}
					},c);
				}
			});
			contentPanel.add(btnNewButton_2, "cell 2 8,alignx center");
		}
		{
			if(InputDeviceManager.getInstance().checkControllerForEvent(InputDeviceManager.EVENT_DASH_LEFT)){
				lblDashLeft = new JLabel("Set");
			}else{
				lblDashLeft = new JLabel("N/A");
			}
			contentPanel.add(lblDashLeft, "cell 0 9,alignx center");
		}
		{
			if(InputDeviceManager.getInstance().checkControllerForEvent(InputDeviceManager.EVENT_DASH_RIGHT)){
				lblDashRight = new JLabel("Set");
			}else{
				lblDashRight = new JLabel("N/A");
			}
			contentPanel.add(lblDashRight, "cell 1 9,alignx center");
		}
		{
			if(InputDeviceManager.getInstance().checkControllerForEvent(InputDeviceManager.EVENT_DASH_TOUCH)){
				lblDashTouch = new JLabel("Set");
			}else{
				lblDashTouch = new JLabel("N/A");
			}
			contentPanel.add(lblDashTouch, "cell 2 9,alignx center");
		}
		{
			lblDashLeft = new JLabel("");
			contentPanel.add(lblDashLeft, "cell 0 9");
		}
		{
			lblDashRight = new JLabel("");
			contentPanel.add(lblDashRight, "cell 1 9");
		}
		{
			lblDashTouch = new JLabel("");
			contentPanel.add(lblDashTouch, "cell 2 9");
		}
		{
			JLabel lblNextDrawGroup = new JLabel("Draw Groups");
			lblNextDrawGroup.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblNextDrawGroup, "cell 0 10");
		}
		{
			JButton btnPrevious = new JButton("Prev.");
			btnPrevious.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent arg0) {
					final Controller c = pickController();
					if(c == null){
						lblDgPrevious.setText("N/A");
						InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_DRAWGROUP_PREV);
						return;
					}

					pickButton(new PickedControllerButtonListener() {
						@Override
						public void picked(Component comp) {
							if(null == comp){
								lblDgPrevious.setText("N/A");
								InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_DRAWGROUP_PREV);
								return;
							}
							lblDgPrevious.setText("Set");
							InputDeviceManager.getInstance().addControllerComponentForEvent(c, comp, InputDeviceManager.EVENT_DASH_DRAWGROUP_PREV);
							Settings s = Settings.getInstance();
							s.setReplaceControllerConfig(c, comp, InputDeviceManager.EVENT_DASH_DRAWGROUP_PREV);
							s.saveSettings();
						}
					},c);
				}
			});
			contentPanel.add(btnPrevious, "cell 0 11");
		}
		{
			JButton btnNext = new JButton("Next");
			btnNext.addMouseListener(new MouseAdapter(){
				@Override
				public void mouseClicked(MouseEvent e){
					final Controller c = pickController();
					if(c == null){
						lblDgNext.setText("N/A");
						InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_DRAWGROUP_NEXT);
						return;
					}

					pickButton(new PickedControllerButtonListener() {
						@Override
						public void picked(Component comp) {
							if(null == comp){
								lblDgNext.setText("N/A");
								InputDeviceManager.getInstance().removeWatchersForEvent(InputDeviceManager.EVENT_DASH_DRAWGROUP_NEXT);
								return;
							}
							lblDgNext.setText("Set");
							InputDeviceManager.getInstance().addControllerComponentForEvent(c, comp, InputDeviceManager.EVENT_DASH_DRAWGROUP_NEXT);
							Settings s = Settings.getInstance();
							s.setReplaceControllerConfig(c, comp, InputDeviceManager.EVENT_DASH_DRAWGROUP_NEXT);
							s.saveSettings();
						}
					},c);
				}
			});
			contentPanel.add(btnNext, "cell 1 11");
		}
		{
			if(InputDeviceManager.getInstance().checkControllerForEvent(InputDeviceManager.EVENT_DASH_DRAWGROUP_PREV)){
				lblDgPrevious = new JLabel("Set");
			}else{
				lblDgPrevious = new JLabel("N/A");
			}
			contentPanel.add(lblDgPrevious, "cell 0 12");
		}
		{
			if(InputDeviceManager.getInstance().checkControllerForEvent(InputDeviceManager.EVENT_DASH_DRAWGROUP_NEXT)){
				lblDgNext = new JLabel("Set");
			}else{
				lblDgNext = new JLabel("N/A");
			}
			contentPanel.add(lblDgNext, "cell 1 12");
		}
		{
			JLabel lblSimulate = new JLabel("Simulate");
			lblSimulate.setVerticalAlignment(SwingConstants.BOTTOM);
			lblSimulate.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblSimulate, "cell 0 14");
		}
		{
			JButton btnNewButton_3 = new JButton("Capture Data");
			btnNewButton_3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					captureSimData();
				}
			});
			contentPanel.add(btnNewButton_3, "cell 0 15 2 1");
		}
		{
			final JCheckBox chckbxSimulate = new JCheckBox("Simulate");
			chckbxSimulate.setSelected(Settings.getInstance().shouldSimulateData());
			chckbxSimulate.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent arg0) {
					Settings.getInstance().setSimulateData(chckbxSimulate.isSelected());
				}
			});
			{
				JButton btnBreak = new JButton("BREAK");
				btnBreak.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						if(gui.getNetworkingBase() != null){
							gui.getNetworkingBase().sendBrokenPacket();
						}
					}
				});
				contentPanel.add(btnBreak, "cell 4 15");
			}
			contentPanel.add(chckbxSimulate, "cell 0 16");
		}
		{
			JLabel lblDataPeriod = new JLabel("Data Period");
			lblDataPeriod.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblDataPeriod, "cell 2 16,alignx right");
		}
		{
			final JSpinner dataPeriodSpinner = new JSpinner();
			dataPeriodSpinner.setModel(new SpinnerNumberModel(Settings.getInstance().getDataPeriod(), 0, 100, 1));
			dataPeriodSpinner.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent arg0) {
					Settings.getInstance().setDataPeriod(((Double)dataPeriodSpinner.getModel().getValue()).intValue());
				}
			});
			contentPanel.add(dataPeriodSpinner, "cell 2 17,alignx right");
		}
		{
			JLabel lblGame = new JLabel("Game");
			lblGame.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblGame, "cell 0 18");
		}
		{
			JSpinner spinner = new JSpinner();
			final CyclingSpinnerListModel<GAME_TYPE> spinnerModel = new CyclingSpinnerListModel<GAME_TYPE>(GAME_TYPE.values());
			spinner.setModel(spinnerModel);
			((DefaultEditor) spinner.getEditor()).getTextField().setEditable(false);
			spinnerModel.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent arg0) {
					Settings.getInstance().setGameType(spinnerModel.getValue());

					if(gameSettingsButton != null){
						if(Settings.getInstance().getGameType().equals(GAME_TYPE.LFS) ||
							Settings.getInstance().getGameType().equals(GAME_TYPE.DiRT2) ||
							Settings.getInstance().getGameType().equals(GAME_TYPE.DiRT3) ||
							Settings.getInstance().getGameType().equals(GAME_TYPE.GRID2)){
							gameSettingsButton.setEnabled(true);
						}else{
							gameSettingsButton.setEnabled(false);
						}
					}
				}
			});
			spinnerModel.setValue(Settings.getInstance().getGameType());
			contentPanel.add(spinner, "cell 0 19 2 1,growx");
		}
		{
			JLabel lblLogLevel = new JLabel("Log level");
			lblLogLevel.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(lblLogLevel, "cell 2 14,alignx right");
		}
		{
			JSpinner spinner = new JSpinner();
			final CyclingSpinnerListModel<LOG_LVL> spinnerModel = new CyclingSpinnerListModel<LOG_LVL>(LOG_LVL.values());
			spinner.setModel(spinnerModel);
			((DefaultEditor) spinner.getEditor()).getTextField().setEditable(false);
			spinnerModel.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent arg0) {
					Settings.getInstance().setLogLevel(spinnerModel.getValue());
				}
			});
			spinnerModel.setValue(Settings.getInstance().getLogLevel());
			contentPanel.add(spinner, "cell 2 15,alignx right");
		}
		{
			gameSettingsButton = new JButton("Game Settings");
			contentPanel.add(gameSettingsButton, "cell 2 19");
			gameSettingsButton.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent ev){
					GameSettingsDialog dialog = new GameSettingsDialog(getBounds().x, getBounds().y, Settings.getInstance().getGameType());
					dialog.setVisible(true);
				}
			});

			if(Settings.getInstance().getGameType().equals(GAME_TYPE.LFS) ||
				Settings.getInstance().getGameType().equals(GAME_TYPE.DiRT2) ||
				Settings.getInstance().getGameType().equals(GAME_TYPE.DiRT3) ||
				Settings.getInstance().getGameType().equals(GAME_TYPE.GRID2)){
				gameSettingsButton.setEnabled(true);
			}else{
				gameSettingsButton.setEnabled(false);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Save");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent arg0) {
						Settings.getInstance().saveSettings();
						gui.logMessage("Settings updated");
						if(gui != null){
							if(gui.getNetworkingBase() != null){
								gui.getNetworkingBase().resetDataSource();
							}
							gui.setGameImage();
						}
						close();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);

			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						Settings.getInstance().reloadFromFile();
						close();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	
	private String pickInterface() throws SocketException{
		LinkedList<String> names = new LinkedList<String>();
		
		for(NetworkInterface i : Collections.list(NetworkInterface.getNetworkInterfaces())){
			for(InetAddress ia : Collections.list(i.getInetAddresses())){
				names.add(ia.getHostAddress());
			}
		}
		
		String[] n = names.toArray(new String[]{});
		
		return (String)JOptionPane.showInputDialog(this, "Pick Interface", "Interface", JOptionPane.QUESTION_MESSAGE, null, n, 0);
	}

	private Controller pickController(){
		Controller[] ca = ControllerEnvironment.getDefaultEnvironment().getControllers();
		String[] names = new String[ca.length];

		for(int i =0;i<ca.length;i++){
			names[i] = ca[i].getName();
		}

		String controllerName = (String) JOptionPane.showInputDialog(this, "Pick controller", "Controller", JOptionPane.QUESTION_MESSAGE, null, names, 0);

		for(int i = 0; i < names.length; i++){
			if(names[i].equals(controllerName)){
				return ca[i];
			}
		}

		return null;
	}

	private void pickButton(final PickedControllerButtonListener completionListener,final Controller c){
		final ControllerInputDialog dialog = new ControllerInputDialog("Waiting for input");
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		dialog.setVisible(true);
		final FinalHolder holder = new FinalHolder();

		final Thread t = new Thread(){
			@Override
			public void run(){
				EventQueue q = c.getEventQueue();
				Event e = new Event();

				while(!Thread.interrupted()){
					c.poll();
					while(q.getNextEvent(e)){
						Component comp = e.getComponent();
						dialog.changeMessage(comp.getName()+" - "+comp.getPollData());
						holder.held = comp;
					}
					try {
						Thread.yield();
						Thread.sleep(10);
					} catch (InterruptedException e1) {
						break;
					}
				}
			}
		};

		Runnable okRunnable = new Runnable() {
			@Override
			public void run() {
				t.interrupt();
				completionListener.picked((Component) holder.held);
			}
		};

		Runnable cancelRunnable = new Runnable() {
			@Override
			public void run() {
				t.interrupt();
				completionListener.picked(null);
			}
		};

		dialog.setListeners(okRunnable, cancelRunnable);

		t.start();
	}

	private void captureSimData(){
		VDashData data;
		DataSource ds = null;
		LogMessager m = new LogMessager() {
			@Override
			public void logMessage(String message) {}
			@Override
			public void logMessage(String message, LOG_LVL l){}
		};

		switch(Settings.getInstance().getGameType()){
		case AC:
			ds = new ACDataSource(m);
			break;
		case DiRT2:
		case DiRT3:
			ds = new DirtDataSource(m);
			break;
		case ProjectCARS:
			ds = new PCarsDataSource(m);
			break;
		case RACE07:
			ds = new Race07DataSource(m);
			break;
		case R3E:
			ds = new R3EDataSource(m);
			break;
		case GTR2:
			ds = new Gtr2DataSource(m);
			break;
		case LFS:
			ds = new LFSData(m);
			break;
		case GRID2:
			ds = new Grid2DataSource(m);
			break;
		}

		try {
			ds.open();
			data = ds.readData();
			Settings.getInstance().saveSimData(this, data);
		} catch (Exception e) {
			System.out.println("Could not capture sim data");
			JOptionPane.showMessageDialog(this, "Could not obtain data. Is the selected game running?", "Error", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private interface PickedControllerButtonListener{
		public void picked(Component ident);
	}
	private class FinalHolder{
		public Object held;
	}

	private void close(){
		setVisible(false);
		dispose();
	}


	@SuppressWarnings("unchecked")
	public class CyclingSpinnerListModel<T> extends SpinnerListModel {
		/**
		 * 
		 */
		private static final long serialVersionUID = -3978927184838545294L;
		T firstValue, lastValue;
		SpinnerModel linkedModel = null;

		public CyclingSpinnerListModel(T[] values) {
			super(values);
			firstValue = values[0];
			lastValue = values[values.length - 1];
		}

		public void setLinkedModel(SpinnerModel linkedModel) {
			this.linkedModel = linkedModel;
		}

		public Object getNextValue() {
			Object value = super.getNextValue();
			if (value == null) {
				value = firstValue;
				if (linkedModel != null) {
					linkedModel.setValue(linkedModel.getNextValue());
				}
			}
			return value;
		}

		@Override
		public T getValue() {
			return (T)super.getValue();
		}

		public T getPreviousValue() {
			T value = (T) super.getPreviousValue();
			if (value == null) {
				value = lastValue;
				if (linkedModel != null) {
					linkedModel.setValue(linkedModel.getPreviousValue());
				}
			}
			return value;
		}
	}
}
