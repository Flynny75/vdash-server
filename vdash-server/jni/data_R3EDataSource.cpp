#include <windows.h>
#include <tchar.h>
#include <jni.h>

#include "data_R3EDataSource.h"
#include "r3e.h"
#include "DataSourceBase.h"

const r3e_shared* sharedData;

//TODO add player data (orientation, world position, local accel)

void readStateInfo (JNIEnv* env, jobject dataObject){
	int sessionState;
	int sessionPhase = sharedData->session_phase;
	switch(sessionPhase){
	case R3E_SESSION_PHASE_UNAVAILABLE:{
		sessionState = 0;
		break;
	}
	case R3E_SESSION_PHASE_GREEN:{
		sessionState = 2;
		break;
	}
	case R3E_SESSION_PHASE_CHECKERED:{
		sessionState = 3;
		break;
	}
	default:{
		sessionState = 0;
	}
	}

	env->SetIntField(dataObject, raceStateID, sessionState);

	int raceState;
	int session = sharedData->session_type;
	switch(session){
	case R3E_SESSION_UNAVAILABLE:{
		raceState = 0;
		break;
	}
	case R3E_SESSION_PRACTICE:{
		raceState = 1;
		break;
	}
	case R3E_SESSION_QUALIFY:{
		raceState = 3;
		break;
	}
	case R3E_SESSION_RACE:{
		raceState = 5;
		break;
	}
	}

	env->SetIntField(dataObject, sessionStateFieldID, raceState);
}

void readCarInfo (JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject, rpmID, std::max(sharedData->engine_rps, 0.0f) * 10);
	env->SetFloatField(dataObject, maxRpmID, sharedData->max_engine_rps * 10);

	env->SetFloatField(dataObject, fuelPressureKPaID, sharedData->fuel_pressure);
	env->SetFloatField(dataObject, fuelLevelID, sharedData->fuel_left);
	env->SetFloatField(dataObject, fuelCapacityID, sharedData->fuel_capacity);

	env->SetFloatField(dataObject, waterTempCelsID, sharedData->engine_water_temp);

	env->SetFloatField(dataObject, oilPressureKPaID, sharedData->engine_oil_pressure);
	env->SetFloatField(dataObject, oilTempCelsID, sharedData->engine_oil_temp);

	//Bottom the speed at 0 rather than -1
	env->SetFloatField(dataObject, speedID, std::max(sharedData->car_speed, 0.0f));

	int g = sharedData->gear;
	if(g == -2){
		//No gear, set to N
		g = 0;
	}
	env->SetIntField(dataObject, gearID, g);
}

void readEventInfo (JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject, lapsInEventFieldID, sharedData->number_of_laps);

	env->SetIntField(dataObject, numParticipantsID, std::max(sharedData->num_cars, 1));
}

void readParticipantInfo (JNIEnv* env, jobject dataObject){
	participantClass = env->FindClass(PARTICIPANT_CLASS);

	jobject participant = env->NewObject(participantClass, participantConstructor);
	jobjectArray allParticipants= env->NewObjectArray(1, participantClass, NULL);

	participant = env->NewObject(participantClass, participantConstructor);

	env->SetBooleanField(participant, participantActiveID, true);
	env->SetIntField(participant, participantLapsCompletedID, sharedData->completed_laps);
	env->SetIntField(participant, participantCurrentLapID, (sharedData->completed_laps)+1);
	env->SetIntField(participant, participantCurrentSectorID, 0);
	env->SetIntField(participant, participantRacePositionID, sharedData->position);
	
	float worldPos[3];
	r3e_playerdata player = sharedData->player;
	worldPos[0] = player.position.x;
	worldPos[1] = player.position.y;
	worldPos[2] = player.position.z;
	jfloatArray worldPosData = env->NewFloatArray(3);
	env->SetFloatArrayRegion(worldPosData, 0, 3, worldPos);
	env->SetObjectField(dataObject, participantWorldPositionID, worldPosData);

	env->SetObjectArrayElement(allParticipants, 0, participant);
	env->SetObjectField(dataObject, participantsID, allParticipants);
	
	float orientation[3];
	orientation[0] = player.orientation.x;
	orientation[1] = player.orientation.y;
	orientation[2] = player.orientation.z;
	jfloatArray orientationData = env->NewFloatArray(3);
	env->SetFloatArrayRegion(orientationData, 0, 3, orientation);
	env->SetObjectField(dataObject, orientationID, orientationData);
	
	float localAccel[3];
	localAccel[0] = player.local_acceleration.x;
	localAccel[1] = player.local_acceleration.y;
	localAccel[2] = player.local_acceleration.z;
	jfloatArray localAccelData = env->NewFloatArray(3);
	env->SetFloatArrayRegion(localAccelData, 0, 3, localAccel);
	env->SetObjectField(dataObject, localAccelerationID, localAccelData);
	
	float worldAccel[3];
	worldAccel[0] = player.acceleration.x;
	worldAccel[1] = player.acceleration.y;
	worldAccel[2] = player.acceleration.z;
	jfloatArray worldAccelData = env->NewFloatArray(3);
	env->SetFloatArrayRegion(worldAccelData, 0, 3, worldAccel);
	env->SetObjectField(dataObject, worldAccelerationID, worldAccelData);
	
	/*float angularAccel[3];
	angularAccel[0] = player.angular_acceleration.x;
	angularAccel[1] = player.angular_acceleration.y;
	angularAccel[2] = player.angular_acceleration.z;
	jfloatArray angularAccelData = env->NewFloatArray(3);
	env->SetFloatArrayRegion(angularAccelData, 0, 3, angularAccel);
	env->SetObjectField(dataObject, angularAccelerationID, angularAccelData);*/
	
	float worldVel[3];
	worldVel[0] = player.velocity.x;
	worldVel[1] = player.velocity.y;
	worldVel[2] = player.velocity.z;
	jfloatArray worldVelData = env->NewFloatArray(3);
	env->SetFloatArrayRegion(worldVelData, 0, 3, worldVel);
	env->SetObjectField(dataObject, worldVelocityID, worldVelData);
}

void readTimeInfo (JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject, bestLapTimeID, sharedData->lap_time_best_leader);
	env->SetFloatField(dataObject, lastLapTimeID, sharedData->lap_time_previous_self);
	env->SetFloatField(dataObject, currentTimeID, sharedData->lap_time_current_self);

	env->SetFloatField(dataObject, splitTimeAheadID, sharedData->time_delta_front);
	env->SetFloatField(dataObject, splitTimeBehindID, sharedData->time_delta_behind);

	env->SetFloatField(dataObject, eventTimeRemainingID, sharedData->session_time_remaining);
}

void readControlInfo (JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject, brakeID, sharedData->brake_pedal);
	env->SetFloatField(dataObject, throttleID, sharedData->throttle_pedal);
	env->SetFloatField(dataObject, clutchID, sharedData->clutch_pedal);
}

void readDamageInfo (JNIEnv* env, jobject dataObject){
	r3e_car_damage damage = sharedData->car_damage;

	env->SetFloatField(dataObject, engineConditionID, damage.engine * 100);
	env->SetFloatField(dataObject, aeroConditionID, damage.aerodynamics * 100);
	env->SetFloatField(dataObject, transmissionConditionID, damage.transmission * 100);
}

void readWheelInfo (JNIEnv* env, jobject dataObject){
	r3e_car_damage damage = sharedData->car_damage;

	float tyreWear[4];
	tyreWear[0] = damage.tire_front_left * 100;
	tyreWear[1] = damage.tire_front_right * 100;
	tyreWear[2] = damage.tire_rear_left * 100;
	tyreWear[3] = damage.tire_rear_right * 100;

	jfloatArray tyreWearData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(tyreWearData, 0, 4, tyreWear);
	env->SetObjectField(dataObject, tyreConditionID, tyreWearData);

	r3e_tire_pressure pressures = sharedData->tire_pressure;

	float p[4];
	p[0] = pressures.front_left;
	p[1] = pressures.front_right;
	p[2] = pressures.rear_left;
	p[3] = pressures.rear_right;

	jfloatArray tyrePressureData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(tyrePressureData, 0, 4, p);
	env->SetObjectField(dataObject, tyrePressuresID, tyrePressureData);

	r3e_brake_temps brakeTemps = sharedData->brake_temps;

	float b[4];
	b[0] = brakeTemps.front_left;
	b[1] = brakeTemps.front_right;
	b[2] = brakeTemps.rear_left;
	b[3] = brakeTemps.rear_right;

	jfloatArray brakeTempData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(brakeTempData, 0, 4, b);
	env->SetObjectField(dataObject, brakeTempCelciusID, brakeTempData);

	float inner[4];
	inner[0] = convertCelsToKelvin(sharedData->tire_temps.frontleft_right);
	inner[1] = convertCelsToKelvin(sharedData->tire_temps.frontright_left);
	inner[2] = convertCelsToKelvin(sharedData->tire_temps.rearleft_right);
	inner[3] = convertCelsToKelvin(sharedData->tire_temps.rearright_left);

	float mid[4];
	mid[0] = convertCelsToKelvin(sharedData->tire_temps.frontleft_center);
	mid[1] = convertCelsToKelvin(sharedData->tire_temps.frontright_center);
	mid[2] = convertCelsToKelvin(sharedData->tire_temps.rearleft_center);
	mid[3] = convertCelsToKelvin(sharedData->tire_temps.rearright_center);

	float outer[4];
	outer[0] = convertCelsToKelvin(sharedData->tire_temps.frontleft_left);
	outer[1] = convertCelsToKelvin(sharedData->tire_temps.frontright_right);
	outer[2] = convertCelsToKelvin(sharedData->tire_temps.rearleft_left);
	outer[3] = convertCelsToKelvin(sharedData->tire_temps.rearright_right);

	jfloatArray innerData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(innerData, 0, 4, inner);
	env->SetObjectField(dataObject, tyreInnerTempsID, innerData);

	jfloatArray midData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(midData, 0, 4, mid);
	env->SetObjectField(dataObject, tyreMidTempsID, midData);

	jfloatArray outerData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(outerData, 0, 4, outer);
	env->SetObjectField(dataObject, tyreOuterTempsID, outerData);
}

void readPlayerInfo (JNIEnv* env, jobject dataObject){
	
}

JNIEXPORT jlong JNICALL Java_data_R3EDataSource_openFileMapping (JNIEnv * pEnv, jclass, jstring name) {

	HANDLE hMapFile = NULL;

	LPCSTR lpName = pEnv->GetStringUTFChars(name, NULL);
	hMapFile = OpenFileMapping( PAGE_READONLY, FALSE, lpName );
	if(hMapFile == NULL){
		return 0;
	}else{
		pEnv->ReleaseStringUTFChars(name, lpName);
		return reinterpret_cast<jlong>(hMapFile);
	}
}

JNIEXPORT jint JNICALL Java_data_R3EDataSource_mapViewOfFile (JNIEnv *, jclass, jlong hMapFile) {

	sharedData = (r3e_shared*)MapViewOfFile(reinterpret_cast<HANDLE>(hMapFile), PAGE_READONLY, 0, 0, sizeof(r3e_shared) );
	if (sharedData == NULL)
	{
		printf( "Could not open R3E memory file.\n", GetLastError() );

		CloseHandle( reinterpret_cast<HANDLE>(hMapFile));
		return 0;
	}
	return 1;
}

JNIEXPORT jobject JNICALL Java_data_R3EDataSource_getData (JNIEnv* env, jclass){
	dataClass = env->FindClass(DATA_CLASS);
	jobject dataObject = env->NewObject(dataClass, constructor);

	readStateInfo (env, dataObject);
	readCarInfo (env, dataObject);
	readEventInfo (env, dataObject);
	readParticipantInfo (env, dataObject);
	readTimeInfo (env, dataObject);
	readControlInfo (env, dataObject);
	readDamageInfo (env, dataObject);
	readWheelInfo (env, dataObject);
	readPlayerInfo (env, dataObject);

	return dataObject;
}

JNIEXPORT void JNICALL Java_data_R3EDataSource_findFieldIDs (JNIEnv* env, jclass){
	findFieldIDs(env);
}

JNIEXPORT jboolean JNICALL Java_data_R3EDataSource_closeHandle (JNIEnv *, jclass, jlong hObject) {
	return CloseHandle(reinterpret_cast<HANDLE>(hObject));
}
