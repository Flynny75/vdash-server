// dll implementation file MemMapLib.cpp
//
////////////////////////////////////////////////////////////////////////////////////
#include <iterator>
#include "data_PCarsDataSource.h"
#include "DataSourceBase.h"

#include "ProjectCars.h"


const SharedMemory* sharedData;

void ErrorHandler(LPCTSTR pszErrorMessage) {
	MessageBox(NULL, pszErrorMessage, _T("Error"), MB_OK | MB_ICONERROR);
}

void getVersion(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject,apiVersionNumberID,sharedData->mVersion);
	env->SetIntField(dataObject,buildNumberID,sharedData->mBuildVersionNumber);
}

void getStates(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject,gameStateFieldID,sharedData->mGameState);
	env->SetIntField(dataObject,sessionStateFieldID,sharedData->mSessionState);
	env->SetIntField(dataObject,raceStateID,sharedData->mRaceState);
}

void getParticipantInfo(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject,viewingParticipantID,sharedData->mViewedParticipantIndex);
	env->SetIntField(dataObject, numParticipantsID, sharedData->mNumParticipants);
}

void getUnfilteredInput(JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject,unfilteredThrottleID,sharedData->mUnfilteredThrottle);
	env->SetFloatField(dataObject,unfilteredBrakeID,sharedData->mUnfilteredBrake);
	env->SetFloatField(dataObject,unfilteredSteeringID,sharedData->mUnfilteredSteering);
	env->SetFloatField(dataObject,unfilteredClutchID,sharedData->mUnfilteredClutch);
}

void getVehicleInformation(JNIEnv* env, jobject dataObject){
	env->SetObjectField(dataObject, carNameID, makeCharArray(env, sharedData->mCarName));
	env->SetObjectField(dataObject, carClassNameID, makeCharArray(env, sharedData->mCarClassName));
}

void getEventInformation(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject,lapsInEventFieldID,sharedData->mLapsInEvent);
	env->SetObjectField(dataObject,trackLocationID,makeCharArray(env, sharedData->mTrackLocation));
	env->SetObjectField(dataObject,trackVariationID,makeCharArray(env, sharedData->mTrackVariation));
	env->SetFloatField(dataObject, trackLengthID, sharedData->mTrackLength);
}

void getTimes(JNIEnv* env, jobject dataObject){
	env->SetBooleanField(dataObject,lapInvalidatedID,sharedData->mLapInvalidated);
	env->SetFloatField(dataObject,bestLapTimeID,sharedData->mBestLapTime);
	env->SetFloatField(dataObject,lastLapTimeID,sharedData->mLastLapTime);
	env->SetFloatField(dataObject,currentTimeID,sharedData->mCurrentTime);
	env->SetFloatField(dataObject,splitTimeAheadID,sharedData->mSplitTimeAhead);
	env->SetFloatField(dataObject,splitTimeBehindID,sharedData->mSplitTimeBehind);
	env->SetFloatField(dataObject,splitTimeID,sharedData->mSplitTime);
	env->SetFloatField(dataObject,eventTimeRemainingID,sharedData->mEventTimeRemaining);
	env->SetFloatField(dataObject, personalFastestLapTimeID, sharedData->mPersonalFastestLapTime);
	env->SetFloatField(dataObject, worldFastestLapTimeID, sharedData->mWorldFastestLapTime);
	env->SetFloatField(dataObject,currentS1TimeID,sharedData->mCurrentSector1Time);
	env->SetFloatField(dataObject,currentS2TimeID,sharedData->mCurrentSector2Time);
	env->SetFloatField(dataObject,currentS3TimeID,sharedData->mCurrentSector3Time);
	env->SetFloatField(dataObject,fastestS1TimeID,sharedData->mFastestSector1Time);
	env->SetFloatField(dataObject,fastestS2TimeID,sharedData->mFastestSector2Time);
	env->SetFloatField(dataObject,fastestS3TimeID,sharedData->mFastestSector3Time);
	env->SetFloatField(dataObject,personalFastestS1TimeID,sharedData->mPersonalFastestSector1Time);
	env->SetFloatField(dataObject,personalFastestS2TimeID,sharedData->mPersonalFastestSector2Time);
	env->SetFloatField(dataObject,personalFastestS3TimeID,sharedData->mPersonalFastestSector3Time);
	env->SetFloatField(dataObject,worldFastestS1TimeID,sharedData->mWorldFastestSector1Time);
	env->SetFloatField(dataObject,worldFastestS2TimeID,sharedData->mWorldFastestSector2Time);
	env->SetFloatField(dataObject,worldFastestS3TimeID,sharedData->mWorldFastestSector3Time);
}

void getFlag(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject, highestFlagColourID, sharedData->mHighestFlagColour);
	env->SetIntField(dataObject, highestFlagReasonID, sharedData->mHighestFlagReason);
}

void getPit(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject, pitModeID, sharedData->mPitMode);
	env->SetIntField(dataObject, pitScheduleID, sharedData->mPitSchedule);
}

void getCarState(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject, carFlagID, sharedData->mCarFlags);
	env->SetFloatField(dataObject, oilTempCelsID, sharedData->mOilTempCelsius);
	env->SetFloatField(dataObject,oilPressureKPaID,sharedData->mOilPressureKPa);
	env->SetFloatField(dataObject,waterTempCelsID,sharedData->mWaterTempCelsius);
	env->SetFloatField(dataObject, waterPressureKPaID, sharedData->mWaterPressureKPa);
	env->SetFloatField(dataObject, fuelPressureKPaID, sharedData->mFuelPressureKPa);
	env->SetFloatField(dataObject,fuelLevelID,(sharedData->mFuelLevel * sharedData->mFuelCapacity));
	env->SetFloatField(dataObject,fuelCapacityID, sharedData->mFuelCapacity);
	env->SetFloatField(dataObject, speedID, sharedData->mSpeed);
	env->SetFloatField(dataObject,rpmID,sharedData->mRpm);
	env->SetFloatField(dataObject,maxRpmID,sharedData->mMaxRPM);
	env->SetFloatField(dataObject,brakeID,sharedData->mBrake);
	env->SetFloatField(dataObject,throttleID,sharedData->mThrottle);
	env->SetFloatField(dataObject,clutchID,sharedData->mClutch);
	env->SetFloatField(dataObject,steeringID,sharedData->mSteering);
	env->SetIntField(dataObject,gearID,sharedData->mGear);
	env->SetIntField(dataObject,numGearsID,sharedData->mNumGears);
	env->SetFloatField(dataObject, odometerKMID, sharedData->mOdometerKM);
	env->SetBooleanField(dataObject, antiLockActiveID, sharedData->mAntiLockActive);
	env->SetIntField(dataObject, lastOpponentCollisionIndexID, sharedData->mLastOpponentCollisionIndex);
	env->SetIntField(dataObject, lastOpponentCollisionMagnitudeID, sharedData->mLastOpponentCollisionMagnitude);
	env->SetBooleanField(dataObject, boostActiveID, sharedData->mBoostActive);
	env->SetFloatField(dataObject, boostAmountID, sharedData->mBoostAmount);
}

void getMotionAndDeviceInfo(JNIEnv* env, jobject dataObject){
	jfloatArray orientationData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(orientationData,0,VEC_MAX,sharedData->mOrientation);
	env->SetObjectField(dataObject,orientationID,orientationData);

	jfloatArray localVelocityData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(localVelocityData,0,VEC_MAX,sharedData->mLocalVelocity);
	env->SetObjectField(dataObject,localVelocityID,localVelocityData);

	jfloatArray worldVelocityData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(worldVelocityData,0,VEC_MAX,sharedData->mWorldVelocity);
	env->SetObjectField(dataObject,worldVelocityID,worldVelocityData);

	jfloatArray angularVelocityData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(angularVelocityData,0,VEC_MAX,sharedData->mAngularVelocity);
	env->SetObjectField(dataObject,angularVelocityID,angularVelocityData);

	jfloatArray localAccelerationData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(localAccelerationData,0,VEC_MAX,sharedData->mLocalAcceleration);
	env->SetObjectField(dataObject,localAccelerationID,localAccelerationData);

	jfloatArray worldAccelerationData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(worldAccelerationData,0,VEC_MAX,sharedData->mWorldAcceleration);
	env->SetObjectField(dataObject,worldAccelerationID,worldAccelerationData);

	jfloatArray extentsCentreData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(extentsCentreData,0,VEC_MAX,sharedData->mExtentsCentre);
	env->SetObjectField(dataObject,extentsCentreID,extentsCentreData);
}

void getWheelsTyresData(JNIEnv* env, jobject dataObject){
	jint tyreFlags[TYRE_MAX];
	jintArray tyreFlagsData = env->NewIntArray(TYRE_MAX);
	for(int i = 0; i < TYRE_MAX; i++){
		tyreFlags[i] = sharedData->mTyreFlags[i];
	}
	env->SetIntArrayRegion(tyreFlagsData,0,TYRE_MAX,tyreFlags);
	env->SetObjectField(dataObject,tyreFlagsID,tyreFlagsData);

	jint tyreTerrainFlags[TYRE_MAX];
	jintArray terrainData = env->NewIntArray(TYRE_MAX);
	for(int i = 0; i < TYRE_MAX; i++){
		tyreTerrainFlags[i] = sharedData->mTerrain[i];
	}
	env->SetIntArrayRegion(terrainData,0,TYRE_MAX,tyreTerrainFlags);
	env->SetObjectField(dataObject,terrainID,terrainData);

	jfloatArray tyreYData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreYData,0,TYRE_MAX,sharedData->mTyreY);
	env->SetObjectField(dataObject,tyreYID,tyreYData);

	jfloatArray tyreRPSData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreRPSData,0,TYRE_MAX,sharedData->mTyreRPS);
	env->SetObjectField(dataObject,tyreRPSID,tyreRPSData);

	jfloatArray tyreSlipSpeedData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreSlipSpeedData,0,TYRE_MAX,sharedData->mTyreSlipSpeed);
	env->SetObjectField(dataObject,tyreSlipSpeedID,tyreSlipSpeedData);

	float tyreTempsK[4];
	for(int i = 0; i < 4; i++){
		tyreTempsK[i] = convertCelsToKelvin(sharedData->mTyreTemp[i]);
	}
	jfloatArray tyreTempData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreTempData,0,TYRE_MAX, tyreTempsK);
	env->SetObjectField(dataObject,tyreTempID,tyreTempData);

	jfloatArray tyreGripData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreGripData,0,TYRE_MAX,sharedData->mTyreGrip);
	env->SetObjectField(dataObject,tyreGripID,tyreGripData);

	jfloatArray tyreHeightAboveGroundData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreHeightAboveGroundData,0,TYRE_MAX,sharedData->mTyreHeightAboveGround);
	env->SetObjectField(dataObject,tyreHeightAboveGroundID,tyreHeightAboveGroundData);

	jfloatArray tyreLateralStiffnessData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreLateralStiffnessData,0,TYRE_MAX,sharedData->mTyreLateralStiffness);
	env->SetObjectField(dataObject,tyreLateralStiffnessID,tyreLateralStiffnessData);

	float tyreCond[4];
	for(int i = 0; i < 4; i++){
		tyreCond[i] = (1 - sharedData->mTyreWear[i]) * 100;
	}
	jfloatArray tyreWearData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreWearData,0,TYRE_MAX, tyreCond);
	env->SetObjectField(dataObject,tyreConditionID,tyreWearData);

	float brakeCond[4];
	for(int i = 0; i < 4; i++){
		brakeCond[i] = (1 - sharedData->mBrakeDamage[i]) * 100;
	}
	jfloatArray brakeDamageData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(brakeDamageData,0,TYRE_MAX, brakeCond);
	env->SetObjectField(dataObject,brakeConditionID,brakeDamageData);

	//Convert suspension damage to condition
	float susCond[4];
	for( int i = 0; i < 4; i++){
		susCond[i] = (1 - sharedData->mSuspensionDamage[i]) * 100;
	}
	jfloatArray suspensionDamageData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(suspensionDamageData,0,TYRE_MAX, susCond);
	env->SetObjectField(dataObject,suspensionConditionID,suspensionDamageData);

	jfloatArray brakeTempCelciusData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(brakeTempCelciusData,0,TYRE_MAX,sharedData->mBrakeTempCelsius);
	env->SetObjectField(dataObject,brakeTempCelciusID,brakeTempCelciusData);

	jfloatArray tyreTreadData = env->NewFloatArray(TYRE_MAX);
	jfloatArray tyreLayerData = env->NewFloatArray(TYRE_MAX);
	jfloatArray tyreCarcassData = env->NewFloatArray(TYRE_MAX);
	jfloatArray tyreRimData = env->NewFloatArray(TYRE_MAX);
	jfloatArray tyreInternalAirData = env->NewFloatArray(TYRE_MAX);

	env->SetFloatArrayRegion(tyreTreadData, 0, TYRE_MAX, sharedData->mTyreTreadTemp);
	env->SetObjectField(dataObject, tyreTreadTempID, tyreTreadData);

	env->SetFloatArrayRegion(tyreLayerData, 0, TYRE_MAX, sharedData->mTyreLayerTemp);
	env->SetObjectField(dataObject, tyreLayerTempID, tyreLayerData);

	env->SetFloatArrayRegion(tyreCarcassData, 0, TYRE_MAX, sharedData->mTyreCarcassTemp);
	env->SetObjectField(dataObject, tyreCarcassTempID, tyreCarcassData);

	env->SetFloatArrayRegion(tyreRimData, 0, TYRE_MAX, sharedData->mTyreRimTemp);
	env->SetObjectField(dataObject, tyreRimTempID, tyreRimData);

	env->SetFloatArrayRegion(tyreInternalAirData, 0, TYRE_MAX, sharedData->mTyreInternalAirTemp);
	env->SetObjectField(dataObject, tyreInternalAirTempID, tyreInternalAirData);
}

void getCarDamage(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject, crashStateID, sharedData->mCrashState);
	env->SetFloatField(dataObject,aeroConditionID, (1 - sharedData->mAeroDamage) * 100);
	env->SetFloatField(dataObject, engineConditionID, (1 - sharedData->mEngineDamage) * 100);
}

void getWeather(JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject,ambientTempID,sharedData->mAmbientTemperature);
	env->SetFloatField(dataObject,trackTempID,sharedData->mTrackTemperature);
	env->SetFloatField(dataObject,rainDensityID, sharedData->mRainDensity);
	env->SetFloatField(dataObject,windSpeedID, sharedData->mWindSpeed);
	env->SetFloatField(dataObject,windDirectionXID, sharedData->mWindDirectionX);
	env->SetFloatField(dataObject,windDirectionYID, sharedData->mWindDirectionY);
	env->SetFloatField(dataObject,cloudBrightnessID, sharedData->mCloudBrightness);
}

void getParticipants(JNIEnv* env, jobject dataObject){
	participantClass = env->FindClass(PARTICIPANT_CLASS);

	if(sharedData->mNumParticipants > 0){
		jobject participant = env->NewObject(participantClass, participantConstructor);
		jobjectArray allParticipants= env->NewObjectArray(sharedData->mNumParticipants, participantClass, NULL);

		for(int i = 0; i < sharedData->mNumParticipants; i++){
			participant = env->NewObject(participantClass, participantConstructor);

			ParticipantInfo p = sharedData->mParticipantInfo[i];

			env->SetBooleanField(participant, participantActiveID, p.mIsActive);
			env->SetObjectField(participant,participantNameID, makeCharArray(env, p.mName));

			jfloatArray participantWorldPosition = env->NewFloatArray(VEC_MAX);
			env->SetFloatArrayRegion(participantWorldPosition,0,VEC_MAX,p.mWorldPosition);
			env->SetObjectField(participant,participantWorldPositionID,participantWorldPosition);

			env->SetFloatField(participant, participantCurrentLapDistanceID, p.mCurrentLapDistance);
			env->SetIntField(participant, participantRacePositionID, p.mRacePosition);
			env->SetIntField(participant, participantLapsCompletedID, p.mLapsCompleted);
			env->SetIntField(participant, participantCurrentLapID, p.mCurrentLap);
			env->SetIntField(participant, participantCurrentSectorID, p.mCurrentSector);

			env->SetObjectArrayElement(allParticipants, i, participant);
		}

		env->SetObjectField(dataObject, participantsID, allParticipants);
	}
}

JNIEXPORT jlong JNICALL Java_data_PCarsDataSource_openFileMapping
(JNIEnv * pEnv, jclass, jstring name) {

	HANDLE hMapFile = NULL;

	LPCSTR lpName = pEnv->GetStringUTFChars(name, NULL);
	hMapFile = OpenFileMapping( PAGE_READONLY, FALSE, lpName );
	if(hMapFile == NULL){
		return 0;
	}else{
		pEnv->ReleaseStringUTFChars(name, lpName);
		return reinterpret_cast<jlong>(hMapFile);
	}
}

JNIEXPORT jint JNICALL Java_data_PCarsDataSource_mapViewOfFile
(JNIEnv *, jclass, jlong hMapFile) {

	sharedData = (SharedMemory*)MapViewOfFile(reinterpret_cast<HANDLE>(hMapFile), PAGE_READONLY, 0, 0, sizeof(SharedMemory) );
	if (sharedData == NULL)
	{
		printf( "Could not open PCars memory file.\n", GetLastError() );

		CloseHandle( reinterpret_cast<HANDLE>(hMapFile));
		return 0;
	}
	return 1;
}

JNIEXPORT jobject JNICALL Java_data_PCarsDataSource_getData(JNIEnv* env, jclass){
	dataClass = env->FindClass(DATA_CLASS);
	jobject dataObject = env->NewObject(dataClass, constructor);

	getVersion(env, dataObject);
	getStates(env, dataObject);
	getParticipantInfo(env, dataObject);
	getUnfilteredInput(env, dataObject);
	getVehicleInformation(env, dataObject);
	getEventInformation(env, dataObject);
	getTimes(env, dataObject);
	getFlag(env, dataObject);
	getPit(env, dataObject);
	getCarState(env, dataObject);
	getMotionAndDeviceInfo(env, dataObject);
	getWheelsTyresData(env, dataObject);
	getCarDamage(env, dataObject);
	getWeather(env, dataObject);
	getParticipants(env, dataObject);

	return dataObject;
}

JNIEXPORT void JNICALL Java_data_PCarsDataSource_findFieldIDs(JNIEnv* env, jclass){
	findFieldIDs(env);
}

JNIEXPORT jboolean JNICALL Java_data_PCarsDataSource_closeHandle
(JNIEnv *, jclass, jlong hObject) {

	return CloseHandle(reinterpret_cast<HANDLE>(hObject));
}

/*
 * Shutdown hooks
 */
static JavaVM *jvm = NULL;

BOOL WINAPI HandlerRoutine(DWORD dwCtrlType) {
	if (dwCtrlType == CTRL_CLOSE_EVENT) {
		JNIEnv *env;
		jint res =  jvm->AttachCurrentThread((void **)(&env), &env);
		jclass cls = env->FindClass("Main");
		jmethodID mid = env->GetStaticMethodID(cls, "shutdown", "()V");
		env->CallStaticVoidMethod(cls, mid);
		jvm->DetachCurrentThread();
		return TRUE;
	}
	return FALSE;
}

JNIEXPORT void JNICALL Java_PCarsDataSource_replaceConsoleHandler(JNIEnv * env, jclass) {
	env->GetJavaVM(&jvm);
	SetConsoleCtrlHandler(&HandlerRoutine, TRUE);
}
