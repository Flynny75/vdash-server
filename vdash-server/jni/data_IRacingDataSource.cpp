#include <windows.h>
#include <tchar.h>
#include <jni.h>
#include <limits.h>

#include "data_IRacingDataSource.h"

#include "iracing_sdk/irsdk_defines.h"

//VERSIONS
static jfieldID versionNumberID,
				buildNumberID;

//GAME STATES
static jfieldID gameStateFieldID,
				sessionStateFieldID,
				raceStateID;

//PARTICIPANTS
static jfieldID viewingParticipantID,
				numParticipantsID;

//UNFILTERED INPUT FIELDS
static jfieldID unfilteredThrottleID,
				unfilteredBrakeID,
				unfilteredSteeringID,
				unfilteredClutchID;

//VEHICLE INFORMATIONS
static jfieldID carNameID,
				carClassNameID;

//EVENT INFORMATIONS
static jfieldID lapsInEventFieldID,
				trackLocationID,
				trackVariationID,
				trackLengthID;

//TIME FIELD IDs
static jfieldID lapInvalidatedID,
				bestLapTimeID,
				lastLapTimeID,
				currentTimeID,
				splitTimeAheadID,
				splitTimeBehindID,
				splitTimeID,
				eventTimeRemainingID,
				personalFastestLapTimeID,
				worldFastestLapTimeID,
				currentS1TimeID,
				currentS2TimeID,
				currentS3TimeID,
				fastestS1TimeID,
				fastestS2TimeID,
				fastestS3TimeID,
				personalFastestS1TimeID,
				personalFastestS2TimeID,
				personalFastestS3TimeID,
				worldFastestS1TimeID,
				worldFastestS2TimeID,
				worldFastestS3TimeID;

//FLAG FIELDS
static jfieldID highestFlagColourID,
				highestFlagReasonID;

//PIT
static jfieldID pitModeID,
				pitScheduleID;

//CAR STATE	FIELD IDs
static jfieldID carFlagID,
				oilTempCelsID,
				oilPressureKPaID,
				waterTempCelsID,
				waterPressureKPaID,
				fuelPressureKPaID,
				fuelLevelID,
				fuelCapacityID,
				speedID,
				rpmID,
				maxRpmID,
				brakeID,
				throttleID,
				clutchID,
				steeringID,
				gearID,
				numGearsID,
				odometerKMID,
				antiLockActiveID,
				lastOpponentCollisionIndexID,
				lastOpponentCollisionMagnitudeID,
				boostActiveID,
				boostAmountID,
				maxTorqueID,
				maxPowerID;

//MOTION STATE FIELD IDs
static jfieldID orientationID,
				localVelocityID,
				worldVelocityID,
				angularVelocityID,
				localAccelerationID,
				worldAccelerationID,
				extentsCentreID;

//WHEELS TYRES FIELD IDs
static jfieldID tyreFlagsID,
				terrainID,
				tyreYID,
				tyreRPSID,
				tyreSlipSpeedID,
				tyreTempID,
				tyreGripID,
				tyreHeightAboveGroundID,
				tyreLateralStiffnessID,
				tyreWearID,
				brakeDamageID,
				suspensionDamageID,
				brakeTempCelciusID,
				tyreTreadTempID,
				tyreLayerTempID,
				tyreCarcassTempID,
				tyreRimTempID,
				tyreInternalAirTempID;

//CAR DAMAGE
static jfieldID crashStateID,
				aeroDamageID,
				engineDamageID;

//WEATHER
static jfieldID ambientTempID,
				trackTempID,
				rainDensityID,
				windSpeedID,
				windDirectionXID,
				windDirectionYID,
				cloudBrightnessID;

static jint tempTyreFlags[4];

static jclass dataClass;
static jclass participantClass;

static jfieldID participantActiveID,
				participantNameID,
				particiantWorldPositionID,
				participantCurrentLapDistanceID,
				participantRacePositionID,
				participantLapsCompletedID,
				participantCurrentLapID,
				participantCurrentSectorID;

static jfieldID participantsID;

static jmethodID constructor, participantConstructor;

static HANDLE hMemMapFile = NULL;
static int lastTickCount = INT_MAX;
static const char *pSharedMem = NULL;
static const irsdk_header *pHeader = NULL;
static HANDLE hDataValidEvent = NULL;
static bool isInitialized = false;



void ErrorHandler(LPCTSTR pszErrorMessage) {
	MessageBox(NULL, pszErrorMessage, _T("Error"), MB_OK | MB_ICONERROR);
}

void getVersionIDs(JNIEnv* env, jclass dataClass){
	versionNumberID = env->GetFieldID(dataClass,"mVersionNumber","I");
	buildNumberID = env->GetFieldID(dataClass,"mBuildVersionNumber","I");
}
//void getVersion(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject,versionNumberID,sharedData->mVersion);
//	env->SetIntField(dataObject,buildNumberID,sharedData->mBuildVersionNumber);
//}

void getStatesIDs(JNIEnv* env, jclass dataClass){
	gameStateFieldID = env->GetFieldID(dataClass,"mGameState","I");
	sessionStateFieldID = env->GetFieldID(dataClass,"mSessionState","I");
	raceStateID = env->GetFieldID(dataClass,"mRaceState","I");
}
//void getStates(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject,gameStateFieldID,sharedData->mGameState);
//	env->SetIntField(dataObject,sessionStateFieldID,sharedData->mSessionState);
//	env->SetIntField(dataObject,raceStateID,sharedData->mRaceState);
//}

void getParticipantInfoIDs(JNIEnv* env, jclass dataClass){
	viewingParticipantID = env->GetFieldID(dataClass,"mViewedParticipantIndex","I");
	numParticipantsID = env->GetFieldID(dataClass,"mNumParticipants","I");
}
//void getParticipantInfo(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject,viewingParticipantID,sharedData->mViewedParticipantIndex);
//	env->SetIntField(dataObject, numParticipantsID, sharedData->mNumParticipants);
//}

void getUnfilteredInputIDs(JNIEnv* env, jclass dataClass){
	unfilteredThrottleID = env->GetFieldID(dataClass,"mUnfilteredThrottle","F");
	unfilteredBrakeID = env->GetFieldID(dataClass,"mUnfilteredBrake","F");
	unfilteredSteeringID = env->GetFieldID(dataClass,"mUnfilteredSteering","F");
	unfilteredClutchID = env->GetFieldID(dataClass,"mUnfilteredClutch","F");
}
//void getUnfilteredInput(JNIEnv* env, jobject dataObject){
//	env->SetFloatField(dataObject,unfilteredThrottleID,sharedData->mUnfilteredThrottle);
//	env->SetFloatField(dataObject,unfilteredBrakeID,sharedData->mUnfilteredBrake);
//	env->SetFloatField(dataObject,unfilteredSteeringID,sharedData->mUnfilteredSteering);
//	env->SetFloatField(dataObject,unfilteredClutchID,sharedData->mUnfilteredClutch);
//}

void getVehicleInformationIDs(JNIEnv* env, jclass dataClass){
	carNameID = env->GetFieldID(dataClass,"mCarName","Ljava/lang/String;");
	carClassNameID = env->GetFieldID(dataClass,"mCarClassName","Ljava/lang/String;");
}
//void getVehicleInformation(JNIEnv* env, jobject dataObject){
//	jobject carNameString = env->NewStringUTF(sharedData->mCarName);
//	env->SetObjectField(dataObject,carNameID,carNameString);
//
//	jobject carClassNameString = env->NewStringUTF(sharedData->mCarClassName);
//	env->SetObjectField(dataObject,carClassNameID,carClassNameString);
//}

void getEventInformationIDs(JNIEnv* env, jclass dataClass){
	lapsInEventFieldID = env->GetFieldID(dataClass,"mLapsInEvent","I");
	trackLocationID = env->GetFieldID(dataClass,"mTrackLocation","Ljava/lang/String;");
	trackVariationID = env->GetFieldID(dataClass,"mTrackVariation","Ljava/lang/String;");
	trackLengthID = env->GetFieldID(dataClass, "mTrackLength","F");
}
//void getEventInformation(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject,lapsInEventFieldID,sharedData->mLapsInEvent);
//	jobject trackLocationString = env->NewStringUTF(sharedData->mTrackLocation);
//	env->SetObjectField(dataObject,trackLocationID,trackLocationString);
//	jobject trackVariationString = env->NewStringUTF(sharedData->mTrackVariation);
//	env->SetObjectField(dataObject,trackVariationID,trackVariationString);
//	env->SetFloatField(dataObject, trackLengthID, sharedData->mTrackLength);
//}

void getTimeIDs(JNIEnv* env, jclass dataClass){
	lapInvalidatedID = env->GetFieldID(dataClass,"mLapInvalidated","Z");
	bestLapTimeID = env->GetFieldID(dataClass,"mBestLapTime","F");
	lastLapTimeID = env->GetFieldID(dataClass,"mLastLapTime","F");
	currentTimeID = env->GetFieldID(dataClass,"mCurrentTime","F");
	splitTimeAheadID = env->GetFieldID(dataClass,"mSplitTimeAhead","F");
	splitTimeBehindID = env->GetFieldID(dataClass,"mSplitTimeBehind","F");
	splitTimeID = env->GetFieldID(dataClass,"mSplitTime","F");
	eventTimeRemainingID = env->GetFieldID(dataClass,"mEventTimeRemaining","F");
	personalFastestLapTimeID = env->GetFieldID(dataClass,"mPersonalFastestLapTime","F");
	worldFastestLapTimeID = env->GetFieldID(dataClass,"mWorldFastestLapTime","F");
	currentS1TimeID = env->GetFieldID(dataClass,"mCurrentSector1Time","F");
	currentS2TimeID = env->GetFieldID(dataClass,"mCurrentSector2Time","F");
	currentS3TimeID = env->GetFieldID(dataClass,"mCurrentSector3Time","F");
	fastestS1TimeID = env->GetFieldID(dataClass,"mFastestSector1Time","F");
	fastestS2TimeID = env->GetFieldID(dataClass,"mFastestSector2Time","F");
	fastestS3TimeID = env->GetFieldID(dataClass,"mFastestSector3Time","F");
	personalFastestS1TimeID = env->GetFieldID(dataClass,"mPersonalFastestSector1Time","F");
	personalFastestS2TimeID = env->GetFieldID(dataClass,"mPersonalFastestSector2Time","F");
	personalFastestS3TimeID = env->GetFieldID(dataClass,"mPersonalFastestSector3Time","F");
	worldFastestS1TimeID = env->GetFieldID(dataClass,"mWorldFastestSector1Time","F");
	worldFastestS2TimeID = env->GetFieldID(dataClass,"mWorldFastestSector2Time","F");
	worldFastestS3TimeID = env->GetFieldID(dataClass,"mWorldFastestSector3Time","F");
}
//void getTimes(JNIEnv* env, jobject dataObject){
//	env->SetBooleanField(dataObject,lapInvalidatedID,sharedData->mLapInvalidated);
//	env->SetFloatField(dataObject,bestLapTimeID,sharedData->mBestLapTime);
//	env->SetFloatField(dataObject,lastLapTimeID,sharedData->mLastLapTime);
//	env->SetFloatField(dataObject,currentTimeID,sharedData->mCurrentTime);
//	env->SetFloatField(dataObject,splitTimeAheadID,sharedData->mSplitTimeAhead);
//	env->SetFloatField(dataObject,splitTimeBehindID,sharedData->mSplitTimeBehind);
//	env->SetFloatField(dataObject,splitTimeID,sharedData->mSplitTime);
//	env->SetFloatField(dataObject,eventTimeRemainingID,sharedData->mEventTimeRemaining);
//	env->SetFloatField(dataObject, personalFastestLapTimeID, sharedData->mPersonalFastestLapTime);
//	env->SetFloatField(dataObject, worldFastestLapTimeID, sharedData->mWorldFastestLapTime);
//	env->SetFloatField(dataObject,currentS1TimeID,sharedData->mCurrentSector1Time);
//	env->SetFloatField(dataObject,currentS2TimeID,sharedData->mCurrentSector2Time);
//	env->SetFloatField(dataObject,currentS3TimeID,sharedData->mCurrentSector3Time);
//	env->SetFloatField(dataObject,fastestS1TimeID,sharedData->mFastestSector1Time);
//	env->SetFloatField(dataObject,fastestS2TimeID,sharedData->mFastestSector2Time);
//	env->SetFloatField(dataObject,fastestS3TimeID,sharedData->mFastestSector3Time);
//	env->SetFloatField(dataObject,personalFastestS1TimeID,sharedData->mPersonalFastestSector1Time);
//	env->SetFloatField(dataObject,personalFastestS2TimeID,sharedData->mPersonalFastestSector2Time);
//	env->SetFloatField(dataObject,personalFastestS3TimeID,sharedData->mPersonalFastestSector3Time);
//	env->SetFloatField(dataObject,worldFastestS1TimeID,sharedData->mWorldFastestSector1Time);
//	env->SetFloatField(dataObject,worldFastestS2TimeID,sharedData->mWorldFastestSector2Time);
//	env->SetFloatField(dataObject,worldFastestS3TimeID,sharedData->mWorldFastestSector3Time);
//}

void getFlagIDs(JNIEnv* env, jclass dataClass){
	highestFlagColourID = env->GetFieldID(dataClass,"mHighestFlagColour","I");
	highestFlagReasonID = env->GetFieldID(dataClass,"mHighestFlagReason","I");
}
//void getFlag(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject, highestFlagColourID, sharedData->mHighestFlagColour);
//	env->SetIntField(dataObject, highestFlagReasonID, sharedData->mHighestFlagReason);
//}

void getPitIDs(JNIEnv* env, jclass dataClass){
	pitModeID = env->GetFieldID(dataClass,"mPitMode","I");
	pitScheduleID = env->GetFieldID(dataClass,"mPitSchedule","I");
}
//void getPit(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject, pitModeID, sharedData->mPitMode);
//	env->SetIntField(dataObject, pitScheduleID, sharedData->mPitSchedule);
//}

void getCarStateIDs(JNIEnv* env, jclass dataClass){
	carFlagID = env->GetFieldID(dataClass,"mCarFlags","I");
	oilTempCelsID = env->GetFieldID(dataClass,"mOilTempCelsius","F");
	oilPressureKPaID = env->GetFieldID(dataClass, "mOilPressureKPa","F");
	waterTempCelsID = env->GetFieldID(dataClass,"mWaterTempCelsius","F");
	waterPressureKPaID = env->GetFieldID(dataClass, "mWaterPressureKPa","F");
	fuelPressureKPaID = env->GetFieldID(dataClass,"mFuelPressureKPa","F");
	fuelLevelID = env->GetFieldID(dataClass,"mFuelLevel","F");
	fuelCapacityID = env->GetFieldID(dataClass,"mFuelCapacity","F");
	speedID = env->GetFieldID(dataClass,"mSpeed","F");
	rpmID = env->GetFieldID(dataClass,"mRPM","F");
	maxRpmID = env->GetFieldID(dataClass,"mMaxRPM","F");
	brakeID = env->GetFieldID(dataClass,"mBrake","F");
	throttleID = env->GetFieldID(dataClass,"mThrottle","F");
	clutchID = env->GetFieldID(dataClass,"mClutch","F");
	steeringID = env->GetFieldID(dataClass,"mSteering","F");
	gearID = env->GetFieldID(dataClass,"mGear","I");
	numGearsID = env->GetFieldID(dataClass,"mNumGears","I");
	odometerKMID = env->GetFieldID(dataClass,"mOdometerKM","F");
	antiLockActiveID = env->GetFieldID(dataClass,"isAbsActive","Z");
	lastOpponentCollisionIndexID = env->GetFieldID(dataClass,"mLastOpponentCollisionIndex","I");
	lastOpponentCollisionMagnitudeID = env->GetFieldID(dataClass,"mLastOpponentCollisionMagnitude","F");
	boostActiveID = env->GetFieldID(dataClass,"mBoostActive","Z");
	boostAmountID = env->GetFieldID(dataClass,"mBoostAmount","F");
	maxTorqueID = env->GetFieldID(dataClass, "mMaxTorque","F");
	maxPowerID = env->GetFieldID(dataClass, "mMaxPower", "F");
}
void getCarState(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject, carFlagID, sharedData->mCarFlags);
//	env->SetFloatField(dataObject, oilTempCelsID, sharedData->mOilTempCelsius);
//	env->SetFloatField(dataObject,oilPressureKPaID,sharedData->mOilPressureKPa);
//	env->SetFloatField(dataObject,waterTempCelsID,sharedData->mWaterTempCelsius);
//	env->SetFloatField(dataObject, waterPressureKPaID, sharedData->mWaterPressureKPa);
//	env->SetFloatField(dataObject, fuelPressureKPaID, sharedData->mFuelPressureKPa);
//	env->SetFloatField(dataObject,fuelLevelID,sharedData->mFuelLevel);
//	env->SetFloatField(dataObject,fuelCapacityID, sharedData->mFuelCapacity);
//	env->SetFloatField(dataObject, speedID, sharedData->mSpeed);

//	env->SetFloatField(dataObject,rpmID,sharedData->mRpm);
//	env->SetFloatField(dataObject,maxRpmID,sharedData->mMaxRPM);
//	env->SetFloatField(dataObject,brakeID,sharedData->mBrake);
//	env->SetFloatField(dataObject,throttleID,sharedData->mThrottle);
//	env->SetFloatField(dataObject,clutchID,sharedData->mClutch);
//	env->SetFloatField(dataObject,steeringID,sharedData->mSteering);
//	env->SetIntField(dataObject,gearID,sharedData->mGear);
//	env->SetIntField(dataObject,numGearsID,sharedData->mNumGears);
//	env->SetFloatField(dataObject, odometerKMID, sharedData->mOdometerKM);
//	env->SetBooleanField(dataObject, antiLockActiveID, sharedData->mAntiLockActive);
//	env->SetIntField(dataObject, lastOpponentCollisionIndexID, sharedData->mLastOpponentCollisionIndex);
//	env->SetIntField(dataObject, lastOpponentCollisionMagnitudeID, sharedData->mLastOpponentCollisionMagnitude);
//	env->SetBooleanField(dataObject, boostActiveID, sharedData->mBoostActive);
//	env->SetFloatField(dataObject, boostAmountID, sharedData->mBoostAmount);
}

void getMotionFieldIDs(JNIEnv* env, jclass dataClass){
	orientationID = env->GetFieldID(dataClass,"mOrientation","[F");
	localVelocityID = env->GetFieldID(dataClass,"mLocalVelocity","[F");
	worldVelocityID = env->GetFieldID(dataClass,"mWorldVelocity","[F");
	angularVelocityID = env->GetFieldID(dataClass,"mAngularVelocity","[F");
	localAccelerationID = env->GetFieldID(dataClass,"mLocalAcceleration","[F");
	worldAccelerationID = env->GetFieldID(dataClass,"mWorldAcceleration","[F");
	extentsCentreID = env->GetFieldID(dataClass,"mExtentsCentre","[F");
}
//void getMotionAndDeviceInfo(JNIEnv* env, jobject dataObject){
//	jfloatArray orientationData = env->NewFloatArray(VEC_MAX);
//	env->SetFloatArrayRegion(orientationData,0,VEC_MAX,sharedData->mOrientation);
//	env->SetObjectField(dataObject,orientationID,orientationData);
//
//	jfloatArray localVelocityData = env->NewFloatArray(VEC_MAX);
//	env->SetFloatArrayRegion(localVelocityData,0,VEC_MAX,sharedData->mLocalVelocity);
//	env->SetObjectField(dataObject,localVelocityID,localVelocityData);
//
//	jfloatArray worldVelocityData = env->NewFloatArray(VEC_MAX);
//	env->SetFloatArrayRegion(worldVelocityData,0,VEC_MAX,sharedData->mWorldVelocity);
//	env->SetObjectField(dataObject,worldVelocityID,worldVelocityData);
//
//	jfloatArray angularVelocityData = env->NewFloatArray(VEC_MAX);
//	env->SetFloatArrayRegion(angularVelocityData,0,VEC_MAX,sharedData->mAngularVelocity);
//	env->SetObjectField(dataObject,angularVelocityID,angularVelocityData);
//
//	jfloatArray localAccelerationData = env->NewFloatArray(VEC_MAX);
//	env->SetFloatArrayRegion(localAccelerationData,0,VEC_MAX,sharedData->mLocalAcceleration);
//	env->SetObjectField(dataObject,localAccelerationID,localAccelerationData);
//
//	jfloatArray worldAccelerationData = env->NewFloatArray(VEC_MAX);
//	env->SetFloatArrayRegion(worldAccelerationData,0,VEC_MAX,sharedData->mWorldAcceleration);
//	env->SetObjectField(dataObject,worldAccelerationID,worldAccelerationData);
//
//	jfloatArray extentsCentreData = env->NewFloatArray(VEC_MAX);
//	env->SetFloatArrayRegion(extentsCentreData,0,VEC_MAX,sharedData->mExtentsCentre);
//	env->SetObjectField(dataObject,extentsCentreID,extentsCentreData);
//}

void getWheelsTyresDataIDs(JNIEnv* env, jclass dataClass){
	tyreFlagsID = env->GetFieldID(dataClass,"mTyreFlags","[I");
	terrainID = env->GetFieldID(dataClass,"mTerrain","[I");
	tyreYID = env->GetFieldID(dataClass,"mTyreY","[F");
	tyreRPSID = env->GetFieldID(dataClass,"mTyreRPS","[F");
	tyreSlipSpeedID = env->GetFieldID(dataClass,"mTyreSlipSpeed","[F");
	tyreTempID = env->GetFieldID(dataClass,"mTyreTemp","[F");
	tyreGripID = env->GetFieldID(dataClass,"mTyreGrip","[F");
	tyreHeightAboveGroundID = env->GetFieldID(dataClass,"mTyreHeightAboveGround","[F");
	tyreLateralStiffnessID = env->GetFieldID(dataClass,"mTyreLateralStiffness","[F");
	tyreWearID = env->GetFieldID(dataClass,"mTyreWear","[F");
	brakeDamageID = env->GetFieldID(dataClass,"mBrakeDamage","[F");
	suspensionDamageID = env->GetFieldID(dataClass,"mSuspensionDamage","[F");
	brakeTempCelciusID = env->GetFieldID(dataClass,"mBrakeTempCelcius","[F");
	tyreTreadTempID = env->GetFieldID(dataClass, "mTyreTreadTemp","[F");
	tyreLayerTempID = env->GetFieldID(dataClass, "mTyreLayerTemp","[F");
	tyreCarcassTempID = env->GetFieldID(dataClass, "mTyreCarcassTemp", "[F");
	tyreRimTempID = env->GetFieldID(dataClass, "mTyreRimTemp", "[F");
	tyreInternalAirTempID = env->GetFieldID(dataClass, "mTyreInternalAirTemp", "[F");
}
//void getWheelsTyresData(JNIEnv* env, jobject dataObject){
//	jintArray tyreFlagsData = env->NewIntArray(TYRE_MAX);
//	for(int i = 0; i < TYRE_MAX; i++){
//		tempTyreFlags[i] = sharedData->mTyreFlags[i];
//	}
//	env->SetIntArrayRegion(tyreFlagsData,0,TYRE_MAX,tempTyreFlags);
//	env->SetObjectField(dataObject,tyreFlagsID,tyreFlagsData);
//
//	jintArray terrainData = env->NewIntArray(TYRE_MAX);
//	for(int i = 0; i < TYRE_MAX; i++){
//		tempTyreFlags[i] = sharedData->mTerrain[i];
//	}
//	env->SetIntArrayRegion(terrainData,0,TYRE_MAX,tempTyreFlags);
//	env->SetObjectField(dataObject,terrainID,terrainData);
//
//	jfloatArray tyreYData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreYData,0,TYRE_MAX,sharedData->mTyreY);
//	env->SetObjectField(dataObject,tyreYID,tyreYData);
//
//	jfloatArray tyreRPSData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreRPSData,0,TYRE_MAX,sharedData->mTyreRPS);
//	env->SetObjectField(dataObject,tyreRPSID,tyreRPSData);
//
//	jfloatArray tyreSlipSpeedData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreSlipSpeedData,0,TYRE_MAX,sharedData->mTyreSlipSpeed);
//	env->SetObjectField(dataObject,tyreSlipSpeedID,tyreSlipSpeedData);
//
//	jfloatArray tyreTempData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreTempData,0,TYRE_MAX,sharedData->mTyreTemp);
//	env->SetObjectField(dataObject,tyreTempID,tyreTempData);
//
//	jfloatArray tyreGripData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreGripData,0,TYRE_MAX,sharedData->mTyreGrip);
//	env->SetObjectField(dataObject,tyreGripID,tyreGripData);
//
//	jfloatArray tyreHeightAboveGroundData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreHeightAboveGroundData,0,TYRE_MAX,sharedData->mTyreHeightAboveGround);
//	env->SetObjectField(dataObject,tyreHeightAboveGroundID,tyreHeightAboveGroundData);
//
//	jfloatArray tyreLateralStiffnessData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreLateralStiffnessData,0,TYRE_MAX,sharedData->mTyreLateralStiffness);
//	env->SetObjectField(dataObject,tyreLateralStiffnessID,tyreLateralStiffnessData);
//
//	jfloatArray tyreWearData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(tyreWearData,0,TYRE_MAX,sharedData->mTyreWear);
//	env->SetObjectField(dataObject,tyreWearID,tyreWearData);
//
//	jfloatArray brakeDamageData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(brakeDamageData,0,TYRE_MAX,sharedData->mBrakeDamage);
//	env->SetObjectField(dataObject,brakeDamageID,brakeDamageData);
//
//	jfloatArray suspensionDamageData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(suspensionDamageData,0,TYRE_MAX,sharedData->mSuspensionDamage);
//	env->SetObjectField(dataObject,suspensionDamageID,suspensionDamageData);
//
//	jfloatArray brakeTempCelciusData = env->NewFloatArray(TYRE_MAX);
//	env->SetFloatArrayRegion(brakeTempCelciusData,0,TYRE_MAX,sharedData->mBrakeTempCelsius);
//	env->SetObjectField(dataObject,brakeTempCelciusID,brakeTempCelciusData);
//
//	jfloatArray tyreTreadData = env->NewFloatArray(TYRE_MAX);
//	jfloatArray tyreLayerData = env->NewFloatArray(TYRE_MAX);
//	jfloatArray tyreCarcassData = env->NewFloatArray(TYRE_MAX);
//	jfloatArray tyreRimData = env->NewFloatArray(TYRE_MAX);
//	jfloatArray tyreInternalAirData = env->NewFloatArray(TYRE_MAX);
//
//	env->SetFloatArrayRegion(tyreTreadData, 0, TYRE_MAX, sharedData->mTyreTreadTemp);
//	env->SetObjectField(dataObject, tyreTreadTempID, tyreTreadData);
//
//	env->SetFloatArrayRegion(tyreLayerData, 0, TYRE_MAX, sharedData->mTyreLayerTemp);
//	env->SetObjectField(dataObject, tyreLayerTempID, tyreLayerData);
//
//	env->SetFloatArrayRegion(tyreCarcassData, 0, TYRE_MAX, sharedData->mTyreCarcassTemp);
//	env->SetObjectField(dataObject, tyreCarcassTempID, tyreCarcassData);
//
//	env->SetFloatArrayRegion(tyreRimData, 0, TYRE_MAX, sharedData->mTyreRimTemp);
//	env->SetObjectField(dataObject, tyreRimTempID, tyreRimData);
//
//	env->SetFloatArrayRegion(tyreInternalAirData, 0, TYRE_MAX, sharedData->mTyreInternalAirTemp);
//	env->SetObjectField(dataObject, tyreInternalAirTempID, tyreInternalAirData);
//}

void getCarDamageIDs(JNIEnv* env, jclass dataClass){
	crashStateID = env->GetFieldID(dataClass, "mCrashState","I");
	aeroDamageID = env->GetFieldID(dataClass, "mAeroDamage","F");
	engineDamageID = env->GetFieldID(dataClass, "mEngineDamage","F");
}
//void getCarDamage(JNIEnv* env, jobject dataObject){
//	env->SetIntField(dataObject, crashStateID, sharedData->mCrashState);
//	env->SetFloatField(dataObject,aeroDamageID,sharedData->mAeroDamage);
//	env->SetFloatField(dataObject, engineDamageID, sharedData->mEngineDamage);
//}

void getWeatherIDs(JNIEnv* env, jclass dataClass){
	ambientTempID = env->GetFieldID(dataClass,"mAmbientTemperature","F");
	trackTempID = env->GetFieldID(dataClass, "mTrackTemperature","F");
	rainDensityID = env->GetFieldID(dataClass,"mRainDensity","F");
	windSpeedID = env->GetFieldID(dataClass,"mWindSpeed","F");
	windDirectionXID = env->GetFieldID(dataClass,"mWindDirectionX","F");
	windDirectionYID = env->GetFieldID(dataClass,"mWindDirectionY","F");
	cloudBrightnessID = env->GetFieldID(dataClass,"mCloudBrightness","F");
}
//void getWeather(JNIEnv* env, jobject dataObject){
//	env->SetFloatField(dataObject,ambientTempID,sharedData->mAmbientTemperature);
//	env->SetFloatField(dataObject,trackTempID,sharedData->mTrackTemperature);
//	env->SetFloatField(dataObject,rainDensityID, sharedData->mRainDensity);
//	env->SetFloatField(dataObject,windSpeedID, sharedData->mWindSpeed);
//	env->SetFloatField(dataObject,windDirectionXID, sharedData->mWindDirectionX);
//	env->SetFloatField(dataObject,windDirectionYID, sharedData->mWindDirectionY);
//	env->SetFloatField(dataObject,cloudBrightnessID, sharedData->mCloudBrightness);
//}

void getParticipantIDs(JNIEnv* env, jclass dataClass){
	participantsID = env->GetFieldID(dataClass, "mParticipants", "[Lpcars/server/VDashData$Participant;");

	participantActiveID = env->GetFieldID(participantClass, "mIsActive", "Z");
	participantNameID = env->GetFieldID(participantClass, "mName", "Ljava/lang/String;");
	particiantWorldPositionID = env->GetFieldID(participantClass, "mWorldPosition","[F");
	participantCurrentLapDistanceID = env->GetFieldID(participantClass, "mCurrentLapDistance","F");
	participantRacePositionID = env->GetFieldID(participantClass, "mRacePosition","I");
	participantLapsCompletedID = env->GetFieldID(participantClass, "mLapsCompleted","I");
	participantCurrentLapID = env->GetFieldID(participantClass, "mCurrentLap","I");
	participantCurrentSectorID = env->GetFieldID(participantClass, "mCurrentSector","I");
}
//void getParticipants(JNIEnv* env, jobject dataObject){
//	participantClass = env->FindClass("pcars/server/VDashData$Participant");
//
//	if(sharedData->mNumParticipants > 0){
//		jobject participant = env->NewObject(participantClass, participantConstructor);
//		jobjectArray allParticipants= env->NewObjectArray(sharedData->mNumParticipants, participantClass, NULL);
//
//		for(int i = 0; i < sharedData->mNumParticipants; i++){
//			participant = env->NewObject(participantClass, participantConstructor);
//
//			ParticipantInfo p = sharedData->mParticipantInfo[i];
//
//			env->SetBooleanField(participant, participantActiveID, p.mIsActive);
//			jobject participantNameString = env->NewStringUTF(p.mName);
//			env->SetObjectField(participant,participantNameID,participantNameString);
//
//			jfloatArray participantWorldPosition = env->NewFloatArray(VEC_MAX);
//			env->SetFloatArrayRegion(participantWorldPosition,0,VEC_MAX,p.mWorldPosition);
//			env->SetObjectField(participant,particiantWorldPositionID,participantWorldPosition);
//
//			env->SetFloatField(participant, participantCurrentLapDistanceID, p.mCurrentLapDistance);
//			env->SetIntField(participant, participantRacePositionID, p.mRacePosition);
//			env->SetIntField(participant, participantLapsCompletedID, p.mLapsCompleted);
//			env->SetIntField(participant, participantCurrentLapID, p.mCurrentLap);
//			env->SetIntField(participant, participantCurrentSectorID, p.mCurrentSector);
//
//			env->SetObjectArrayElement(allParticipants, i, participant);
//		}
//
//		env->SetObjectField(dataObject, participantsID, allParticipants);
//	}
//}

JNIEXPORT jboolean JNICALL Java_data_IRacingDataSource_irsdkStartup(JNIEnv* env, jclass)
{
	if(!hMemMapFile)
	{
		hMemMapFile = OpenFileMapping( FILE_MAP_READ, FALSE, IRSDK_MEMMAPFILENAME);
		lastTickCount = INT_MAX;
	}

	if(hMemMapFile)
	{
		if(!pSharedMem)
		{
			pSharedMem = (const char *)MapViewOfFile(hMemMapFile, FILE_MAP_READ, 0, 0, 0);
			pHeader = (irsdk_header *)pSharedMem;
			lastTickCount = INT_MAX;
		}

		if(pSharedMem)
		{
			if(!hDataValidEvent)
			{
				hDataValidEvent = OpenEvent(SYNCHRONIZE, false, IRSDK_DATAVALIDEVENTNAME);
				lastTickCount = INT_MAX;
			}

			if(hDataValidEvent)
			{
				isInitialized = true;
				return isInitialized;
			}
			//else printf("Error opening event: %d\n", GetLastError());
		}
		//else printf("Error mapping file: %d\n", GetLastError());
	}
	//else printf("Error opening file: %d\n", GetLastError());

	isInitialized = false;
	return isInitialized;
}

JNIEXPORT jobject JNICALL Java_data_IRacingDataSource_getData(JNIEnv* env, jclass){
	dataClass = env->FindClass("pcars/server/VDashData");
	jobject dataObject = env->NewObject(dataClass, constructor);

	//getVersion(env, dataObject);
	//getStates(env, dataObject);
	//getParticipantInfo(env, dataObject);
	//getUnfilteredInput(env, dataObject);
	//getVehicleInformation(env, dataObject);
	//getEventInformation(env, dataObject);
	//getTimes(env, dataObject);
	//getFlag(env, dataObject);
	//getPit(env, dataObject);
	getCarState(env, dataObject);
	//getMotionAndDeviceInfo(env, dataObject);
	//getWheelsTyresData(env, dataObject);
	//getCarDamage(env, dataObject);
	//getWeather(env, dataObject);
	//getParticipants(env, dataObject);

	return dataObject;
}

JNIEXPORT void JNICALL Java_data_IRacingDataSource_findFieldIDs(JNIEnv* env, jclass){
	dataClass = env->FindClass("data/VDashData");
	participantClass = env->FindClass("data/VDashData$Participant");

	constructor = env->GetMethodID(dataClass, "<init>", "()V");
	participantConstructor = env->GetMethodID(participantClass, "<init>","()V");

	getVersionIDs(env, dataClass);
	getStatesIDs(env,dataClass);
	getParticipantInfoIDs(env, dataClass);
	getUnfilteredInputIDs(env,dataClass);
	getVehicleInformationIDs(env, dataClass);
	getEventInformationIDs(env, dataClass);
	getTimeIDs(env, dataClass);
	getFlagIDs(env, dataClass);
	getPitIDs(env, dataClass);
	getCarStateIDs(env,dataClass);
	getMotionFieldIDs(env,dataClass);
	getWheelsTyresDataIDs(env,dataClass);
	getCarDamageIDs(env, dataClass);
	getWeatherIDs(env, dataClass);
	getParticipantIDs(env, dataClass);
}
