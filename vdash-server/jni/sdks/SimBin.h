struct racedata{
float userInput[ 6 ]; // This structure allows for a number of parameters to be
// passed from the user to the exteral application via control input
// in the game. The ISIInputType enum describes which element of this
// array corresponds to which in-game control. Note that this data
// is floating point, and can be configured in the game to be driven
// by an analog input device (joystick). The user may also map the
// control to a keyboard key, or a digital controller button. This
// means that the value may be anywhere between 0.0 and 1.0. Also note
// that these values will not be debounced; if the user
// maps the "External Signal Up" control to a key on the keyboard,
// the coresponding value in this array will remain 1.0 for as long
// as the user holds the key down.

float rpm; // Engine speed, Radians Per Second.
float maxEngineRPS; // For use with an "analog" rpm display.
float fuelPressure; // KPa
float fuel; // Current liters of fuel in the tank(s).
float fuelCapacityLiters; // Maximum capacity of fuel tank(s).
float engineWaterTemp; //
float engineOilTemp; //
float engineOilPressure; //

float carSpeed; // meters per second
long numberOfLaps; // # of laps in race, or -1 if player is not in
// race mode (player is in practice or test mode).

long completedLaps; // How many laps the player has completed. If this
// value is 6, the player is on his 7th lap. -1 = n/a

float lapTimeBest; // Seconds. -1.0 = none
float lapTimePrevious; // Seconds. -1.0 = none
float lapTimeCurrent; // Seconds. -1.0 = none
long position; // Current position. 1 = first place.
long numCars; // Number of cars (including the player) in the race.
long gear; // -2 = no data available, -1 = reverse, 0 = neutral,
// 1 = first gear... (valid range -1 to 7).

//float tireTemp[ TIRE_LOC_MAX ][ TREAD_LOC_MAX ]; // Temperature of three points
float tirefrontleft[3]; //tire values from [0]=left to [2]=right
float tirefrontright[3]; //tire values from [0]=left to [2]=right
float tirerearleft[3]; //tire values from [0]=left to [2]=right
float tirerearright[3]; //tire values from [0]=left to [2]=right
// across the tread of each tire.
long numPenalties; // Number of penalties pending for the player.

float carCGLoc[3]; // Physical location of car's Center of Gravity in world space, X,Y,Z... Y=up.
//float carOri[ ORI_MAXIMUM ]; // Pitch, Yaw, Roll. Electronic compass, perhaps?
float pitch;
float yaw;
float roll;
//float localAcceleration[3]; // Acceleration in three axes (X, Y, Z) of car body (divide by
// 9.81 to get G-force). From car center, +X=left, +Y=up, +Z=back.
float lateral; //Force left-right
float vertical; //force up-down
float longitudinal; //force faster, slower
};
