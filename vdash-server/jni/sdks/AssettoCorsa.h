#pragma once

typedef int AC_STATUS;

#define AC_OFF 0
#define AC_REPLAY 1
#define AC_LIVE 2
#define AC_PAUSE 3

typedef int AC_SESSION_TYPE;

#define AC_UNKNOWN -1
#define AC_PRACTICE 0
#define AC_QUALIFY 1
#define AC_RACE 2
#define AC_HOTLAP 3
#define AC_TIME_ATTACK 4
#define AC_DRIFT 5
#define AC_DRAG 6


#pragma pack(push)
#pragma pack(4)

struct SPageFilePhysics
{
int packetId = 0;
float gas = 0;													//DONE
float brake = 0;												//DONE
float fuel = 0;													//DONE
int gear = 0;													//DONE
int rpms = 0;													//DONE
float steerAngle = 0;
float speedKmh = 0;												//DONE
float velocity[3];												//DONE
float accG[3];													//DONE
float wheelSlip[4];
float wheelLoad[4];
float wheelsPressure[4];
float wheelAngularSpeed[4];
float tyreWear[4];												//DONE
float tyreDirtyLevel[4];
float tyreCoreTemperature[4];
float camberRAD[4];
float suspensionTravel[4];
float drs = 0;
float tc = 0;
float heading = 0;
float pitch = 0;
float roll = 0;
float cgHeight;
//TODO what are these damages?
float carDamage[5];
int numberOfTyresOut = 0;
int pitLimiterOn = 0;
float abs = 0;
};


struct SPageFileGraphic
{
int packetId = 0;
AC_STATUS status = AC_OFF;											//DONE
AC_SESSION_TYPE session = AC_PRACTICE;								//DONE
char currentTime[15];												//DONE
char lastTime[15];													//DONE
//TODO is this session or personal?
char bestTime[15];													//DONE
char split[15];														//DONE
int completedLaps = 0;												//DONE
int position = 0;													//DONE
int iCurrentTime = 0;												//DONE
int iLastTime = 0;													//DONE
int iBestTime = 0;													//DONE
float sessionTimeLeft = 0;											//DONE
float distanceTraveled = 0;
int isInPit = 0;													//DONE
int currentSectorIndex = 0;											//DONE
int lastSectorTime = 0;
int numberOfLaps = 0;												//DONE
char tyreCompound[33];

float replayTimeMultiplier = 0;
float normalizedCarPosition = 0;
float carCoordinates[3];
};


struct SPageFileStatic
{
char smVersion[15];
char acVersion[15];
// session static info
int numberOfSessions = 0;
int numCars = 0;													//DONE
char carModel[33];													//DONE
char track[33];														//DONE
char playerName[33];
char playerSurname[33];
char playerNick[33];												//DONE
int sectorCount = 0;

// car static info
float maxTorque = 0;												//DONE
float maxPower = 0;													//DONE
int maxRpm = 0;														//DONE
float maxFuel = 0;													//DONE
float suspensionMaxTravel[4];
float tyreRadius[4];
};


#pragma pack(pop)
