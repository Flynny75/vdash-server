/*
 * ets2_mmf.h
 *
 *  Created on: 7 Jul 2015
 *      Author: Alex
 */

#ifndef JNI_SDKS_ETS2_SDK_INCLUDE_ETS2_MMF_H_
#define JNI_SDKS_ETS2_SDK_INCLUDE_ETS2_MMF_H_

#include "scssdk_telemetry.h"

const size_t MAX_SUPPORTED_WHEEL_COUNT = 4;

#pragma pack(push)
#pragma pack(1)

/**
 * @brief The layout of the shared memory.
*/
struct telemetry_state_t
{
	scs_u8_t				running;					// Is the telemetry running or it is paused?

	scs_value_dplacement_t	ws_truck_placement;			// SCS_TELEMETRY_TRUCK_CHANNEL_world_placement

	scs_float_t				speedometer_speed;			// SCS_TELEMETRY_TRUCK_CHANNEL_speed
	scs_float_t				rpm;						// SCS_TELEMETRY_TRUCK_CHANNEL_engine_rpm
	scs_float_t				maxRpm;
	scs_s32_t				gear;						// SCS_TELEMETRY_TRUCK_CHANNEL_engine_gear
	scs_s32_t				maxGears;					// SCS_TELEMETRY

	scs_float_t				brakeTemp;					//Brake temp in C, approx for whole truck
	scs_float_t				oilTemp;					//C
	scs_float_t				oilPressure;				//PSI
	scs_float_t				waterTemp;					//C

	scs_float_t				engineDamage;				//%
	scs_float_t				fuel;						//L
	scs_float_t				fuelCapacity;				//L

	//JOB DETAILS
	char					jobSourceCity[64];				//Name of job source city
	char					jobDestinationCity[64];			//Name of the job destination city
	scs_float_t				navigationDistance;			//meters

	scs_float_t				steering;					// SCS_TELEMETRY_TRUCK_CHANNEL_effective_steering
	scs_float_t				throttle;					// SCS_TELEMETRY_TRUCK_CHANNEL_effective_throttle
	scs_float_t				brake;						// SCS_TELEMETRY_TRUCK_CHANNEL_effective_brake
	scs_float_t				clutch;						// SCS_TELEMETRY_TRUCK_CHANNEL_effective_clutch

	scs_value_fvector_t		linear_valocity;			// SCS_TELEMETRY_TRUCK_CHANNEL_local_linear_velocity
	scs_value_fvector_t		angular_velocity;			// SCS_TELEMETRY_TRUCK_CHANNEL_local_angular_velocity
	scs_value_fvector_t		linear_acceleration;		// SCS_TELEMETRY_TRUCK_CHANNEL_local_linear_acceleration
	scs_value_fvector_t		angular_acceleration;		// SCS_TELEMETRY_TRUCK_CHANNEL_local_angular_acceleration
	scs_value_fvector_t		cabin_angular_velocity;		// SCS_TELEMETRY_TRUCK_CHANNEL_cabin_angular_velocity
	scs_value_fvector_t		cabin_angular_acceleration;	// SCS_TELEMETRY_TRUCK_CHANNEL_cabin_angular_acceleration

	scs_u32_t				wheel_count;				// SCS_TELEMETRY_CONFIG_ATTRIBUTE_wheel_count
	scs_float_t				wheel_deflections[MAX_SUPPORTED_WHEEL_COUNT]; // SCS_TELEMETRY_TRUCK_CHANNEL_wheel_susp_deflection
};

#pragma pack(pop)


#endif /* JNI_SDKS_ETS2_SDK_INCLUDE_ETS2_MMF_H_ */
