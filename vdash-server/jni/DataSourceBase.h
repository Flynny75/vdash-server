/*
 * DataSourceBase.h
 *
 *  Created on: 22 Nov 2014
 *      Author: Alex
 */

#ifndef DATASOURCEBASE_H_
#define DATASOURCEBASE_H_

#include <iostream>
#include <jni.h>
#include <windows.h>
#include <tchar.h>

#define DATA_CLASS "data/VDashData"
#define PARTICIPANT_CLASS "data/VDashData$Participant"

//VERSIONS
static jfieldID apiVersionNumberID,
				buildNumberID,
				apiVersionNumberStringID,
				buildNumberStringID;

//GAME STATES
static jfieldID gameStateFieldID,
				sessionStateFieldID,
				raceStateID;

//PARTICIPANTS
static jfieldID viewingParticipantID,
				numParticipantsID;

//UNFILTERED INPUT FIELDS
static jfieldID unfilteredThrottleID,
				unfilteredBrakeID,
				unfilteredSteeringID,
				unfilteredClutchID;

//VEHICLE INFORMATIONS
static jfieldID carNameID,
				carClassNameID;

//EVENT INFORMATIONS
static jfieldID lapsInEventFieldID,
				trackLocationID,
				trackVariationID,
				trackLengthID;

//TIME FIELD IDs
static jfieldID lapInvalidatedID,
				bestLapTimeID,
				lastLapTimeID,
				currentTimeID,
				splitTimeAheadID,
				splitTimeBehindID,
				splitTimeID,
				eventTimeRemainingID,
				personalFastestLapTimeID,
				worldFastestLapTimeID,
				currentS1TimeID,
				currentS2TimeID,
				currentS3TimeID,
				fastestS1TimeID,
				fastestS2TimeID,
				fastestS3TimeID,
				personalFastestS1TimeID,
				personalFastestS2TimeID,
				personalFastestS3TimeID,
				worldFastestS1TimeID,
				worldFastestS2TimeID,
				worldFastestS3TimeID,
				lastSectorTimeID;

//FLAG FIELDS
static jfieldID highestFlagColourID,
				highestFlagReasonID;

//PIT
static jfieldID pitModeID,
				pitScheduleID;

//CAR STATE	FIELD IDs
static jfieldID carFlagID,
				oilTempCelsID,
				oilPressureKPaID,
				waterTempCelsID,
				waterPressureKPaID,
				fuelPressureKPaID,
				fuelLevelID,
				fuelCapacityID,
				speedID,
				rpmID,
				maxRpmID,
				brakeID,
				throttleID,
				clutchID,
				steeringID,
				gearID,
				numGearsID,
				odometerKMID,
				antiLockActiveID,
				lastOpponentCollisionIndexID,
				lastOpponentCollisionMagnitudeID,
				boostActiveID,
				boostAmountID,
				maxTorqueID,
				maxPowerID;

//MOTION STATE FIELD IDs
static jfieldID orientationID,
				localVelocityID,
				worldVelocityID,
				angularVelocityID,
				localAccelerationID,
				worldAccelerationID,
				extentsCentreID;

//WHEELS TYRES FIELD IDs
static jfieldID tyreFlagsID,
				terrainID,
				tyreYID,
				tyreRPSID,
				tyreSlipSpeedID,
				tyreTempID,
				tyreGripID,
				tyreHeightAboveGroundID,
				tyreLateralStiffnessID,
				tyreConditionID,
				brakeConditionID,
				suspensionConditionID,
				brakeTempCelciusID,
				tyreTreadTempID,
				tyreLayerTempID,
				tyreCarcassTempID,
				tyreRimTempID,
				tyreInternalAirTempID,
				tyrePressuresID,
				tyreInnerTempsID,
				tyreMidTempsID,
				tyreOuterTempsID;

//CAR DAMAGE
static jfieldID crashStateID,
				aeroConditionID,
				engineConditionID,
				transmissionConditionID;

//WEATHER
static jfieldID ambientTempID,
				trackTempID,
				rainDensityID,
				windSpeedID,
				windDirectionXID,
				windDirectionYID,
				cloudBrightnessID;


static jclass dataClass;
static jclass participantClass;

static jfieldID participantActiveID,
				participantNameID,
				participantWorldPositionID,
				participantCurrentLapDistanceID,
				participantRacePositionID,
				participantLapsCompletedID,
				participantCurrentLapID,
				participantCurrentSectorID;

static jfieldID participantsID;

//ETS2
static jfieldID navigationDistanceID,
				jobSourceCityID,
				jobDestinationCityID;

static jmethodID constructor, participantConstructor;

jcharArray makeCharArray(JNIEnv *env, const char * str){
	jobject dataString = env->NewStringUTF(str);
	int stringLen = strlen(str);

	jcharArray array = env->NewCharArray(stringLen);
	jchar *dst = (jchar *)calloc(sizeof(jchar), stringLen);

	for(int i = 0; i < stringLen; i++){
		dst[i] = (jchar)str[i];
	}

	env->SetCharArrayRegion(array, 0, stringLen, dst);

	free(dst);
	return array;
}

float convertCelsToKelvin(float c){
	return c + 273.15;
}

void getVersionIDs(JNIEnv* env, jclass dataClass){
	apiVersionNumberID = env->GetFieldID(dataClass,"mVersionNumber","I");
	buildNumberID = env->GetFieldID(dataClass,"mBuildVersionNumber","I");
	apiVersionNumberStringID = env->GetFieldID(dataClass,"mVersionNumberString","[C");
	buildNumberStringID = env->GetFieldID(dataClass, "mBuildVersionNumberString","[C");
}
void getStatesIDs(JNIEnv* env, jclass dataClass){
	gameStateFieldID = env->GetFieldID(dataClass,"mGameState","I");
	sessionStateFieldID = env->GetFieldID(dataClass,"mSessionState","I");
	raceStateID = env->GetFieldID(dataClass,"mRaceState","I");
}
void getParticipantInfoIDs(JNIEnv* env, jclass dataClass){
	viewingParticipantID = env->GetFieldID(dataClass,"mViewedParticipantIndex","I");
	numParticipantsID = env->GetFieldID(dataClass,"mNumParticipants","I");
}
void getUnfilteredInputIDs(JNIEnv* env, jclass dataClass){
	unfilteredThrottleID = env->GetFieldID(dataClass,"mUnfilteredThrottle","F");
	unfilteredBrakeID = env->GetFieldID(dataClass,"mUnfilteredBrake","F");
	unfilteredSteeringID = env->GetFieldID(dataClass,"mUnfilteredSteering","F");
	unfilteredClutchID = env->GetFieldID(dataClass,"mUnfilteredClutch","F");
}
void getVehicleInformationIDs(JNIEnv* env, jclass dataClass){
	carNameID = env->GetFieldID(dataClass,"mCarName","[C");
	carClassNameID = env->GetFieldID(dataClass,"mCarClassName","[C");
}
void getEventInformationIDs(JNIEnv* env, jclass dataClass){
	lapsInEventFieldID = env->GetFieldID(dataClass,"mLapsInEvent","I");
	trackLocationID = env->GetFieldID(dataClass,"mTrackLocation","[C");
	trackVariationID = env->GetFieldID(dataClass,"mTrackVariation","[C");
	trackLengthID = env->GetFieldID(dataClass, "mTrackLength","F");
}
void getTimeIDs(JNIEnv* env, jclass dataClass){
	lapInvalidatedID = env->GetFieldID(dataClass,"mLapInvalidated","Z");
	bestLapTimeID = env->GetFieldID(dataClass,"mBestLapTime","F");
	lastLapTimeID = env->GetFieldID(dataClass,"mLastLapTime","F");
	currentTimeID = env->GetFieldID(dataClass,"mCurrentTime","F");
	splitTimeAheadID = env->GetFieldID(dataClass,"mSplitTimeAhead","F");
	splitTimeBehindID = env->GetFieldID(dataClass,"mSplitTimeBehind","F");
	splitTimeID = env->GetFieldID(dataClass,"mSplitTime","F");
	eventTimeRemainingID = env->GetFieldID(dataClass,"mEventTimeRemaining","F");
	personalFastestLapTimeID = env->GetFieldID(dataClass,"mPersonalFastestLapTime","F");
	worldFastestLapTimeID = env->GetFieldID(dataClass,"mWorldFastestLapTime","F");
	currentS1TimeID = env->GetFieldID(dataClass,"mCurrentSector1Time","F");
	currentS2TimeID = env->GetFieldID(dataClass,"mCurrentSector2Time","F");
	currentS3TimeID = env->GetFieldID(dataClass,"mCurrentSector2Time","F");
	fastestS1TimeID = env->GetFieldID(dataClass,"mFastestSector1Time","F");
	fastestS2TimeID = env->GetFieldID(dataClass,"mFastestSector2Time","F");
	fastestS3TimeID = env->GetFieldID(dataClass,"mFastestSector3Time","F");
	personalFastestS1TimeID = env->GetFieldID(dataClass,"mPersonalFastestSector1Time","F");
	personalFastestS2TimeID = env->GetFieldID(dataClass,"mPersonalFastestSector2Time","F");
	personalFastestS3TimeID = env->GetFieldID(dataClass,"mPersonalFastestSector3Time","F");
	worldFastestS1TimeID = env->GetFieldID(dataClass,"mWorldFastestSector1Time","F");
	worldFastestS2TimeID = env->GetFieldID(dataClass,"mWorldFastestSector2Time","F");
	worldFastestS3TimeID = env->GetFieldID(dataClass,"mWorldFastestSector3Time","F");
	lastSectorTimeID = env->GetFieldID(dataClass,"mLastSectorTime","F");
}
void getFlagIDs(JNIEnv* env, jclass dataClass){
	highestFlagColourID = env->GetFieldID(dataClass,"mHighestFlagColour","I");
	highestFlagReasonID = env->GetFieldID(dataClass,"mHighestFlagReason","I");
}
void getPitIDs(JNIEnv* env, jclass dataClass){
	pitModeID = env->GetFieldID(dataClass,"mPitMode","I");
	pitScheduleID = env->GetFieldID(dataClass,"mPitSchedule","I");
}
void getCarStateIDs(JNIEnv* env, jclass dataClass){
	carFlagID = env->GetFieldID(dataClass,"mCarFlags","I");
	oilTempCelsID = env->GetFieldID(dataClass,"mOilTempCelsius","F");
	oilPressureKPaID = env->GetFieldID(dataClass, "mOilPressureKPa","F");
	waterTempCelsID = env->GetFieldID(dataClass,"mWaterTempCelsius","F");
	waterPressureKPaID = env->GetFieldID(dataClass, "mWaterPressureKPa","F");
	fuelPressureKPaID = env->GetFieldID(dataClass,"mFuelPressureKPa","F");
	fuelLevelID = env->GetFieldID(dataClass,"mFuelLevel","F");
	fuelCapacityID = env->GetFieldID(dataClass,"mFuelCapacity","F");
	speedID = env->GetFieldID(dataClass,"mSpeed","F");
	rpmID = env->GetFieldID(dataClass,"mRPM","F");
	maxRpmID = env->GetFieldID(dataClass,"mMaxRPM","F");
	brakeID = env->GetFieldID(dataClass,"mBrake","F");
	throttleID = env->GetFieldID(dataClass,"mThrottle","F");
	clutchID = env->GetFieldID(dataClass,"mClutch","F");
	steeringID = env->GetFieldID(dataClass,"mSteering","F");
	gearID = env->GetFieldID(dataClass,"mGear","I");
	numGearsID = env->GetFieldID(dataClass,"mNumGears","I");
	odometerKMID = env->GetFieldID(dataClass,"mOdometerKM","F");
	antiLockActiveID = env->GetFieldID(dataClass,"isAbsActive","Z");
	lastOpponentCollisionIndexID = env->GetFieldID(dataClass,"mLastOpponentCollisionIndex","I");
	lastOpponentCollisionMagnitudeID = env->GetFieldID(dataClass,"mLastOpponentCollisionMagnitude","F");
	boostActiveID = env->GetFieldID(dataClass,"mBoostActive","Z");
	boostAmountID = env->GetFieldID(dataClass,"mBoostAmount","F");
	maxTorqueID = env->GetFieldID(dataClass, "mMaxTorque","F");
	maxPowerID = env->GetFieldID(dataClass, "mMaxPower", "F");
}
void getMotionFieldIDs(JNIEnv* env, jclass dataClass){
	orientationID = env->GetFieldID(dataClass,"mOrientation","[F");
	localVelocityID = env->GetFieldID(dataClass,"mLocalVelocity","[F");
	worldVelocityID = env->GetFieldID(dataClass,"mWorldVelocity","[F");
	angularVelocityID = env->GetFieldID(dataClass,"mAngularVelocity","[F");
	localAccelerationID = env->GetFieldID(dataClass,"mLocalAcceleration","[F");
	worldAccelerationID = env->GetFieldID(dataClass,"mWorldAcceleration","[F");
	extentsCentreID = env->GetFieldID(dataClass,"mExtentsCentre","[F");
}
void getWheelsTyresDataIDs(JNIEnv* env, jclass dataClass){
	tyreFlagsID = env->GetFieldID(dataClass,"mTyreFlags","[I");
	terrainID = env->GetFieldID(dataClass,"mTerrain","[I");
	tyreYID = env->GetFieldID(dataClass,"mTyreY","[F");
	tyreRPSID = env->GetFieldID(dataClass,"mTyreRPS","[F");
	tyreSlipSpeedID = env->GetFieldID(dataClass,"mTyreSlipSpeed","[F");
	tyreTempID = env->GetFieldID(dataClass,"mTyreTemp","[F");
	tyreGripID = env->GetFieldID(dataClass,"mTyreGrip","[F");
	tyreHeightAboveGroundID = env->GetFieldID(dataClass,"mTyreHeightAboveGround","[F");
	tyreLateralStiffnessID = env->GetFieldID(dataClass,"mTyreLateralStiffness","[F");
	tyreConditionID = env->GetFieldID(dataClass,"mTyreCondition","[F");
	brakeConditionID = env->GetFieldID(dataClass,"mBrakeCondition","[F");
	suspensionConditionID = env->GetFieldID(dataClass,"mSuspensionCondition","[F");
	brakeTempCelciusID = env->GetFieldID(dataClass,"mBrakeTempCelcius","[F");
	tyreTreadTempID = env->GetFieldID(dataClass, "mTyreTreadTemp","[F");
	tyreLayerTempID = env->GetFieldID(dataClass, "mTyreLayerTemp","[F");
	tyreCarcassTempID = env->GetFieldID(dataClass, "mTyreCarcassTemp", "[F");
	tyreRimTempID = env->GetFieldID(dataClass, "mTyreRimTemp", "[F");
	tyreInternalAirTempID = env->GetFieldID(dataClass, "mTyreInternalAirTemp", "[F");
	tyrePressuresID = env->GetFieldID(dataClass, "mTyrePressures", "[F");
	tyreInnerTempsID = env->GetFieldID(dataClass, "mTyreInnerTemps", "[F");
	tyreMidTempsID = env->GetFieldID(dataClass, "mTyreMidTemps", "[F");
	tyreOuterTempsID = env->GetFieldID(dataClass, "mTyreOuterTemps", "[F");
}
void getCarDamageIDs(JNIEnv* env, jclass dataClass){
	crashStateID = env->GetFieldID(dataClass, "mCrashState","I");
	aeroConditionID = env->GetFieldID(dataClass, "mAeroCondition","F");
	engineConditionID = env->GetFieldID(dataClass, "mEngineCondition","F");
	transmissionConditionID = env->GetFieldID(dataClass, "mTransmissionCondition", "F");
}
void getWeatherIDs(JNIEnv* env, jclass dataClass){
	ambientTempID = env->GetFieldID(dataClass,"mAmbientTemperature","F");
	trackTempID = env->GetFieldID(dataClass, "mTrackTemperature","F");
	rainDensityID = env->GetFieldID(dataClass,"mRainDensity","F");
	windSpeedID = env->GetFieldID(dataClass,"mWindSpeed","F");
	windDirectionXID = env->GetFieldID(dataClass,"mWindDirectionX","F");
	windDirectionYID = env->GetFieldID(dataClass,"mWindDirectionY","F");
	cloudBrightnessID = env->GetFieldID(dataClass,"mCloudBrightness","F");
}
void getParticipantIDs(JNIEnv* env, jclass dataClass){
	participantsID = env->GetFieldID(dataClass, "mParticipants", "[Ldata/VDashData$Participant;");

	participantActiveID = env->GetFieldID(participantClass, "mIsActive", "Z");
	participantNameID = env->GetFieldID(participantClass, "mName", "[C");
	participantWorldPositionID = env->GetFieldID(participantClass, "mWorldPosition","[F");
	participantCurrentLapDistanceID = env->GetFieldID(participantClass, "mCurrentLapDistance","F");
	participantRacePositionID = env->GetFieldID(participantClass, "mRacePosition","I");
	participantLapsCompletedID = env->GetFieldID(participantClass, "mLapsCompleted","I");
	participantCurrentLapID = env->GetFieldID(participantClass, "mCurrentLap","I");
	participantCurrentSectorID = env->GetFieldID(participantClass, "mCurrentSector","I");
}

void getETS2IDs(JNIEnv* env, jclass dataClass){
	navigationDistanceID = env->GetFieldID(dataClass,"navigationDistance","F");
	jobSourceCityID = env->GetFieldID(dataClass, "jobSourceCity", "[C");
	jobDestinationCityID = env->GetFieldID(dataClass, "jobDestinationCity", "[C");
}

static void findFieldIDs(JNIEnv *env){
	dataClass = env->FindClass("data/VDashData");
	participantClass = env->FindClass("data/VDashData$Participant");

	constructor = env->GetMethodID(dataClass, "<init>", "()V");
	participantConstructor = env->GetMethodID(participantClass, "<init>","()V");

	dataClass = env->FindClass("data/VDashData");
	getVersionIDs(env, dataClass);
	getStatesIDs(env, dataClass);
	getParticipantInfoIDs(env, dataClass);
	getUnfilteredInputIDs(env, dataClass);
	getVehicleInformationIDs(env, dataClass);
	getEventInformationIDs(env, dataClass);
	getTimeIDs(env, dataClass);
	getFlagIDs(env, dataClass);
	getPitIDs(env, dataClass);
	getCarStateIDs(env, dataClass);
	getMotionFieldIDs(env, dataClass);
	getWheelsTyresDataIDs(env, dataClass);
	getCarDamageIDs(env, dataClass);
	getWeatherIDs(env, dataClass);
	getParticipantIDs(env, dataClass);
	getETS2IDs(env, dataClass);
}

static void test(){

}





#endif /* DATASOURCEBASE_H_ */
