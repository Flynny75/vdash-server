// dll implementation file MemMapLib.cpp
//
////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <tchar.h>
#include <jni.h>

#include "data_SimBinDataSource.h"
#include "DataSourceBase.h"
#include "SimBin.h"

const racedata* sharedData;

void ErrorHandler(LPCTSTR pszErrorMessage) {
	MessageBox(NULL, pszErrorMessage, _T("Error"), MB_OK | MB_ICONERROR);
}

float FtoK(float f){
	return ((5.0f/9.0f)*(f-32))+273;
}

void getParticipantInfo(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject,viewingParticipantID,0);

	int numCars = sharedData->numCars;
	if(numCars < 0){numCars = 0;}
	env->SetIntField(dataObject, numParticipantsID, numCars);
}

void getEventInformation(JNIEnv* env, jobject dataObject){
	int laps = sharedData->numberOfLaps;
	if(laps < 0){laps = 0;}
	env->SetIntField(dataObject,lapsInEventFieldID,laps);
}

void getTimes(JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject,bestLapTimeID,sharedData->lapTimeBest);
	env->SetFloatField(dataObject,lastLapTimeID,sharedData->lapTimePrevious);
	env->SetFloatField(dataObject,currentTimeID,sharedData->lapTimeCurrent);
}

void getCarState(JNIEnv* env, jobject dataObject){
	float oilTemp = sharedData->engineOilTemp;
	if(oilTemp < 0){oilTemp = 0;}
	env->SetFloatField(dataObject, oilTempCelsID, oilTemp);

	float oilP = sharedData->engineOilPressure;
	if(oilP < 0){oilP = 0;}
	env->SetFloatField(dataObject,oilPressureKPaID,oilP);

	float waterTemp = sharedData->engineWaterTemp;
	if(waterTemp < 0){waterTemp = 0;}
	env->SetFloatField(dataObject,waterTempCelsID,waterTemp);

	env->SetFloatField(dataObject, fuelPressureKPaID, sharedData->fuelPressure);

	env->SetFloatField(dataObject,fuelLevelID,sharedData->fuel);
	env->SetFloatField(dataObject,fuelCapacityID, sharedData->fuelCapacityLiters);

	env->SetFloatField(dataObject, speedID, sharedData->carSpeed);

	float rpm = (sharedData->rpm)*10;
	if(rpm < 0){rpm = 0;}
	env->SetFloatField(dataObject,rpmID,rpm);

	float maxRpm = (sharedData->maxEngineRPS)*10;
	if(maxRpm < 0){maxRpm = 0;}
	env->SetFloatField(dataObject,maxRpmID,maxRpm);

	int gear = sharedData->gear;
	if(gear == -2){
		gear = 0;
	}
	env->SetIntField(dataObject,gearID,gear);
	env->SetIntField(dataObject,numGearsID, 7);
}

void getMotionAndDeviceInfo(JNIEnv* env, jobject dataObject){
	jfloatArray localAccelerationData = env->NewFloatArray(3);
	float accs[3];
	accs[0] = sharedData->lateral;
	accs[1] = sharedData->vertical;
	accs[2] = sharedData->longitudinal;
	env->SetFloatArrayRegion(localAccelerationData,0,3,accs);
	env->SetObjectField(dataObject,localAccelerationID,localAccelerationData);
}

void getWheelsTyresData(JNIEnv* env, jobject dataObject){
	float tread[4];
	float layer[4];
	float internal[4];

	layer[0] = FtoK(sharedData->tirefrontleft[0]);	layer[1] = FtoK(sharedData->tirefrontright[2]);
	layer[2] = FtoK(sharedData->tirerearleft[0]);	layer[3] = FtoK(sharedData->tirerearright[2]);

	tread[0] = FtoK(sharedData->tirefrontleft[1]);	tread[1] = FtoK(sharedData->tirefrontright[1]);
	tread[2] = FtoK(sharedData->tirerearleft[1]);	tread[3] = FtoK(sharedData->tirerearright[1]);

	internal[0] = FtoK(sharedData->tirefrontleft[2]);	internal[1]=FtoK(sharedData->tirefrontright[0]);
	internal[2] = FtoK(sharedData->tirerearleft[2]);	internal[3]=FtoK(sharedData->tirerearright[0]);

	jfloatArray treadData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(treadData, 0, 4, tread);
	env->SetObjectField(dataObject, tyreTreadTempID, treadData);

	jfloatArray layerData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(layerData, 0, 4, layer);
	env->SetObjectField(dataObject, tyreLayerTempID, layerData);

	jfloatArray internalData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(internalData, 0, 4, internal);
	env->SetObjectField(dataObject, tyreInternalAirTempID, internalData);
}

void getParticipants(JNIEnv* env, jobject dataObject){
	participantClass = env->FindClass(PARTICIPANT_CLASS);

	jobject participant = env->NewObject(participantClass, participantConstructor);
	jobjectArray allParticipants= env->NewObjectArray(1, participantClass, NULL);

	participant = env->NewObject(participantClass, participantConstructor);

	env->SetBooleanField(participant, participantActiveID, true);
	env->SetIntField(participant, participantLapsCompletedID, sharedData->completedLaps);
	env->SetIntField(participant, participantCurrentLapID, (sharedData->completedLaps)+1);
	env->SetIntField(participant, participantCurrentSectorID, 0);
	env->SetIntField(participant, participantRacePositionID, sharedData->position);

	env->SetObjectArrayElement(allParticipants, 0, participant);
	env->SetObjectField(dataObject, participantsID, allParticipants);
}

JNIEXPORT jlong JNICALL Java_data_SimBinDataSource_openFileMapping
(JNIEnv * pEnv, jclass, jstring name) {

	HANDLE hMapFile = NULL;

	LPCSTR lpName = pEnv->GetStringUTFChars(name, NULL);
	hMapFile = OpenFileMapping( PAGE_READONLY, FALSE, lpName );
	if(hMapFile == NULL){
		return 0;
	}else{
		pEnv->ReleaseStringUTFChars(name, lpName);
		return reinterpret_cast<jlong>(hMapFile);
	}
}

JNIEXPORT jint JNICALL Java_data_SimBinDataSource_mapViewOfFile
(JNIEnv *, jclass, jlong hMapFile) {

	sharedData = (racedata*)MapViewOfFile(reinterpret_cast<HANDLE>(hMapFile), PAGE_READONLY, 0, 0, sizeof(racedata) );
	if (sharedData == NULL)
	{
		printf( "Could not open SimBin memory file.\n", GetLastError() );

		CloseHandle( reinterpret_cast<HANDLE>(hMapFile));
		return 0;
	}
	return 1;
}

JNIEXPORT jobject JNICALL Java_data_SimBinDataSource_getData(JNIEnv* env, jclass){
	dataClass = env->FindClass(DATA_CLASS);
	jobject dataObject = env->NewObject(dataClass, constructor);

	getParticipantInfo(env, dataObject);
	getEventInformation(env, dataObject);
	getTimes(env, dataObject);
	getCarState(env, dataObject);
	getMotionAndDeviceInfo(env, dataObject);
	getWheelsTyresData(env, dataObject);
	getParticipants(env, dataObject);

	return dataObject;
}

JNIEXPORT void JNICALL Java_data_SimBinDataSource_findFieldIDs(JNIEnv* env, jclass){
	findFieldIDs(env);
}

JNIEXPORT jboolean JNICALL Java_data_SimBinDataSource_closeHandle
(JNIEnv *, jclass, jlong hObject) {

	return CloseHandle(reinterpret_cast<HANDLE>(hObject));
}
