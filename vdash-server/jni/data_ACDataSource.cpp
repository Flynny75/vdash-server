// dll implementation file MemMapLib.cpp
//
////////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <sstream>

#include "data_ACDataSource.h"
#include "AssettoCorsa.h"

#include "DataSourceBase.h"

const int TYRE_MAX = 4;
const int VEC_MAX = 3;

const SPageFilePhysics* sharedDataPhysics;
const SPageFileGraphic* sharedDataGraphics;
const SPageFileStatic* sharedDataStatics;

int maxRpm;

void ErrorHandler(LPCTSTR pszErrorMessage) {
	MessageBox(NULL, pszErrorMessage, _T("Error"), MB_OK | MB_ICONERROR);
}

float FtoK(float f){
	return ((5.0f/9.0f)*(f-32))+273;
}

float floatTime(const char* str){
	std::string s(str);
	std::vector<std::string> tokens;
	size_t pos=0;
	while((pos = s.find(":")) != std::string::npos){
		tokens.push_back(s.substr(0, pos));
		s.erase(0, pos + 1);
	}
	tokens.push_back(s);

	float seconds = 0;

	for(int i = 0; i < tokens.size(); i++){
		int t;
		std::istringstream( tokens.at((tokens.size()-1)-i)) >> t;
		if(i == 0){
			//millis
			seconds += ((float)t/(float)1000);
		}else if(i == 1){
			//secs
			seconds += t;
		}else if(i == 2){
			//mins
			seconds += (t*60);
		}
	}

	return seconds;
}

void getVersion(JNIEnv* env, jobject dataObject){
	env->SetObjectField(dataObject, apiVersionNumberStringID, makeCharArray(env, sharedDataStatics->smVersion));
	env->SetObjectField(dataObject, buildNumberStringID, makeCharArray(env, sharedDataStatics->acVersion));
}

void getStates(JNIEnv* env, jobject dataObject){
	int status;
	switch(sharedDataGraphics->status){
	case AC_OFF:
	{
		status = 0;
		break;
	}
	case AC_REPLAY:
	{
		status = 7;
		break;
	}
	case AC_LIVE:
	{
		status = 2;
		break;
	}
	case AC_PAUSE:
	{
		status = 3;
		break;
	}
	}
	env->SetIntField(dataObject,gameStateFieldID,status);

	int sessionState;
	switch(sharedDataGraphics->session){
	case AC_UNKNOWN:{
		sessionState = 0;
		break;
	}
	case AC_PRACTICE:{
		sessionState = 1;
		break;
	}
	case AC_QUALIFY:{
		sessionState = 3;
		break;
	}
	case AC_RACE:{
		sessionState = 5;
		break;
	}
	case AC_HOTLAP:{
		sessionState = 7;
		break;
	}
	case AC_TIME_ATTACK:{
		sessionState = 6;
		break;
	}
	case AC_DRIFT:{
		sessionState = 8;
		break;
	}
	case AC_DRAG:{
		sessionState = 9;
		break;
	}
	}

	env->SetIntField(dataObject, sessionStateFieldID, sessionState);
}

void getParticipantInfo(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject,viewingParticipantID,0);
	int min = std::min(sharedDataStatics->numCars,64);
	env->SetIntField(dataObject, numParticipantsID, min);
}

void getUnfilteredInput(JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject,unfilteredThrottleID,sharedDataPhysics->gas);
	env->SetFloatField(dataObject,unfilteredBrakeID,sharedDataPhysics->brake);
	env->SetFloatField(dataObject,unfilteredSteeringID,sharedDataPhysics->steerAngle);
}

void getVehicleInformation(JNIEnv* env, jobject dataObject){
	env->SetObjectField(dataObject,carNameID,makeCharArray(env, sharedDataStatics->carModel));

	//XXX No car class name
//	jobject carClassNameString = env->NewStringUTF(sharedData->mCarClassName);
//	env->SetObjectField(dataObject,carClassNameID,carClassNameString);
}

void getEventInformation(JNIEnv* env, jobject dataObject){
	env->SetIntField(dataObject,lapsInEventFieldID,sharedDataGraphics->numberOfLaps);
	env->SetObjectField(dataObject,trackLocationID,makeCharArray(env, sharedDataStatics->track));

	//XXX No track variation or length also TRACK is broken!
//	jobject trackVariationString = env->NewStringUTF(sharedData->mTrackVariation);
//	env->SetObjectField(dataObject,trackLocationID,trackVariationString);
//	env->SetFloatField(dataObject, trackLengthID, sharedData->mTrackLength);
}

void getTimes(JNIEnv* env, jobject dataObject){
	env->SetFloatField(dataObject,bestLapTimeID, floatTime(sharedDataGraphics->bestTime));
	env->SetFloatField(dataObject,lastLapTimeID, floatTime(sharedDataGraphics->lastTime));
	env->SetFloatField(dataObject,currentTimeID, floatTime(sharedDataGraphics->currentTime));
	env->SetFloatField(dataObject,splitTimeID, floatTime(sharedDataGraphics->split));
	env->SetFloatField(dataObject,eventTimeRemainingID, ((sharedDataGraphics->sessionTimeLeft)/(float)1000));
	env->SetFloatField(dataObject, lastSectorTimeID, ((sharedDataGraphics->lastSectorTime)/(float)1000));
}

void getFlag(JNIEnv* env, jobject dataObject){
	//XXX no flag info
//	env->SetIntField(dataObject, highestFlagColourID, sharedData->mHighestFlagColour);
//	env->SetIntField(dataObject, highestFlagReasonID, sharedData->mHighestFlagReason);
}

void getPit(JNIEnv* env, jobject dataObject){
	//XXX limit pit info. isInPit > PIT_MODE.PIT_MODE_IN_PIT else PIT_MODE_NONE
	int pMode;
	if(sharedDataGraphics->isInPit){
		pMode = 2; //(SharedMemory.h #152)
	}else{
		pMode = 0; //(SharedMemory.h #150)
	}

	env->SetIntField(dataObject, pitModeID, pMode);
}

void getCarState(JNIEnv* env, jobject dataObject){
	//XXX include DRS/ABS/TCS in car flags. Lots of other things missing

	//Expecting fuel in %. AC provides real values. Need to convert back to %.
	float realFuel = sharedDataPhysics->fuel;
	float realMaxFuel = sharedDataStatics->maxFuel;
	float fuelPercent = (realFuel / realMaxFuel);
	env->SetFloatField(dataObject,fuelLevelID,realFuel);
	env->SetFloatField(dataObject,fuelCapacityID, realMaxFuel);
	env->SetFloatField(dataObject, speedID, ((sharedDataPhysics->speedKmh * 1000))/ 3600);
	env->SetFloatField(dataObject,rpmID,sharedDataPhysics->rpms);

	if(sharedDataStatics->maxRpm == 0){
		maxRpm = std::max(maxRpm, sharedDataPhysics->rpms);
		env->SetFloatField(dataObject,maxRpmID, maxRpm);
	}else{
		env->SetFloatField(dataObject,maxRpmID, sharedDataStatics->maxRpm);
	}

	env->SetFloatField(dataObject,brakeID,sharedDataPhysics->brake);
	env->SetFloatField(dataObject,throttleID,sharedDataPhysics->gas);
	env->SetFloatField(dataObject,steeringID,sharedDataPhysics->steerAngle);
	env->SetIntField(dataObject,gearID,(sharedDataPhysics->gear)-1);
	env->SetFloatField(dataObject, maxTorqueID, sharedDataStatics->maxTorque);
	env->SetFloatField(dataObject, maxPowerID, sharedDataStatics->maxPower);
}

void getMotionAndDeviceInfo(JNIEnv* env, jobject dataObject){
	jfloatArray worldVelocityData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(worldVelocityData,0,VEC_MAX,sharedDataPhysics->velocity);
	env->SetObjectField(dataObject,worldVelocityID,worldVelocityData);

	jfloatArray localAccelerationData = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(localAccelerationData,0,VEC_MAX,sharedDataPhysics->accG);
	env->SetObjectField(dataObject,localAccelerationID,localAccelerationData);
}

void getWheelsTyresData(JNIEnv* env, jobject dataObject){

	float temp[4];

	temp[0] = convertCelsToKelvin(sharedDataPhysics->tyreCoreTemperature[0]);	temp[1] = convertCelsToKelvin(sharedDataPhysics->tyreCoreTemperature[1]);
	temp[2] = convertCelsToKelvin(sharedDataPhysics->tyreCoreTemperature[2]);	temp[3] = convertCelsToKelvin(sharedDataPhysics->tyreCoreTemperature[3]);

	jfloatArray treadData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(treadData, 0, 4, temp);
	env->SetObjectField(dataObject, tyreTreadTempID, treadData);

	jfloatArray layerData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(layerData, 0, 4, temp);
	env->SetObjectField(dataObject, tyreLayerTempID, layerData);

	jfloatArray internalData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(internalData, 0, 4, temp);
	env->SetObjectField(dataObject, tyreInternalAirTempID, internalData);

	jfloatArray tyreTempsData = env->NewFloatArray(4);
	env->SetFloatArrayRegion(tyreTempsData, 0, 4, temp);
	env->SetObjectField(dataObject, tyreTempID, tyreTempsData);

	jfloatArray tyreWearData = env->NewFloatArray(TYRE_MAX);
	env->SetFloatArrayRegion(tyreWearData,0,TYRE_MAX,sharedDataPhysics->tyreWear);
	env->SetObjectField(dataObject,tyreConditionID,tyreWearData);
}

void getCarDamage(JNIEnv* env, jobject dataObject){
}

void getWeather(JNIEnv* env, jobject dataObject){
}

void getParticipants(JNIEnv* env, jobject dataObject){
	participantClass = env->FindClass(PARTICIPANT_CLASS);

	jobject participant = env->NewObject(participantClass, participantConstructor);
	jobjectArray allParticipants= env->NewObjectArray(1, participantClass, NULL);

	participant = env->NewObject(participantClass, participantConstructor);

	env->SetBooleanField(participant, participantActiveID, true);
	env->SetIntField(participant, participantLapsCompletedID, sharedDataGraphics->completedLaps);
	env->SetIntField(participant, participantCurrentLapID, (sharedDataGraphics->completedLaps)+1);
	env->SetIntField(participant, participantCurrentSectorID, sharedDataGraphics->currentSectorIndex);
	env->SetIntField(participant, participantRacePositionID, sharedDataGraphics->position);
	env->SetObjectField(participant,participantNameID,makeCharArray(env, sharedDataStatics->playerNick));

	jfloatArray participantWorldPosition = env->NewFloatArray(VEC_MAX);
	env->SetFloatArrayRegion(participantWorldPosition,0,VEC_MAX, sharedDataGraphics->carCoordinates);
	env->SetObjectField(participant,participantWorldPositionID,participantWorldPosition);

	env->SetObjectArrayElement(allParticipants, 0, participant);
	env->SetObjectField(dataObject, participantsID, allParticipants);
}

JNIEXPORT jlong JNICALL Java_data_ACDataSource_openFileMapping
(JNIEnv * pEnv, jclass, jstring name) {

	HANDLE hMapFile = NULL;

	LPCSTR lpName = pEnv->GetStringUTFChars(name, NULL);
	hMapFile = OpenFileMapping( PAGE_READONLY, FALSE, lpName );
	if(hMapFile == NULL){
		return 0;
	}else{
		pEnv->ReleaseStringUTFChars(name, lpName);
		return reinterpret_cast<jlong>(hMapFile);
	}
}

JNIEXPORT jint JNICALL Java_data_ACDataSource_mapViewOfFile
(JNIEnv* env, jclass, jlong acPhysicsFile, jlong acGraphicsFile, jlong acStaticsFile) {

	sharedDataGraphics = (SPageFileGraphic*)MapViewOfFile(reinterpret_cast<HANDLE>(acGraphicsFile), PAGE_READONLY, 0, 0, sizeof(SPageFileGraphic));
	if (sharedDataGraphics == NULL)
	{
		printf( "Could not open AC graphics file.\n", GetLastError() );
		CloseHandle( reinterpret_cast<HANDLE>(acGraphicsFile));
		return 0;
	}

	sharedDataPhysics = (SPageFilePhysics*)MapViewOfFile(reinterpret_cast<HANDLE>(acPhysicsFile), PAGE_READONLY, 0, 0, sizeof(SPageFilePhysics));
	if(sharedDataPhysics == NULL)
	{
		printf("Could not open AC physics file.\n", GetLastError());
		CloseHandle(reinterpret_cast<HANDLE>(acPhysicsFile));
		return 0;
	}

	sharedDataStatics = (SPageFileStatic*)MapViewOfFile(reinterpret_cast<HANDLE>(acStaticsFile), PAGE_READONLY, 0, 0, sizeof(SPageFileStatic));
	if(sharedDataStatics == NULL)
	{
		printf("Could not open AC statics file.\n", GetLastError());
		CloseHandle(reinterpret_cast<HANDLE>(acStaticsFile));
		return 0;
	}

	return 1;
}

JNIEXPORT jobject JNICALL Java_data_ACDataSource_getData(JNIEnv* env, jclass){
	dataClass = env->FindClass(DATA_CLASS);
	jobject dataObject = env->NewObject(dataClass, constructor);

	getVersion(env, dataObject);
	getStates(env, dataObject);
	getParticipantInfo(env, dataObject);
	getUnfilteredInput(env, dataObject);
	getVehicleInformation(env, dataObject);
	getEventInformation(env, dataObject);
	getTimes(env, dataObject);
	getFlag(env, dataObject);
	getPit(env, dataObject);
	getCarState(env, dataObject);
	getMotionAndDeviceInfo(env, dataObject);
	getWheelsTyresData(env, dataObject);
	getCarDamage(env, dataObject);
	getWeather(env, dataObject);
	getParticipants(env, dataObject);

	return dataObject;
}

JNIEXPORT void JNICALL Java_data_ACDataSource_findFieldIDs(JNIEnv *env, jclass){
	findFieldIDs(env);
}

JNIEXPORT jboolean JNICALL Java_data_ACDataSource_closeHandle (JNIEnv *, jclass, jlong hObject) {
	return CloseHandle(reinterpret_cast<HANDLE>(hObject));
}
