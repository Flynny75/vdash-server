package net.sf.jinsim.examples.queue;

import net.sf.jinsim.response.InSimListener;
import net.sf.jinsim.response.InSimResponse;

public class SimpleInSimListener implements InSimListener {

	private String name;

	public SimpleInSimListener(String name) {
		this.name = name;
	}
	
	public void packetReceived(InSimResponse response) {
		System.out.println(name + ": " + response);

	}

}
