/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is the JInSim Library.
 *
 * The Initial Developer of the Original Code is Rob Heiser.
 *
 * Portions created by the Initial Developer are Copyright (C) 2006
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *
 *   Rob Heiser <jinsim@kerf.org>
 *   Rene Pirringer <brilwing@liveforspeed.at>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package net.sf.jinsim.examples.button;

import java.io.IOException;

import net.sf.jinsim.Channel;
import net.sf.jinsim.SimpleClient;
import net.sf.jinsim.UDPChannel;
import net.sf.jinsim.request.ButtonRequest;
import net.sf.jinsim.response.ButtonClickedResponse;
import net.sf.jinsim.response.InSimListener;
import net.sf.jinsim.response.InSimResponse;
import net.sf.jinsim.response.MessageResponse;

/**
 * This is the "Hello World!" example for using JInSim. This is the bare minimum you need to
 * do to see JInSim work. If you 
 * @author Rob Heiser (jinsim@kerf.org)
 * @since 0.001
 * 
 */
public class Main implements InSimListener {
    private SimpleClient client;
    private String hostname;
    private int port; 
    private String adminPassword;
	private boolean isClicked = false;

    public Main(String[] args) {
        hostname = args[0];
        port = Integer.parseInt(args[1]);

        if (args.length > 2) {
            adminPassword = args[2];
        }
    }

    public void run() {
    	System.out.println("Run button test");
        try {
        	client = new SimpleClient();
        	
            
            Channel channel = new UDPChannel(hostname, port);
            client.addListener(this);
            client.connect(channel, adminPassword, "ButtonTest");

            ButtonRequest buttonRequest = new ButtonRequest();
            buttonRequest.setButtonStyle(ButtonRequest.BUTTON_STYLE_GREEN | ButtonRequest.BUTTON_STYLE_LIGHT | ButtonRequest.BUTTON_STYLE_CLICK);
            buttonRequest.setText("My first Button");
            buttonRequest.setConnectionId(255);
            buttonRequest.setClickId((byte)1);
            buttonRequest.setRequestInfo((byte)1);
            buttonRequest.setLeft((byte)75);
            buttonRequest.setTop((byte)30);
            buttonRequest.setWidth((byte)50);
            buttonRequest.setHeight((byte)5);
            client.send(buttonRequest);
            while (!isClicked) {
            	Thread.sleep(100);
            }
        } catch (Exception e) {
            e.printStackTrace();
            
        } finally {
        	try {
				client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }

    public void packetReceived(InSimResponse response) {
        if (response instanceof MessageResponse) {
            MessageResponse msgResponse = (MessageResponse) response;
            System.out.println(msgResponse.getMessage());
        } else if (response instanceof ButtonClickedResponse) {
        	ButtonClickedResponse buttonClickResponse = (ButtonClickedResponse)response;
        	isClicked = true;
            System.out.println("Left Click: " + buttonClickResponse.isLeftClick());
            System.out.println("Right Click: " + buttonClickResponse.isRightClick());
            System.out.println("Ctrl: " + buttonClickResponse.isCtrl());
            System.out.println("Shift: " + buttonClickResponse.isShift());
        }
    }

     static public void main(String[] args) {
        if (args.length >= 2) {
            Main testConnect = new Main(args);
            testConnect.run();
        } else {
            System.out.println("usage: net.sf.insim.message.Main <hostname> <port> <admin password>");
        }
    }

}
