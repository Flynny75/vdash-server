package net.sf.jinsim.response;

import net.sf.jinsim.PacketType;

public class ConnectionCloseResponse extends InSimResponse {

	public ConnectionCloseResponse() {
		super(PacketType.CLOSE);
	}

}
